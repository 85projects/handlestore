<?php
session_start(); 
include_once("template-parts/header_links.php");
include_once("template-parts/navbar_m.php");
include_once("template-parts/footer.php");
include_once("includes/main_include.php");

 ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
  <title>The Handle Store | Checkout</title>
  <meta name="description" content="Best handle selling website">
  <meta name="author" content="Handle Store">
  <?php header_links(); ?>
  <script src="js/sweetalert.min.js"></script>
  <link href="css/sweetalert.css" rel="stylesheet">
</head>
<?php if(isset($_SESSION["thehandlestore_AZwUghJKdP"])){ ?>
<body>
<?php bottom_menu(); ?>
<div class="checkout">
  <div class="container2">

    <div class="checkout_title">      
       <div class="row">
         <div class="col-md-12">
           <h1>Checkout</h1>
         </div>
       </div>
    </div>
    <form name="" action="" method="post">
    <div class="checkout_details">
       
       <div class="row">
         <div class="col-md-6" style="padding-top: 20px;">
           <input type="text" name="name" class="form-control" placeholder="Name*">
         </div>
         <div class="col-md-12" style="padding-top: 20px;">
           <input type="text" name="company" class="form-control" placeholder="Company Name">
         </div>
       </div>
       <div class="row">
         <div class="col-md-12" style="padding-top: 20px;">
           <label for="cars">Choose a country*:</label>

<select name="country" id="cars" class="form-control">
  <option value="India">India</option>
  <option value="USA">USA</option>
  <option value="ENGLAND">England</option>
  <option value="AUSTRALIA">Australia</option>
</select>
         </div>
       </div>


         <div class="row">
         <div class="col-md-12" style="padding-top: 20px;">
           <input type="text" name="address" class="form-control" placeholder="Address*">
         </div>
       </div>

       <div class="row">
         <div class="col-md-12" style="padding-top: 20px;">
           <input type="text" name="town" class="form-control" placeholder="Town/City*">
         </div>
       </div>

        <div class="row">
         <div class="col-md-12" style="padding-top: 20px;">
           <input type="number" name="postcode" class="form-control" placeholder="Postcode/Zip*">
         </div>
       </div>

       <div class="row">
         <div class="col-md-6" style="padding-top: 20px;">
           <input type="number" name="phone" class="form-control" placeholder="Phone*">
         </div>
       </div>

       <div class="row">
         <div class="col-md-12" style="padding-top: 20px;">
            <label for="cars">Order Notes</label>
            <textarea class="form-control" name="notes" placeholder="Note about your order,eg.special notes for delivery"></textarea>
         </div>
       </div>
     
       <div class="row">
         <div class="col-md-12 text-right" style="padding-top: 40px;">
           <h3>Billing Method</h3>
       
           <input type="radio" id="" name="payment" value="bank transfer" required>
           <label for="">Bank Transfer</label><br>
           <input type="radio" id="" name="payment" value="Check Payment" required> 
           <label for="">Check Payment</label><br>
           <input type="radio" id="" name="payment" value="UPI Payment" required>
           <label for="">UPI Payment</label><br>

           <button class="btn" type="submit" name="submit" id="checkout_final_btn"><strong>Checkout</strong></button>
         </div>
       </div>

    </div>
    </form>

  </div>
</div>

<?php
if(isset($_POST["submit"])){ 
$name = $_POST["name"];
$company = $_POST["company"];
$country = $_POST["country"];
$address = $_POST["address"];
$town = $_POST["town"];
$postcode = $_POST["postcode"];
$phone = $_POST["phone"];
$notes = $_POST["notes"];
$payment = $_POST["payment"];
    $csid = $_COOKIE["thehandlestore_AZwUghJKdP"];
	 $user_id = $_SESSION["thehandlestore_AZwUghJKdP"];
    $q = "SELECT `cart_id`, `pro_id`, `pro_qty`, `pro_price`, `pro_total_price`, `csid` FROM `user_cart` WHERE `csid` = '$csid'";
	$b = mysqli_query($conn,$q);
	while($row = mysqli_fetch_array($b))
	{
				   $pro_qty = $row['pro_qty'];
				   $pro_price = $row['pro_price'];
				   $pro_total_price = $row['pro_total_price'];
				   $total_price = array($row["pro_total_price"]);
				   $values = array_sum($total_price);
				   $total+=$values;
		  
				   
	}
	$date = date_default_timezone_set('Asia/Kolkata');
	$today = date("F j, Y, g:i a");	
    $query ="INSERT INTO `cust_order`(`order_id`, `payment_option`, `user_id`, `total_amount`, `order_date`, `order_note`, `order_status`) VALUES ('','$payment','$user_id','$total','$today','$notes','0')";
    $xb = mysqli_query($conn,$query);
	$last_id = mysqli_insert_id($conn);
	if($xb){
	$sq = "SELECT * FROM `user_sub_details` WHERE `user_id` = '$user_id'";
	$b1 = mysqli_query($conn,$sq);
    $numw = mysqli_num_rows($conn,$b1);
	if($numw > 0){
	
		$update = "UPDATE `user_sub_details` SET  `cust_name`='$name',`country`='$country',`address`='$address',`town`='$town',`pin_code`='$postcode',`company_name`='$company', `contact_no`='$phone' WHERE `user_id` = '$user_id'";
		$b1 = mysqli_query($conn,$update);
 	
	}else{
		$update = "INSERT INTO `user_sub_details`(`user_sub_id`, `user_id`, `cust_name`, `country`, `address`, `town`, `pin_code`, `company_name`, `contact_no`) VALUES ('','$user_id','$name','$country','$address','$town','$postcode','$company','$phone')";
		$b1 = mysqli_query($conn,$update);
	}
	$q1 = "SELECT `cart_id`, `pro_id`, `pro_qty`, `pro_price`, `pro_total_price`, `csid` FROM `user_cart` WHERE `csid` = '$csid'";
	$b1 = mysqli_query($conn,$q1);
	while($row = mysqli_fetch_array($b1))
	{
				   $pro_id = $row['pro_id'];
				   $pro_qty = $row['pro_qty'];
				   $pro_price = $row['pro_price'];
				   $pro_total_price = $row['pro_total_price'];
				   $csid = $row['csid'];
				   
				   $ins = "INSERT INTO `cust_order_data`(`cust_order_id`, `order_id`, `user_id`, `pro_id`, `pro_qty`, `pro_price`, `pro_total_price`, `csid`) VALUES ('','$last_id','$user_id','$pro_id','$pro_qty','$pro_price','$pro_total_price','$csid')";
				   $b1s = mysqli_query($conn,$ins);
				   if($b1s)
				   {
					   
					   $del = "DELETE FROM `user_cart` WHERE `csid` = '$csid'";
					   $b1d = mysqli_query($conn,$del); if($b1d){?>
					   <script>alert("Your order has been placed successfully");
					   window.location.href = "index.php";
					   </script>
					  <?php
					   } }
				   
	}	
		
		
		
	}





}
?>
<!---------------------------FOOTER AREA--------------------------------->

<footer id="footer_area" style="background-color: #f5f6f8;">

  <div class="container">
    
    
    <div class="row">
    
    <div class="col-md-4" id="left_part_footer" >

             <a href="index.php"><img src="logos/handle_logo.png" alt="The hangle store" id="store_logo"></a>
      <p>Aliquam sodales accumsan justo, at fringilla 
elit pulvinar cursus.Aliquam sodales accumsan justo, 
at fringilla elit pulvinar cursus.</p>

    </div>  
    <div class="col-md-2">
      <ul>
      <a href=""><li><p>MAIN DOOR</p></li></a>
      <a href=""><li><p>KITCHEN</p></li></a>    
      <a href=""><li><p>CABINET</p></li></a>  
      <a href=""><li><p>KNOBS</p></li></a>  
      <a href=""><li><p>PROFILES</p></li></a> 
      <a href=""><li><p>MORTICE</p></li></a>  

      </ul>
    </div>
    <div class="col-md-2">
      <ul>
      <a href=""><li>ABOUT US</li></a>
      <a href=""><li>CONTACT US</li></a>    
      <a href=""><li>PRIVACY POLICY</li></a>  
      <a href=""><li>FAQ'S</li></a> 
      <a href=""><li>WARRANTY</li></a>  
      <a href=""><li>T&C</li></a> 
      <a href=""><li>RETURN POLICY</li></a> 

      </ul>
    </div>
    <div class="col-md-4"  id="social_media">
      
      <h4>Social Media</h4>

      <div class="btn-group" id="button_group_social">
        <button><i class="fab fa-facebook-f"></i></button>
        <button><i class="fab fa-twitter"></i></button>
        <button><i class="fab fa-instagram"></i></button>
        <button><i class="fab fa-youtube"></i></button>
      </div>


    </div>

    </div>


  </div>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<?php }else{ ?>
									    <script>
										alert("Please Login before Checkout");
										window.location.href = "account-login.php";
										</script>	
	
<?php } ?>
</body>
</html>