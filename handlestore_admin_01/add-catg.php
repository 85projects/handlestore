<?php 
ob_start();
session_start();
include_once("template-parts/footer.php"); 
include_once("template-parts/header.php"); 
include_once("includes/main_include.php"); 
if(isset($_SESSION["ADMIN_LOGIN_09"]) && $_SESSION["ADMIN_LOGIN_09"] !=""){
echo header_main(); 
 ?>   

            <!-- row -->

        <div class="content-body">
		    <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Data Table</h4>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered zero-configuration">
                                        <thead>
                                            <tr>
											    <th>Category Id</th>
                                                <th>Catgory Name</th>
                                                <th>Status</th>
                                                <th>Delete</th>                                               
                                            </tr>
                                        </thead>
                                        <tbody>
											<?php 
								$b1 ="SELECT * FROM `pro_main_category` WHERE 1";
								$c2 = mysqli_query($conn,$b1);
								while($row = mysqli_fetch_array($c2))
								{   
									$cat_id = $row['cat_id'];
									$cat_name = $row['cat_name'];
									
								
									?>
                                            <tr>
                                                <td><?php echo $cat_id ?></td>
                                                <td><?php echo $cat_name ?></td>
                                                <td>Active</td>
												<td>Delete</td>
                                            </tr>
                                           
								<?php }?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                               <th>Category Id</th>
                                                <th>Catgory Name</th>
                                                <th>Status</th>
                                                <th>Delete</th>    
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- row -->
			<form name="form1" action="" method="post" enctype="multipart/form-data">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Category Details</h4>
                                <div class="basic-form">
                                        <div class="form-group">
                                        <input type="text" name="category_name" class="form-control input-default" placeholder="Enter Product Name">
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
              
					<div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
							<button type="submit" name="submit" class="btn btn-dark mb-2">Add Main Category</button>
                            </div>
                        </div>
						
                    </div>

                    </div>
                </div>
				</form>
				
            </div>
			
        </div>
		<?php
		if(isset($_POST["submit"])){
		$category_name = $_POST["category_name"]; 
		$query = "INSERT INTO `pro_main_category`(`cat_id`, `cat_name`, `cat_status`) VALUES ('','$category_name','1')";
		$q_check = mysqli_query($conn,$query);
		if($q_check){
		?>
		<script>alert("New Category has been Added Successfully");</script><?php
	
		}}
    ?>
     
<?php site_footer(); ?>

	<script src="plugins/common/common.min.js"></script>
    <script src="js/custom.min.js"></script>
    <script src="js/settings.js"></script>
    <script src="js/gleek.js"></script>
    <script src="js/styleSwitcher.js"></script>
</body>

</html>
<?php } else{
	
	header("location:index.php");
} ?>