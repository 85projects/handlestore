<?php function header_main() { ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Handle Store</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="logo.png">
    <!-- Custom Stylesheet -->
    <link href="css/style.css" rel="stylesheet">
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	<link href="./plugins/tables/css/datatable/dataTables.bootstrap4.min.css" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<style>
[data-nav-headerbg="color_1"] .nav-header{
	background:#fff!important;
}
.nav-header .brand-logo a{
	padding:0!important;
	padding-right:10px;
}
.count{
color:red;	
}
</style>
</head>
<body>
 <script>
$(document).ready(function(){
 
 function load_unseen_notification(view = '')
 {
  $.ajax({
   url:"fetch.php",
   method:"POST",
   data:{view:view},
   dataType:"json",
   success:function(data)
   {
    if(data.unseen_notification > 0)
    {
     $('.count').html(data.unseen_notification);
    }
   }
  });
 }
 
 load_unseen_notification();
 
  setInterval(function(){ 
  load_unseen_notification();
 }, 50);
 
});
 </script>
    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
            </svg>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->

    
    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <div class="brand-logo">
                <a href="add.php">
                    <b class="logo-abbr"><img height="60px" width="80px" src="min_logo.png" alt=""> </b>
                    <span class="logo-compact"><img height="60px" width="100%" src="handle_logo.png" alt=""></span>
                    <span class="brand-title">
                        <img src="handle_logo.png" height="80px" width="150px" alt="">
                    </span>
                </a>
            </div>
        </div>
        <div class="header">    
            <div class="header-content clearfix">
                
                <div class="nav-control">
                    <div class="hamburger">
                        <span class="toggle-icon"><i class="icon-menu"></i></span>
                    </div>
                </div>
                <div class="header-right">
                    <ul class="clearfix">
                        <li class="icons dropdown">
                            <div class="user-img c-pointer position-relative"   data-toggle="dropdown">
                                <span class="activity active"></span>
                                <img src="min_logo.png" height="40" width="40" alt="">
                            </div>
                            <div class="drop-down dropdown-profile   dropdown-menu">
                                <div class="dropdown-content-body">
                                    <ul>
                                        <li><a href="logout.php"><i class="icon-key"></i> <span>Logout</span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!--**********************************
            Header end ti-comment-alt
        ***********************************-->

        <!--**********************************
            Sidebar start
        ***********************************-->
		<style>
		.nk-sidebar{
			overflow-x:scroll;
		}
		</style>
        <div class="nk-sidebar">           
            <div class="nk-nav-scroll">
                <ul class="metismenu" id="menu">
                    <li class="nav-label">Products Section</li>
                    <li class="mega-menu mega-menu-sm">
						 <a href="add.php" aria-expanded="false">
                            <i class="icon-note menu-icon"></i><span class="nav-text">Add New Products</span>
                        </a>
                    </li>
					<li class="mega-menu mega-menu-sm">
						 <a href="add-variable.php" aria-expanded="false">
                            <i class="icon-note menu-icon"></i><span class="nav-text">Add Variable Products</span>
                        </a>
                    </li>
					<li class="mega-menu mega-menu-sm">
						 <a href="update.php" aria-expanded="false">
                            <i class="icon-note menu-icon"></i><span class="nav-text">Update Products</span>
                        </a>
                    </li>
				   <li class="nav-label">Orders</li>
                   <li class="mega-menu mega-menu-sm">
						 <a href="list-orders.php" aria-expanded="false">
                          <i class="icon-menu menu-icon"></i><span class="nav-text"> New Orders (<i class="count">0</i> )</span>
                        </a>
                   </li>
				   
				   <li class="mega-menu mega-menu-sm">
						 <a href="pending-orders.php" aria-expanded="false">
                            <i class="icon-menu menu-icon"></i><span class="nav-text">Pending Orders</span>
                        </a>
                   </li>
				   <li class="mega-menu mega-menu-sm">
						 <a href="completed-orders.php" aria-expanded="false">
                            <i class="icon-menu menu-icon"></i><span class="nav-text">Completed Orders</span>
                        </a>
                   </li>
				   <li class="mega-menu mega-menu-sm">
						 <a href="rejected-orders.php" aria-expanded="false">
                            <i class="icon-menu menu-icon"></i><span class="nav-text">Rejected Orders</span>
                        </a>
                   </li>
                   <li class="nav-label">All Products</li>
                   <li class="mega-menu mega-menu-sm">
						 <a href="list.php" aria-expanded="false">
                            <i class="icon-menu menu-icon"></i><span class="nav-text">List All Products</span>
                        </a>
                    </li>
					
				   <li class="nav-label">Users</li>
                   <li class="mega-menu mega-menu-sm">
						 <a href="user-reg.php" aria-expanded="false">
                            <i class="icon-menu menu-icon"></i><span class="nav-text">Registered User List</span>
                        </a>
                    </li>
					<!--<li class="mega-menu mega-menu-sm">
						 <a href="list.php" aria-expanded="false">
                            <i class="icon-menu menu-icon"></i><span class="nav-text">User Feedback</span>
                        </a>
                    </li>-->
					
					<li class="nav-label">Category Section</li>
                   <li class="mega-menu mega-menu-sm">
						 <a href="add-catg.php" aria-expanded="false">
                            <i class="icon-menu menu-icon"></i><span class="nav-text">Add Main Category</span>
                        </a>
                    </li>
					<li class="mega-menu mega-menu-sm">
						 <a href="add-sub.php" aria-expanded="false">
                            <i class="icon-menu menu-icon"></i><span class="nav-text">Add Sub Category</span>
                        </a>
                    </li>
					<li class="mega-menu mega-menu-sm">
						 <a href="add-mat.php" aria-expanded="false">
                            <i class="icon-menu menu-icon"></i><span class="nav-text">Add Material</span>
                        </a>
                    </li>
					<li class="mega-menu mega-menu-sm">
						 <a href="add-sizes.php" aria-expanded="false">
                            <i class="icon-menu menu-icon"></i><span class="nav-text">Add Product Sizes</span>
                        </a>
                    </li> 
					<li class="mega-menu mega-menu-sm">
						 <a href="add-fin.php" aria-expanded="false">
                            <i class="icon-menu menu-icon"></i><span class="nav-text">Add Finish</span>
                        </a>
                    </li> 					
                </ul>
            </div>
        </div>
<?php } ?>