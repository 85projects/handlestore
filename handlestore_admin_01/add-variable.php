<?php 
ob_start();
session_start();
include_once("template-parts/footer.php"); 
include_once("template-parts/header.php"); 
include_once("includes/main_include.php"); 
if(isset($_SESSION["ADMIN_LOGIN_09"]) && $_SESSION["ADMIN_LOGIN_09"] !=""){
echo header_main(); 
 ?>
 <style>
  .select2-container .select2-selection--single{
    height:40px !important;
}
.select2-container--default .select2-selection--single{
         border: 1px solid #ccc !important; 
     border-radius: 5px !important; 
	 padding:5px;
}
 </style>
        <div class="content-body">

            <div class="row page-titles mx-0">
                <div class="col p-md-0">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Add Products</a></li>
						
                    </ol>
                </div>
            </div>
            <!-- row -->
			<form name="form1" action="" method="post" enctype="multipart/form-data">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Product Details</h4>
                                <p class="text-muted m-b-15 f-s-12">Add Product Name<code> and product Description Here</code></p>
                                <div class="basic-form">
                                        <div class="form-group">
                                            <input type="text" name="product_name_1" class="form-control input-default" placeholder="Enter Product Name">
                                        </div>
                                        <div class="form-group">
                                            <textarea type="text" name="product_desc" class="form-control input-flat" rows="5" placeholder="Enter Product Description"></textarea>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Product Sub Details</h4>
                                <div class="basic-form">
								        <div class="form-group">
                                            <label>Choose Main Product:</label>
                                            <select class="form-control select2" name="pro_variation" id="sel1">
										<?php	$b1 ="SELECT * FROM `products_db` WHERE 1";
												$c2 = mysqli_query($conn,$b1);
												while($row = mysqli_fetch_array($c2))
												{   
													$pro_id = $row['pro_id']; 
													$pro_name = $row['pro_name'];
										?>
                                                <option value="<?php echo $pro_id ?>"><?php echo $pro_name ?></option>
												<?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Choose Product Main Category:</label>
                                            <select class="form-control" name="pro_main_catg" id="sel1">
										<?php	$b1 ="SELECT * FROM `pro_main_category` WHERE 1";
												$c2 = mysqli_query($conn,$b1);
												while($row = mysqli_fetch_array($c2))
												{   $cat_id = $row['cat_id'];
													$cat_name = $row['cat_name']; 
													
										?>
                                                <option value="<?php echo $cat_id ?>"><?php echo $cat_name ?></option>
												<?php } ?>
                                            </select>
                                        </div>
										<div class="form-group">
                                            <label>Choose Sub Product Sub Category:</label>
                                            <select class="form-control" name="pro_sub_catg" id="sel1">
										<?php	$b1 ="SELECT `sub_id`, `cat_name`, `main_cat_id`, `status` FROM `pro_sub_category` WHERE 1";
												$c2 = mysqli_query($conn,$b1);
												while($row = mysqli_fetch_array($c2))
												{   
													$sub_id = $row['sub_id']; 
													$cat_name = $row['cat_name'];
										?>
                                                <option value="<?php echo $sub_id ?>"><?php echo $cat_name ?></option>
												<?php } ?>
                                            </select>
                                        </div>
										
										<div class="form-group">
                                            <label>Product Material:</label>
                                            <select class="form-control" name="pro_material" id="sel1">
										<?php	$b1 ="SELECT * FROM `material` WHERE 1";
												$c2 = mysqli_query($conn,$b1);
												while($row = mysqli_fetch_array($c2))
												{   
													$mat_id = $row['mat_id']; 
													$material_name = $row['material_name'];
										?>
                                                <option value="<?php echo $mat_id ?>"><?php echo $material_name ?></option>
												<?php } ?>
                                            </select>
                                        </div>
										
										<div class="form-group">
                                            <label>Product Size:</label>
                                            <select class="form-control" name="pro_size" id="sel1">
										<?php	$b1 ="SELECT * FROM `handle_sizes` WHERE 1";
												$c2 = mysqli_query($conn,$b1);
												while($row = mysqli_fetch_array($c2))
												{   
													$size_id = $row['size_id']; 
													$size_name = $row['size_name'];
										?>
                                                <option value="<?php echo $size_id ?>"><?php echo $size_name ?></option>
												<?php } ?>
                                            </select>
                                        </div>
										
																				<div class="form-group">
                                            <label>Product Finishes:</label>
                                            <select class="form-control" name="pro_finish" id="sel1">
										<?php	$b1 ="SELECT * FROM `handle_finishes` WHERE 1";
												$c2 = mysqli_query($conn,$b1);
												while($row = mysqli_fetch_array($c2))
												{   
													$fin_id = $row['fin_id']; 
													$finish_name = $row['finish_name'];
										?>
                                                <option value="<?php echo $fin_id ?>"><?php echo $finish_name ?></option>
												<?php } ?>
                                            </select>
                                        </div>
										
										
										<div class="form-group">
                                            <input type="number"  value="" id="cBalance" name="pro_price" class="form-control input-default" placeholder="Enter Product Price">
                                        </div>
										<div class="form-group">
                                            <label>Is Discount:</label>
                                            <select class="form-control" name="is_dis" id="sel1">
                                                <option value="0">No</option>
                                                <option value="1">Yes</option>
                                            </select>
                                        </div>
										<div class="form-group">
                                            <input type="number" value="" id="chDiscount" name="discount_percentage" class="form-control input-default" placeholder="Discount Percentage">
                                        </div>
										<div class="form-group">
                                            <input type="number" value="" id="result_k" name="discount_price" class="form-control input-default" placeholder="Discount Price">
                                        </div>
										<script>
										 $(document).on("change keyup blur", "#chDiscount", function() {
            var main = $('#cBalance').val();
            var disc = $('#chDiscount').val();
            var dec = (disc / 100).toFixed(2); //its convert 10 into 0.10
            var mult = main * dec; // gives the value for subtract from main value
            var discont = main - mult;
            $('#result_k').val(discont);  });

										</script>
										<div class="form-group">
                                            <input type="number" value=""  name="pro_qty" class="form-control input-default" placeholder="Product Qty Avilable" required>
                                        </div><div class="form-group">
                                            <input type="number" value="" name="pro_qty_o" class="form-control input-default" placeholder="Product Qty Customer Can Order" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Product Status:</label>
                                            <select class="form-control" name="is_sta" id="sel1">
                                                <option value="1">Available</option>
                                                <option value="0">Not Availabele</option>
                                            </select>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
       <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Product Image</h4>
                                   <h4 class="card-title">Product Image</h4>
                                <div class="basic-form">
                                        <div class="form-group">
                                            <input type="file" name="file" id="file-1" class="inputfile inputfile-1 form-control-file">
                                        </div>
                                </div>
								<div class="basic-form">
                                        <div class="form-group">
                                            <input type="file" name="file-2" id="file-2" class="inputfile inputfile-1 form-control-file">
                                        </div>
                                </div>
								<div class="basic-form">
                                        <div class="form-group">
                                            <input type="file" name="file-3" id="file-3" class="inputfile inputfile-1 form-control-file">
                                        </div>
                                </div><div class="basic-form">
                                        <div class="form-group">
                                            <input type="file" name="file-4" id="file-4" class="inputfile inputfile-1 form-control-file">
                                        </div>
                                </div>
								<br>
							<button type="submit" name="submit" class="btn btn-dark mb-2">Add Product</button>
                            </div>
                        </div>
						
                    </div>

                    </div>
                </div>
				</form>
            </div>
        </div>
		<?php
		if(isset($_POST["submit"])){
			
		$product_name = $_POST["product_name_1"];
		$product_desc = $_POST["product_desc"];
		$pro_main_catg = $_POST["pro_main_catg"];
		$pro_sub_catg = $_POST["pro_sub_catg"];
		$pro_price = $_POST["pro_price"];
		$is_dis = $_POST["is_dis"];
		if($is_dis == 1){
			$discount_percentage = $_POST["discount_percentage"];
			$discount_price = $_POST["discount_price"];
		}else{
			$discount_percentage = "0";
			$discount_price = "0";
		}
		$pro_qty = $_POST["pro_qty"];
		$pro_qty_o = $_POST["pro_qty_o"];
		$pro_satus = $_POST["is_sta"];
		
		$pro_material = $_POST["pro_material"];
		$pro_size = $_POST["pro_size"];
		$pro_finish = $_POST["pro_finish"];
		$pro_variation = $_POST["pro_variation"];
		
		$file1 = $_FILES["file"]["name"];
		$file2 = $_FILES["file-2"]["name"];
		$file3 = $_FILES["file-3"]["name"];
		$file4 = $_FILES["file-4"]["name"];
		define("UPLOAD_DIR", "../images/products_images/");
		if($file1 !=""){
		/*********** For Image 1 *************/
		
		$fileName = $_FILES["file"]["name"]; // The file name
		$fileTmpLoc = $_FILES["file"]["tmp_name"]; // File in the PHP tmp folder
		$fileType = $_FILES["file"]["type"]; // The type of file it is
		$fileSize = $_FILES["file"]["size"]; // File size in bytes
		$fileErrorMsg = $_FILES["file"]["error"]; // 0 for false... and 1 for true
    // process file upload
	if($_SERVER["REQUEST_METHOD"] == "POST" && !empty($_FILES['file'])) {
    $myFile = $_FILES['file'];
    // verify the file type
    $fileType = exif_imagetype($_FILES['file']["tmp_name"]);
    $allowed = array(IMAGETYPE_GIF, IMAGETYPE_JPEG, IMAGETYPE_PNG);
    if (!in_array($fileType, $allowed)) {
        ?>
		<script>alert("File Type Error");
		</script>
		<?php
        exit;
    }

    // ensure a safe filename
    $name = preg_replace("/[^A-Z0-9._-]/i", "_", $myFile["name"]);
    // don't overwrite an existing file
    $i = 0;
    $parts = pathinfo($name);
    while (file_exists(UPLOAD_DIR . $name)) {
        $i++;
        $name = $parts["filename"] . "-" . $i . "." . $parts["extension"];
    }
    // preserve file from temporary directory
    $success = move_uploaded_file($myFile["tmp_name"], UPLOAD_DIR . $name);	
	}
	}else{
		$name = "0";
		
	}
	
	if($file2 !=""){
	/************ For Image 2 **************/
	$fileName = $_FILES["file-2"]["name"]; // The file name
	$fileTmpLoc = $_FILES["file-2"]["tmp_name"]; // File in the PHP tmp folder
	$fileType = $_FILES["file-2"]["type"]; // The type of file it is
	$fileSize = $_FILES["file-2"]["size"]; // File size in bytes
	$fileErrorMsg = $_FILES["file-2"]["error"]; // 0 for false... and 1 for true
    // process file upload
	if($_SERVER["REQUEST_METHOD"] == "POST" && !empty($_FILES['file-2'])) {
    $myFile = $_FILES['file'];
    // verify the file type
    $fileType = exif_imagetype($_FILES['file-2']["tmp_name"]);
    $allowed = array(IMAGETYPE_GIF, IMAGETYPE_JPEG, IMAGETYPE_PNG);
    if (!in_array($fileType, $allowed)) {
        ?>
		<script>alert("File Type Error");
		</script>
		<?php
        exit;
    }

    // ensure a safe filename
    $name1 = preg_replace("/[^A-Z0-9._-]/i", "_", $myFile["name"]);
    // don't overwrite an existing file
    $i = 0;
    $parts = pathinfo($name1);
    while (file_exists(UPLOAD_DIR . $name1)) {
        $i++;
        $name1 = $parts["filename"] . "-" . $i . "." . $parts["extension"];
    }
    // preserve file from temporary directory
    $success = move_uploaded_file($myFile["tmp_name"], UPLOAD_DIR . $name1);	
	}
	}else{
		$name1 = "0";
		
	}
	
	
	if($file3 !=""){
	
	/************ For Image 3 *************/
	$fileName = $_FILES["file-3"]["name"]; // The file name
	$fileTmpLoc = $_FILES["file-3"]["tmp_name"]; // File in the PHP tmp folder
	$fileType = $_FILES["file-3"]["type"]; // The type of file it is
	$fileSize = $_FILES["file-3"]["size"]; // File size in bytes
	$fileErrorMsg = $_FILES["file-3"]["error"]; // 0 for false... and 1 for true
    // process file upload
	if($_SERVER["REQUEST_METHOD"] == "POST" && !empty($_FILES['file-3'])) {
    $myFile = $_FILES['file-3'];
    // verify the file type
    $fileType = exif_imagetype($_FILES['file-3']["tmp_name"]);
    $allowed = array(IMAGETYPE_GIF, IMAGETYPE_JPEG, IMAGETYPE_PNG);
    if (!in_array($fileType, $allowed)) {
        ?>
		<script>alert("File Type Error");
		</script>
		<?php
        exit;
    }

    // ensure a safe filename
    $name3 = preg_replace("/[^A-Z0-9._-]/i", "_", $myFile["name"]);
    // don't overwrite an existing file
    $i = 0;
    $parts = pathinfo($name3);
    while (file_exists(UPLOAD_DIR . $name3)) {
        $i++;
        $name3 = $parts["filename"] . "-" . $i . "." . $parts["extension"];
    }
    // preserve file from temporary directory
    $success = move_uploaded_file($myFile["tmp_name"], UPLOAD_DIR . $name3);	
	}
	}else{
		$name3 = "0";
		
	}
	
	
	if($file4 !=""){
	/************** For Image 4 ***************/

	$fileName = $_FILES["file-4"]["name"]; // The file name
	$fileTmpLoc = $_FILES["file-4"]["tmp_name"]; // File in the PHP tmp folder
	$fileType = $_FILES["file-4"]["type"]; // The type of file it is
	$fileSize = $_FILES["file-4"]["size"]; // File size in bytes
	$fileErrorMsg = $_FILES["file-4"]["error"]; // 0 for false... and 1 for true
    // process file upload
	if($_SERVER["REQUEST_METHOD"] == "POST" && !empty($_FILES['file-4'])) {
    $myFile = $_FILES['file'];
    // verify the file type
    $fileType = exif_imagetype($_FILES['file-4']["tmp_name"]);
    $allowed = array(IMAGETYPE_GIF, IMAGETYPE_JPEG, IMAGETYPE_PNG);
    if (!in_array($fileType, $allowed)) {
        ?>
		<script>alert("File Type Error");
		</script>
		<?php
        exit;
    }

    // ensure a safe filename
    $name4 = preg_replace("/[^A-Z0-9._-]/i", "_", $myFile["name"]);
    // don't overwrite an existing file
    $i = 0;
    $parts = pathinfo($name4);
    while (file_exists(UPLOAD_DIR . $name4)) {
        $i++;
        $name4 = $parts["filename"] . "-" . $i . "." . $parts["extension"];
    }
    // preserve file from temporary directory
    $success = move_uploaded_file($myFile["tmp_name"], UPLOAD_DIR . $name4);	
	}
	}else{
		$name4 = "0";
		
	}
	$ra_check = "INSERT INTO `products_db`(`pro_id`, `pro_c_id`, `pro_name`, `pro_details`, `main_category`, `sub_category`, `pro_parent_id`, `pro_price`, `is_discount`, `discount_price`,`discount_perc`, `pro_image_1`, `pro_image_2`, `pro_image_3`, `pro_image_4`,`handle_finishes`, `handle_sizes`, `material`,`pro_qty`, `pro_qty_order_per_user`, `status`) VALUES ('','$pro_variation','$product_name','$product_desc','$pro_main_catg','$pro_sub_catg','$pro_variation','$pro_price','$is_dis','$discount_price','$discount_percentage','$name','$name1','$name3','$name4','$pro_finish','$pro_size','$pro_material','$pro_qty','$pro_qty_o','$pro_satus')";
	$q_check = mysqli_query($conn,$ra_check);
	if($q_check){
		?><script>alert("Variable Product has been Added Successfully");</script><?php
	}else{
		?><script>alert("Sorry! Something Went Wrong with Interent Connection");</script><?php
	}		
			
		}
		?>
     
<?php site_footer(); ?>
<script>
    $('.select2').select2();
</script>
	<script src="plugins/common/common.min.js"></script>
    <script src="js/custom.min.js"></script>
    <script src="js/settings.js"></script>
    <script src="js/gleek.js"></script>
    <script src="js/styleSwitcher.js"></script>
</body>

</html>
<?php } else{
	
	header("location:index.php");
} ?>