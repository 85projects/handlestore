<?php 
ob_start();
session_start(); 
include_once("includes/main_include.php"); ?>
<!DOCTYPE html>
<html class="h-100" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>The Handle Store | Login</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../../assets/images/favicon.png">
    <!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous"> -->
    <link href="css/style.css" rel="stylesheet">
	<style>
	body{
	 background-image: url('body_bg.jpg');
	 background-repeat: no-repeat;
     background-attachment: fixed;
	background-size: cover;}
	</style>
    
</head>

<body class="h-100">

    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
            </svg>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->
    <div class="login-form-bg h-100">
        <div class="container h-100">
            <div class="row justify-content-center h-100">
                <div class="col-xl-6">
                    <div class="form-input-content">
                        <div class="card login-form mb-0">
                            <div class="card-body pt-5">
                                <a class="text-center" href="index.php"> <h4>The Handle Store | Login</h4></a>
        
                                <form class="mt-5 mb-5 login-input" action="" method="post">
                                    <div class="form-group">
                                        <input type="email" name="email" class="form-control" placeholder="Email" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" name="password" class="form-control" placeholder="Password" required>
                                    </div>
                                    <button type="submit" name="submit" class="btn login-form__btn submit w-100">Sign In</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   <?php 
   if(isset($_POST["submit"])){
   $email = $_POST["email"];
   $password_1 = $_POST["password"];
   $flag = 0;
  $b1 ="SELECT * FROM `admin_handle_store` WHERE `email_id` = '$email'";
   $c2 = mysqli_query($conn,$b1);
   while($row = mysqli_fetch_array($c2))
   {   
		$flag = 1;
		$password = $row['password']; 
   }
   if($flag  == 1)
   {
	   if($password == $password_1){
		   
		   $_SESSION["ADMIN_LOGIN_09"] = "ADMIN_LOGIN_09";
		   header("location:add.php");
	   }else{ ?>
	 <script>alert("Email ID or Password Error"); </script>  
	   
  <?php }
   }else{ ?>
	 <script>alert("Email ID or Password Error"); </script>  
	   
   <?php } } ?>

    

    <!--**********************************
        Scripts
    ***********************************-->
    <script src="plugins/common/common.min.js"></script>
    <script src="js/custom.min.js"></script>
    <script src="js/settings.js"></script>
    <script src="js/gleek.js"></script>
    <script src="js/styleSwitcher.js"></script>
</body>
</html>





