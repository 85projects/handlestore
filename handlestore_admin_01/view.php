<?php 
ob_start();
session_start();
include_once("template-parts/footer.php"); 
include_once("template-parts/header.php"); 
include_once("includes/main_include.php"); 
if(isset($_SESSION["ADMIN_LOGIN_09"]) && $_SESSION["ADMIN_LOGIN_09"] !=""){
?>
<link href="invoice.css" rel="stylesheet" />
<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet" />
<link href="https://fonts.googleapis.com/css2?family=Noto+Serif&display=swap" rel="stylesheet">
<style>
body{
	font-family:Noto Serif;
}
</style>
<div id="invoice">
<?php if(isset($_GET["order_id"])){ 
$order_id = $_GET["order_id"];
$order = 1000;
								$b1 ="SELECT * FROM `cust_order` WHERE `order_id` = '$order_id'";
								$c2 = mysqli_query($conn,$b1);
								while($row = mysqli_fetch_array($c2))
								{   
									$order_id = $row['order_id'];
									$user_id = $row['user_id'];									
									$payment_option = $row['payment_option'];
									$total_amount = $row['total_amount'];
									$order_note = $row['order_note'];
									$order_date = $row['order_date'];
									$order = $order + $order_id;
									$b11 ="SELECT * FROM `user_sub_details` WHERE `user_id` = '$user_id'";
									$c21 = mysqli_query($conn,$b11);
									while($row = mysqli_fetch_array($c21))
									{
										$cust_name = $row['cust_name'];
										$country = $row['country'];									
										$address = $row['address'];
										$town = $row['town'];
										$pin_code= $row['pin_code'];
										$company_name = $row['company_name'];
										$contact_no = $row['contact_no'];
									
?>
    <div class="toolbar hidden-print">
        <div class="text-right">
            <button id="printInvoice" class="btn btn-info"><i class="fa fa-print"></i> Print</button>
            <button class="btn btn-info"><i class="fa fa-file-pdf-o"></i> Export as PDF</button>
        </div>
        <hr>
    </div>
    <div class="invoice overflow-auto">
        <div style="min-width: 600px">
            <header>
                <div class="row">
                    <div class="col-md-12 vt">
                        <a target="_blank" href="#">
                            <img src="handle_logo.png" data-holder-rendered="true" height="100px" />
                        </a>
                    </div>
                    <div class="col-md-12 company-details">
                        <h2 class="name"> 
                            The Handlestore
                        </h2>
                        <div><?php echo $company_name ?></div>
                        <div><?php echo $address ?></div>
						<div><?php echo $town  ?></div>
						<div><?php echo $pin_code ?></div>
						<div><?php echo $contact_no ?></div>
                    </div>
                </div>
            </header>
            <main>
                <div class="row contacts">
                    <div class="col invoice-to">
                        <div class="text-gray-light">INVOICE TO:</div>
                        <h2 class="to"><?php echo $cust_name ?></h2>
                        <div class="address"><?php echo $address ?><br><?php echo $town  ?><br><?php echo $pin_code ?><br><?php echo $contact_no ?></div>
                        <div class="email"><a href="#"><?php echo $user_id ?></a></div>
                    </div>
                    <div class="col invoice-details">
                        <h1 class="invoice-id">INVOICE <?php echo $order ?></h1>
                        <div class="date">Date of Invoice: <?php echo $order_date ?></div>
                    </div>
                </div>
                <table border="0" cellspacing="0" cellpadding="0">
                    <thead>
                        <tr>
                            <th>Sr.no</th>
                            <th class="text-left">Product</th>
                            <th class="text-right">Price</th>
                            <th class="text-right">Qty</th>
                            <th class="text-right">Total</th>
                        </tr>
                    </thead>
                    <tbody>
					<?php 			
					$i=0;
					$total = 0;
					$b11 ="SELECT * FROM `cust_order_data` WHERE `order_id`='$order_id'";
								    $c21 = mysqli_query($conn,$b11);
								    while($row = mysqli_fetch_array($c21))
								    {   
									$order_id = $row['order_id']; 
									$pro_price = $row['pro_price'];
									$pro_id = $row['pro_id'];
									$pro_qty = $row['pro_qty'];
									$pro_total_price = $row['pro_total_price']; 
									$total_price = array($row["pro_total_price"]);
									$values = array_sum($total_price);
									$total+=$values;
									
											$b12 ="SELECT * FROM `products_db` WHERE `pro_id`='$pro_id'";
											$c22 = mysqli_query($conn,$b12);
											while($row = mysqli_fetch_array($c22))
											{
												$i = $i+1;
												$pro_name = $row['pro_name']; 
												$handle_finishes = $row['handle_finishes'];
												$handle_sizes = $row['handle_sizes'];
												$material = $row['material'];
												
												$bw ="SELECT * FROM `handle_finishes` WHERE `fin_id` = '$handle_finishes'";
												$cw = mysqli_query($conn,$bw);
												while($row = mysqli_fetch_array($cw))
												{   
													$finish_name = $row['finish_name']; 
												}
												$bq ="SELECT * FROM `handle_sizes` WHERE `size_id` = '$handle_sizes'";
												$cq = mysqli_query($conn,$bq);
												while($row = mysqli_fetch_array($cq))
												{   
													$size_name = $row['size_name']; 
												}
												$bm ="SELECT * FROM `material` WHERE `mat_id` = '$material'";
												$cm = mysqli_query($conn,$bm);
												while($row = mysqli_fetch_array($cm))
												{   
													$material_name = $row['material_name']; 
												}
									?>
                        <tr>
                            <td class="no"><?php echo $i ?></td>
                            <td class="text-left"><h3><?php echo $pro_name ?></h3><small><?php echo $finish_name ?> - <?php echo $size_name ?> - <?php echo $material_name ?></td>
                            <td class="unit">&#x20B9; <?php echo $pro_price ?></td>
                            <td class="qty"><?php echo $pro_qty ?></td>
                            <td class="total">&#x20B9; <?php echo $pro_total_price ?></td>
                        </tr>
									<?php } }?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="2"></td>
                            <td colspan="2">GRAND TOTAL</td>
                            <td>&#x20B9; <?php echo $total ?></td>
                        </tr>
                    </tfoot>
                </table>
                <div class="notices">
                    <div>Order Note:</div>
                    <div class="notice"><?php echo $order_note ?></div>
                </div>
            </main>
            <footer>
                Invoice was created on a computer and is valid without the signature and seal.<br><br>
				<form name="" method="post">
				<input type="hidden" name="email" value="<?php echo $user_id ?>">
				<input type="hidden" name="order_id" value="<?php echo $order_id ?>">
				<button type="submit" name="confirm" class="btn btn-warning">Confirm</button>&nbsp;<button type="submit" name="reject" class="btn btn-danger">Reject</button>&nbsp;<button type="submit" name="complete" class="btn btn-success">Complete</button>
            </footer>
        </div>
        <div></div>
    </div>
								<?php } }  ?>
</div>
<?php
if(isset($_POST["confirm"])){
	
	$email = $_POST["email"];
	$order_id = $_POST["order_id"];
	$name = 'The Handle Store';
	$to = $email;
	$subject = 'Order confirmation message';
	$message = "Hi, Your Order has been Confirmed. Our representive will call you soon.";
	$from = "From: <<?php echo $name ?>>";
	$result = mail($to,$subject,$message,$from);
	if($result){
		$query ="UPDATE `cust_order` SET `order_status`='1' WHERE `order_id` = '$order_id'";
		$c22 = mysqli_query($conn,$query); 
		if($c22){ ?>
			<script>alert("Order Has been Confirmed");</script>
		<?php }
	}
}else if(isset($_POST["reject"])){

		$query ="UPDATE `cust_order` SET `order_status`='3' WHERE `order_id` = '$order_id'";
		$c22 = mysqli_query($conn,$query); 
		if($c22){ ?>
			<script>alert("Order Has been Rejected");</script>
		<?php }

}else if(isset($_POST["complete"])){

		$query ="UPDATE `cust_order` SET `order_status`='2' WHERE `order_id` = '$order_id'";
		$c22 = mysqli_query($conn,$query); 
		if($c22){ ?>
			<script>alert("Order Has been Completed");</script>
		<?php }

}
?>
<script src="plugins/common/common.min.js"></script>
    <script src="js/custom.min.js"></script>
    <script src="js/settings.js"></script>
    <script src="js/gleek.js"></script>
    <script src="js/styleSwitcher.js"></script>
</body>

</html>
<?php }else{ ?>
<script>alert("Something Went Wrong");</script>
<?php	
}
} else{
	header("location:index.php");
} ?>