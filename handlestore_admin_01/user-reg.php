<?php 
session_start();
include_once("template-parts/footer.php"); 
include_once("template-parts/header.php"); 
include_once("includes/main_include.php"); 
if(isset($_SESSION["ADMIN_LOGIN_09"]) && $_SESSION["ADMIN_LOGIN_09"] !=""){
echo header_main(); 
 ?>
        <div class="content-body">

            <div class="row page-titles mx-0">
                <div class="col p-md-0">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">All Orders</a></li>
                    </ol>
                </div>
            </div>
            <!-- row -->

            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Data Table</h4>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered zero-configuration">
                                        <thead>
                                            <tr>
											    <th>User Name</th>
                                                <th>Email</th>
                                                <th>Address</th>
                                                <th>Contact No</th>
                                                <th>Reg On</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
											<?php 
								$b1 ="SELECT * FROM `user_db` WHERE 1";
								$c2 = mysqli_query($conn,$b1);
								while($row = mysqli_fetch_array($c2))
								{   
									$email = $row['email'];
									$join_date = $row['join_date'];
									$b11 ="SELECT * FROM `user_sub_details` WHERE `user_id` = '$email'";
									$c21 = mysqli_query($conn,$b11);
									while($row = mysqli_fetch_array($c21))
									{
										$cust_name = $row['cust_name'];
										$country = $row['country'];									
										$address = $row['address'];
										$town = $row['town'];
										$pin_code= $row['pin_code'];
										$company_name = $row['company_name'];
										$contact_no = $row['contact_no'];
									?>
                                            <tr>
                                                <td><?php echo $cust_name ?></td>
                                                <td><?php echo $email ?></td>
                                                <td>Company Name: <?php echo $company_name ?><br>Address: <?php echo $address ?><br>City/Town: <?php echo $town ?><br>Country: <?php echo $country ?></td>
                                                <td><?php echo $contact_no ?></td>
                                                <td><?php echo $join_date ?></td>
												
                                            </tr>
                                           
								<?php } }?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>User Name</th>
                                                <th>Email</th>
                                                <th>Address</th>
                                                <th>Contact No</th>
                                                <th>Reg On</th>
                                                
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
       <?php site_footer(); ?>
	   <?php if(isset($_GET["del"])){
		   $pro_id = $_GET["del"];
		   $ra_check = "DELETE FROM `products_db` WHERE `pro_id` = '$pro_id'";
		   $q_check = mysqli_query($conn,$ra_check);
			if($q_check){
				?><script>alert('Alert For your User!');location.href = 'list.php';;</script><?php
			}
		   
	   } ?>
	   <script src="plugins/common/common.min.js"></script>
    <script src="js/custom.min.js"></script>
    <script src="js/settings.js"></script>
    <script src="js/gleek.js"></script>
    <script src="js/styleSwitcher.js"></script>
<script src="./plugins/tables/js/jquery.dataTables.min.js"></script>
    <script src="./plugins/tables/js/datatable/dataTables.bootstrap4.min.js"></script>
    <script src="./plugins/tables/js/datatable-init/datatable-basic.min.js"></script>
</body>

</html>
<?php } else{
	
	header("location:index.php");
}?>