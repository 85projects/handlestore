<?php 
ob_start();
session_start();
include_once("template-parts/footer.php"); 
include_once("template-parts/header.php"); 
include_once("includes/main_include.php"); 
if(isset($_SESSION["ADMIN_LOGIN_09"]) && $_SESSION["ADMIN_LOGIN_09"] !=""){
echo header_main(); 
 ?>
        <div class="content-body">

            <div class="row page-titles mx-0">
                <div class="col p-md-0">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Order Details</a></li>
						
                    </ol>
                </div>
            </div>
            <!-- row -->
			<form name="form1" action="" method="post" enctype="multipart/form-data">
            
<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body p-0">
                    <div class="row p-5">
                        <div class="col-md-6">
                            <img src="http://via.placeholder.com/400x90?text=logo">
                        </div>

                        <div class="col-md-6 text-right">
                            <p class="font-weight-bold mb-1">Invoice #550</p>
                            <p class="text-muted">Due to: 4 Dec, 2019</p>
                        </div>
                    </div>

                    <hr class="my-5">

                    <div class="row pb-5 p-5">
                        <div class="col-md-6">
                            <p class="font-weight-bold mb-4">Client Information</p>
                            <p class="mb-1">John Doe, Mrs Emma Downson</p>
                            <p>Acme Inc</p>
                            <p class="mb-1">Berlin, Germany</p>
                            <p class="mb-1">6781 45P</p>
                        </div>

                        <div class="col-md-6 text-right">
                            <p class="font-weight-bold mb-4">Payment Details</p>
                            <p class="mb-1"><span class="text-muted">VAT: </span> 1425782</p>
                            <p class="mb-1"><span class="text-muted">VAT ID: </span> 10253642</p>
                            <p class="mb-1"><span class="text-muted">Payment Type: </span> Root</p>
                            <p class="mb-1"><span class="text-muted">Name: </span> John Doe</p>
                        </div>
                    </div>

                    <div class="row p-5">
                        <div class="col-md-12">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th class="border-0 text-uppercase small font-weight-bold">ID</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Item</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Description</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Quantity</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Unit Cost</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Software</td>
                                        <td>LTS Versions</td>
                                        <td>21</td>
                                        <td>$321</td>
                                        <td>$3452</td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>Software</td>
                                        <td>Support</td>
                                        <td>234</td>
                                        <td>$6356</td>
                                        <td>$23423</td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>Software</td>
                                        <td>Sofware Collection</td>
                                        <td>4534</td>
                                        <td>$354</td>
                                        <td>$23434</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="d-flex flex-row-reverse bg-dark text-white p-4">
                        <div class="py-3 px-5 text-right">
                            <div class="mb-2">Grand Total</div>
                            <div class="h2 font-weight-light">$234,234</div>
                        </div>

                        <div class="py-3 px-5 text-right">
                            <div class="mb-2">Discount</div>
                            <div class="h2 font-weight-light">10%</div>
                        </div>

                        <div class="py-3 px-5 text-right">
                            <div class="mb-2">Sub - Total amount</div>
                            <div class="h2 font-weight-light">$32,432</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
   

</div>



		    </form>
        </div>
		<?php
		if(isset($_POST["submit"])){
			
		$product_name = $_POST["product_name_1"];
		$product_desc = $_POST["product_desc"];
		$pro_main_catg = $_POST["pro_main_catg"];
		$pro_sub_catg = $_POST["pro_sub_catg"];
		$pro_price = $_POST["pro_price"];
		$is_dis = $_POST["is_dis"];
		if($is_dis == 1){
			$discount_percentage = $_POST["discount_percentage"];
			$discount_price = $_POST["discount_price"];
		}else{
			$discount_percentage = "0";
			$discount_price = "0";
		}
		$pro_qty = $_POST["pro_qty"];
		$pro_qty_o = $_POST["pro_qty_o"];
		$pro_satus = $_POST["is_sta"];
		
		$pro_material = $_POST["pro_material"];
		$pro_size = $_POST["pro_size"];
		$pro_finish = $_POST["pro_finish"];
		$pro_variation = $_POST["pro_variation"];
		
		define("UPLOAD_DIR", "../images/products_images/");
		$fileName = $_FILES["file"]["name"]; // The file name
		$fileTmpLoc = $_FILES["file"]["tmp_name"]; // File in the PHP tmp folder
		$fileType = $_FILES["file"]["type"]; // The type of file it is
		$fileSize = $_FILES["file"]["size"]; // File size in bytes
		$fileErrorMsg = $_FILES["file"]["error"]; // 0 for false... and 1 for true
// process file upload
	if($_SERVER["REQUEST_METHOD"] == "POST" && !empty($_FILES['file'])) {
    $myFile = $_FILES['file'];
    // verify the file type
    $fileType = exif_imagetype($_FILES['file']["tmp_name"]);
    $allowed = array(IMAGETYPE_GIF, IMAGETYPE_JPEG, IMAGETYPE_PNG);
    if (!in_array($fileType, $allowed)) {
        ?>
		<script>alert("File Type Error");
		</script>
		<?php
        exit;
    }

    // ensure a safe filename
    $name = preg_replace("/[^A-Z0-9._-]/i", "_", $myFile["name"]);
    // don't overwrite an existing file
    $i = 0;
    $parts = pathinfo($name);
    while (file_exists(UPLOAD_DIR . $name)) {
        $i++;
        $name = $parts["filename"] . "-" . $i . "." . $parts["extension"];
    }
    // preserve file from temporary directory
    $success = move_uploaded_file($myFile["tmp_name"], UPLOAD_DIR . $name);	
	}
	$ra_check = "INSERT INTO `products_db`(`pro_id`, `pro_name`, `pro_details`, `main_category`, `sub_category`, `pro_parent_id`, `pro_price`, `is_discount`, `discount_price`,`discount_perc`, `pro_image_1`, `pro_image_2`, `pro_image_3`, `pro_image_4`,`handle_finishes`, `handle_sizes`, `material`,`pro_qty`, `pro_qty_order_per_user`, `status`) VALUES ('','$product_name','$product_desc','$pro_main_catg','$pro_sub_catg','$pro_variation','$pro_price','$is_dis','$discount_price','$discount_percentage','$name','0','0','0','$pro_finish','$pro_size','$pro_material','$pro_qty','$pro_qty_o','$pro_satus')";
	$q_check = mysqli_query($conn,$ra_check);
	if($q_check){
		?><script>alert("Variable Product has been Added Successfully");</script><?php
	}else{
		?><script>alert("Sorry! Something Went Wrong with Interent Connection");</script><?php
	}		
			
		}
		?>
     
<?php site_footer(); ?>

	<script src="plugins/common/common.min.js"></script>
    <script src="js/custom.min.js"></script>
    <script src="js/settings.js"></script>
    <script src="js/gleek.js"></script>
    <script src="js/styleSwitcher.js"></script>
</body>

</html>
<?php } else{
	
	header("location:index.php");
} ?>