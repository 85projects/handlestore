<?php 
ob_start();
session_start();
include_once("template-parts/footer.php"); 
include_once("template-parts/header.php"); 
include_once("includes/main_include.php"); 
if(isset($_SESSION["ADMIN_LOGIN_09"]) && $_SESSION["ADMIN_LOGIN_09"] !=""){
echo header_main(); 
 ?>
 <style>
 .select2-container .select2-selection--single{
    height:40px !important;
}
.select2-container--default .select2-selection--single{
         border: 1px solid #ccc !important; 
     border-radius: 5px !important; 
	 padding:5px;
}
.top-x{
margin-top:20px}

 </style>
        <div class="content-body">

            <div class="row page-titles mx-0">
                <div class="col p-md-0">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Add Products</a></li>
						
                    </ol>
                </div>
            </div>
			<form name="form1" action="" method="post">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Select Product to be Updated</h4>
								<select name="pro_id" class="form-control select2" required>
									<option value="">Select Products</option>
									<?php	$b1 ="SELECT * FROM `products_db` WHERE 1";
												$c2 = mysqli_query($conn,$b1);
												while($row = mysqli_fetch_array($c2))
												{   $pro_id = $row['pro_id'];
													$pro_name = $row['pro_name']; 
													
										?>
                                                <option value="<?php echo $pro_id ?>"><?php echo $pro_name ?></option>
												<?php } ?>
								</select><br>
								<div class="top-x">
								<button type="submit" name="p_submit" class="btn btn-dark mb-2 pull-right">Search Product</button></div>
							</div>
							
						</div>
					</div>
					<div class="col-lg-6">
                               
					</div>
				</div>
			</div>
			</form>
            <!-- row -->
			<?php if(isset($_POST["p_submit"])){ 
			$pro_id = $_POST["pro_id"];
			$b1 ="SELECT * FROM `products_db` WHERE `pro_id` = '$pro_id'";
												$c2 = mysqli_query($conn,$b1);
												while($row = mysqli_fetch_array($c2))
												{   $pro_id = $row['pro_id'];
													$pro_name = $row['pro_name']; 
													$pro_details = $row['pro_details'];
													$main_category = $row['main_category'];
													$sub_category = $row['sub_category'];
													$pro_price = $row['pro_price'];
													$is_discount = $row['is_discount'];
													$discount_price = $row['discount_price'];
													$discount_perc = $row['discount_perc'];
													$pro_qty = $row['pro_qty'];
													$pro_qty_order_per_user = $row['pro_qty_order_per_user'];
													$status = $row['status'];
													$b11 ="SELECT * FROM `pro_main_category` WHERE `cat_id` = '$main_category'";
									$c21 = mysqli_query($conn,$b11);
									while($row = mysqli_fetch_array($c21))
									{   
										$cat_name = $row['cat_name']; 
									}
									$b12 ="SELECT * FROM `pro_sub_category` WHERE `sub_id` = '$sub_category'";
									$c22 = mysqli_query($conn,$b12);
									while($row = mysqli_fetch_array($c22))
									{   
										$pro_category = $row['cat_name']; 
									}
													
												
			?>
			<form name="form1" action="" method="post" enctype="multipart/form-data">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Product Details</h4>
                                <p class="text-muted m-b-15 f-s-12">Add Product Name<code> and product Description Here</code></p>
                                <div class="basic-form">
                                        <div class="form-group">
										<input type="hidden" name="pro_id" value="<?php echo $pro_id ?>">
                                            <input type="text" name="product_name_1" value="<?php echo $pro_name?>" class="form-control input-default" placeholder="Enter Product Name">
                                        </div>
                                        <div class="form-group">
                                            <textarea type="text" value="<?php echo $pro_details ?>" name="product_desc" class="form-control input-flat" rows="5" placeholder="Enter Product Description"></textarea>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Product Sub Details</h4>
                                <div class="basic-form">
                                        <div class="form-group">
                                            <label>Choose Product Main Category:</label>
                                            <select class="form-control" name="pro_main_catg" id="sel1">
											<option value="<?php echo $main_category ?>"><?php echo $cat_name ?></option>
										<?php	$b1 ="SELECT * FROM `pro_main_category` WHERE 1";
												$c2 = mysqli_query($conn,$b1);
												while($row = mysqli_fetch_array($c2))
												{   $cat_id = $row['cat_id'];
													$cat_name = $row['cat_name']; 
													
										?>
                                                <option value="<?php echo $cat_id ?>"><?php echo $cat_name ?></option>
												<?php } ?>
                                            </select>
                                        </div>
										<div class="form-group">
                                            <label>Choose Sub Product Sub Category:</label>
                                            <select class="form-control" name="pro_sub_catg" id="sel1">
										<?php	$b1 ="SELECT `sub_id`, `cat_name`, `main_cat_id`, `status` FROM `pro_sub_category` WHERE 1";
												$c2 = mysqli_query($conn,$b1);
												while($row = mysqli_fetch_array($c2))
												{   
													$sub_id = $row['sub_id']; 
													$cat_name = $row['cat_name'];
										?>
                                                <option value="<?php echo $pro_category ?>"><?php echo $sub_category ?></option>
												<?php } ?>
                                            </select>
                                        </div>
										<div class="form-group">
                                            <input type="number"  value="<?php echo $pro_price ?>"  id="cBalance" name="pro_price" class="form-control input-default" placeholder="Enter Product Price">
                                        </div>
										<div class="form-group">
                                            <label>Is Discount:</label>
                                            <select class="form-control" name="is_dis" id="sel1">
												<option value="<?php echo $is_discount ?>"><?php if($is_discount == 0){echo "No";}else{echo "Yes";} ?></option>
                                                <option value="0">No</option>
                                                <option value="1">Yes</option>
                                            </select>
                                        </div>
										<div class="form-group">
                                            <input type="number" value="<?php echo $discount_perc ?>" id="chDiscount" name="discount_percentage" class="form-control input-default" placeholder="Discount Percentage">
                                        </div>
										<div class="form-group">
                                            <input type="number" value="<?php echo $discount_price ?>" id="result_k" name="discount_price" class="form-control input-default" placeholder="Discount Price">
                                        </div>
										<script>
										 $(document).on("change keyup blur", "#chDiscount", function() {
            var main = $('#cBalance').val();
            var disc = $('#chDiscount').val();
            var dec = (disc / 100).toFixed(2); //its convert 10 into 0.10
            var mult = main * dec; // gives the value for subtract from main value
            var discont = main - mult;
            $('#result_k').val(discont);  });

										</script>
										<div class="form-group">
                                            <input type="number" value="<?php echo $pro_qty ?>"  name="pro_qty" class="form-control input-default" placeholder="Product Qty Avilable" required>
                                        </div><div class="form-group">
                                            <input type="number" value="<?php echo $pro_qty_order_per_user ?>" name="pro_qty_o" class="form-control input-default" placeholder="Product Qty Customer Can Order" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Product Status:</label>
											
                                            <select class="form-control" name="is_sta" id="sel1">
                                                <option value="<?php echo $status ?>"><?php if($status == 0){echo "Not Availabele";}else{echo "Available";} ?></option>
                                                <option value="1">Available</option>
                                                <option value="0">Not Available</option>
                                            </select>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
-   <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                               <!--   <h4 class="card-title">Product Image</h4>
                               <div class="basic-form">
                                        <div class="form-group">
                                            <input type="file" name="file" id="file-1" class="inputfile inputfile-1 form-control-file">
                                        </div>
                                </div> -->
								<br>
							<button type="submit" name="submit" class="btn btn-dark mb-2">Update Product</button>
                            </div>
                        </div>
						
                    </div>

                    </div>
                </div>
				</form>
			<?php } }?>
            </div>
        </div>
		<?php
		if(isset($_POST["submit"])){
			
		$product_name = $_POST["product_name_1"];
		$product_desc = $_POST["product_desc"];
		$pro_main_catg = $_POST["pro_main_catg"];
		$pro_sub_catg = $_POST["pro_sub_catg"];
		$pro_price = $_POST["pro_price"];
		$is_dis = $_POST["is_dis"];
		if($is_dis == 1){
			$discount_percentage = $_POST["discount_percentage"];
			$discount_price = $_POST["discount_price"];
		}else{
			$discount_percentage = "0";
			$discount_price = "0";
		}
		$pro_qty = $_POST["pro_qty"];
		$pro_qty_o = $_POST["pro_qty_o"];
		$pro_satus = $_POST["is_sta"];		
	$ra_check = "UPDATE `products_db` SET `pro_name`='$product_name',`pro_details`='$product_desc',`main_category`='$pro_main_catg',`sub_category`='$pro_sub_catg',`variation_id`='0',`pro_price`='$pro_price',`is_discount`='$is_dis',`discount_price`='$discount_price',`discount_perc`='$discount_percentage',`pro_qty`='$pro_qty',`pro_qty_order_per_user`='$pro_qty_o',`status`='$pro_satus' WHERE 1";
	$q_check = mysqli_query($conn,$ra_check);
	if($q_check){
		?><script>alert("Product has been Updated Successfully");</script><?php
	}else{
		?><script>alert("Sorry! Something Went Wrong with Interent Connection");</script><?php
	}		
			
		}
		?>
     
<?php site_footer(); ?>
<script>
    $('.select2').select2();
</script>
	<script src="plugins/common/common.min.js"></script>
    <script src="js/custom.min.js"></script>
    <script src="js/settings.js"></script>
    <script src="js/gleek.js"></script>
    <script src="js/styleSwitcher.js"></script>
</body>

</html>
<?php } else{
	
	header("location:index.php");
} ?>