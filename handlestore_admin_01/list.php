<?php 
session_start();
include_once("template-parts/footer.php"); 
include_once("template-parts/header.php"); 
include_once("includes/main_include.php"); 
if(isset($_SESSION["ADMIN_LOGIN_09"]) && $_SESSION["ADMIN_LOGIN_09"] !=""){
echo header_main(); 
 ?>
        <div class="content-body">

            <div class="row page-titles mx-0">
                <div class="col p-md-0">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Update</a></li>
                    </ol>
                </div>
            </div>
            <!-- row -->

            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Data Table</h4>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered zero-configuration">
                                        <thead>
                                            <tr>
											    <th>Product Image</th>
                                                <th>Product Name</th>
                                                <th>Product Description</th>
                                                <th>Product Category</th>
                                                <th>Product Sub Category</th>
                                                <th>Product Price</th>
                                                <th>Is Discount</th>
												<th>Discount Price</th>
												<th>Qty Avilable</th>
												<th>Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
											<?php 
								$b1 ="SELECT * FROM `products_db` WHERE 1";
								$c2 = mysqli_query($conn,$b1);
								while($row = mysqli_fetch_array($c2))
								{   
									$pro_id = $row['pro_id']; 
									$product_name = $row['pro_name'];
									$product_details = $row['pro_details'];
									$product_image = $row['pro_image_1'];
									$main_category = $row['main_category'];
									$sub_category = $row['sub_category'];
									$pro_price = $row['pro_price'];
									$is_discount = $row['is_discount'];
									$discount_price = $row['discount_price'];
									$discount_perc = $row['discount_perc'];
									$pro_qty = $row['pro_qty'];
									$b11 ="SELECT * FROM `pro_main_category` WHERE `cat_id` = '$main_category'";
									$c21 = mysqli_query($conn,$b11);
									while($row = mysqli_fetch_array($c21))
									{   
										$cat_name = $row['cat_name']; 
									}
									$b12 ="SELECT * FROM `pro_sub_category` WHERE `sub_id` = '$sub_category'";
									$c22 = mysqli_query($conn,$b12);
									while($row = mysqli_fetch_array($c22))
									{   
										$pro_category = $row['cat_name']; 
									}
									?>
                                            <tr>
                                                <td><img src="../images/products_images/<?php echo $product_image ?>" height="40px" width="40px"></td>
                                                <td><?php echo $product_name?></td>
                                                <td><?php echo $product_details ?></td>
                                                <td><?php echo $cat_name ?></td>
												<td><?php echo $pro_category ?></td>
                                                <td><?php echo $pro_price ?></td>
                                                <td><?php if($is_discount == 0){echo "No";}else{echo "Yes";} ?></td>
												<td><?php echo $discount_perc ?></td>
												<td><?php echo $discount_perc ?></td>
												<td><a href="list.php?del=<?php echo $pro_id ?>"><font color="red">Delete</font></a></td>
                                            </tr>
                                           
								<?php } ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                 <th>Product Image</th>
                                                <th>Product Name</th>
                                                <th>Product Description</th>
                                                <th>Product Category</th>
                                                <th>Product Sub Category</th>
                                                <th>Product Price</th>
                                                <th>Is Discount</th>
												<th>Discount Price</th>
												<th>Qty Avilable</th>
												<th>Delete</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
       <?php site_footer(); ?>
	   <?php if(isset($_GET["del"])){
		   $pro_id = $_GET["del"];
		   $ra_check = "DELETE FROM `products_db` WHERE `pro_id` = '$pro_id'";
		   $q_check = mysqli_query($conn,$ra_check);
			if($q_check){
				?><script>alert('Alert For your User!');location.href = 'list.php';;</script><?php
			}
		   
	   } ?>
	   <script src="plugins/common/common.min.js"></script>
    <script src="js/custom.min.js"></script>
    <script src="js/settings.js"></script>
    <script src="js/gleek.js"></script>
    <script src="js/styleSwitcher.js"></script>
<script src="./plugins/tables/js/jquery.dataTables.min.js"></script>
    <script src="./plugins/tables/js/datatable/dataTables.bootstrap4.min.js"></script>
    <script src="./plugins/tables/js/datatable-init/datatable-basic.min.js"></script>
</body>

</html>
<?php } else{
	
	header("location:index.php");
} ?>