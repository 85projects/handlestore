<?php
session_start(); 
error_reporting(0);
include_once("template-parts/header_links.php");
include_once("template-parts/navbar_m.php");
include_once("template-parts/footer.php");
include_once("includes/main_include.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
  <title>The Handle Store | Order Confirmation</title>
  <meta name="description" content="Best handle selling website">
  <meta name="author" content="Handle Store">
  <link href="https://fonts.googleapis.com/css2?family=Ramaraja&display=swap" rel="stylesheet">
  <script
  src="https://code.jquery.com/jquery-3.5.1.min.js"
  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
  crossorigin="anonymous"></script>
  <?php header_links(); ?>
</head>
<body>
  <style>
    button.filter-button.active:focus {
      background: white;
      color:black;
      border: 1px solid yellow;
    }
  </style>
  <section> 
    <header>
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <nav class="navbar navbar-expand-lg navbar-light">
              <a class="navbar-brand" href="index.php"><img src="logos/handle_logo_final.png" id="store_logo"></a>
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>

              <div class="collapse navbar-collapse" id="navbarSupportedContent">
  <!--      <form class="form-inline my-2 my-lg-0 ml-auto">
      <input class="form-control mr-sm-2" type="search" placeholder="Search for Handles,Knobs and More.." aria-label="Search">
    </form> -->
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="index.php">HOME<span class="sr-only">(current)</span></a>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          HANDLES
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Mortice Plates</a>
          <a class="dropdown-item" href="#">Mortice Rose</a>
          <a class="dropdown-item" href="#">Main Door Pull</a>
          <a class="dropdown-item" href="#">Cabinets</a>
          <a class="dropdown-item" href="#">Knobs</a>
          <a class="dropdown-item" href="#">Concealed</a>
          <a class="dropdown-item" href="#">Profile</a>
          <a class="dropdown-item" href="#">Glass door</a>
        </div>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          DOOR ACCESSORIES
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          BATH ACCESSORIES
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
    </ul>

    <form class="form-inline my-2 my-lg-0 ml-auto" id="my_header_form">
      <input class="form-control mr-sm-2" type="search" placeholder="Search for Handles,Knobs and More.." aria-label="Search">
      <button class="cart_btn"><img src="icons/cart_icon.png" id="cart_icon"> cart</button>
      <div class="mydropdown">
        <button class="login_btn"><img src="icons/login_icon.png"> LOGIN / SIGNUP</button>
        <div class="dropdown-content">
          <a href="#">Orders</a>
          <a href="#">Whishlist</a>
          <a href="#">Contact Us</a> 
          <hr style="padding: 0;margin: 0;" />   
          <a href="#" style="color: red;">Logout</a>
        </div>
      </div>
    </form>
  </div>
</nav>
</div>
</div>
</div>

</header>





<section>
  <div class="container-fluid" style="background-color: #eff7fa;padding: 60px 0px 60px 0px;">
    <div class="container2" style="padding: 10px 20px 10px 20px;background-color: #ffffff">



      <div class="row">
        <div class="col-md-4 text-center" style="padding: 40px 12px 160px 12px;">


          <h2>HI, Sai</h2>
          <p>Reg. Date Oct 05, 2020 09:22:17</p>


          <div class="tab form-control">
            <button class="tablinks" onclick="openCity(event, 'Profile')" id="defaultOpen"><i class="fas fa-user"></i>  Profile</button>
            <button class="tablinks" onclick="openCity(event, 'Address')"><i class="fas fa-list-ul"></i>  Update Address</button>
            <button class="tablinks" onclick="openCity(event, 'Orders')"><i class="fas fa-list-ul"></i>  My Orders</button>
            <button class="tablinks"><i class="fas fa-lock"></i>  Logout</button>
          </div>



        </div>
        <div class="col-md-8" style="padding: 25px 20px 50px 20px;border:1px solid #eef2f3; ">

         <div id="Profile" class="tabcontent">
          <h3>My profile</h3>             
          <form style="margin-top: 40px;">
            <input type="text" name="full_name" class="form-control" placeholder="Full Name" disabled>

          <div class="row" style="margin-top: 30px;">
            <div class="col-md-6">
              <input type="text" name="phone_number" class="form-control" placeholder="Phone Number*" disabled>
            </div>
            <div class="col-md-6">
              <input type="text" name="email_address" class="form-control" placeholder="Your Email*" disabled>
            </div>
          </div>
          <div class="row" style="margin-top: 30px;">
            <div class="col-md-6">
              <input type="text" name="country" class="form-control" placeholder="Country*" disabled>
            </div>
            <div class="col-md-6">
              <input type="text" name="city" class="form-control" placeholder="City*" disabled>
            </div>
          </div>
           <div class="row" style="margin-top: 30px;">
            <div class="col-md-6">
              <input type="text" name="zip_code" class="form-control" placeholder="Zip Code*">
            </div>
            <div class="col-md-6">
              <input type="text" name="state" class="form-control" placeholder="State*">
            </div>
          </div>
          <textarea class="form-control" style="margin-top: 30px;height: 130px;" disabled>Your Address</textarea>
         </form>
        </div>

        <div id="Address" class="tabcontent">
          <h3>Update Address</h3>
                    <form style="margin-top: 40px;">
            <input type="text" name="full_name" class="form-control" placeholder="Full Name" disabled>

          <div class="row" style="margin-top: 30px;">
            <div class="col-md-6">
              <input type="text" name="phone_number" class="form-control" placeholder="Phone Number*" disabled>
            </div>
            <div class="col-md-6">
              <input type="text" name="email_address" class="form-control" placeholder="Your Email*" disabled>
            </div>
          </div>
          <div class="row" style="margin-top: 30px;">
            <div class="col-md-6">
              <input type="text" name="country" class="form-control" placeholder="Country*" disabled>
            </div>
            <div class="col-md-6">
              <input type="text" name="city" class="form-control" placeholder="City*" disabled>
            </div>
          </div>
           <div class="row" style="margin-top: 30px;">
            <div class="col-md-6">
              <input type="text" name="zip_code" class="form-control" placeholder="Zip Code*">
            </div>
            <div class="col-md-6">
              <input type="text" name="state" class="form-control" placeholder="State*">
            </div>
          </div>
          <textarea class="form-control" style="margin-top: 30px;height: 130px;" disabled>Your Address</textarea>
         </form>
        </div>

        <div id="Orders" class="tabcontent">
          <h3>Order List</h3>
           <div class="row">
             <div class="col-md-12 text-center">
               <img src="icons/empty-cart.png">
             </div>
           </div>

        </div>


      </div>
    </div>







  </div>
</div>
</section>




<script>
  function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
  }

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>





<!-- Footer Starts Here -->
<?php site_footer(); ?>

</body>
</html>