<?php 
session_start(); 
include_once("template-parts/header_links.php");
include_once("template-parts/navbar_m.php");
include_once("template-parts/footer.php");
include_once("includes/main_include.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
  <title>The Handle Store | Main</title>
  <meta name="description" content="Best handle selling website">
  <meta name="author" content="Handle Store">
  <script src="js/sweetalert.min.js"></script>
  <link href="css/sweetalert.css" rel="stylesheet">
  <link href="css/login.css" rel="stylesheet">
  <?php header_links(); ?>
</head>
<body>
<!---------------------------HEADER AREA--------------------------------->
<?php bottom_menu(); ?>
<!---------------------------MAIN BANNER IMAGE ------------------------------------->
  <section>
    <div class="container gt">
      <div class="user signinBx">
        <div class="imgBx"><img src="background/offer_img.png" alt="" /></div>
        <div class="formBx">
          <form action="" method="post">
            <h2>Sign In</h2>
            <input type="text" name="email_20" placeholder="Enter Your Email Id" />
            <input type="password" name="password" placeholder="Password" />
            <input type="submit" name="login" value="Login" />
            <p class="signup">
              Don't have an account ?
              <a href="#" onclick="toggleForm();">Sign Up.</a>
            </p>
          </form>
        </div>
      </div>
      <div class="user signupBx">
        <div class="formBx">
          <form action="" method="post">
            <h2>Create an account</h2>
            <input type="text" name="name" placeholder="Enter Your Full Name" required/>
            <input type="email" name="email_20" id="email_id" onkeyup="checkemail();" placeholder="Enter Email Address" required/>
			<p id="m2" style="color:red;"></p>
            <input type="password" name="password" placeholder="Create Password" required/>
            <input type="submit" name="signup" value="Sign Up" />
            <p class="signup">
              Already have an account ?
              <a href="#" onclick="toggleForm();">Sign in.</a>
            </p>
          </form>
        </div>
        <div class="imgBx"><img src="background/arrival3.png" alt="" /></div>
      </div>
    </div>
  </section>
<!---------------------------FOOTER AREA--------------------------------->

<footer id="footer_area" style="background-color: #f5f6f8;">

	<div class="container">
		
    
    <div class="row">
    
    <div class="col-md-4" id="left_part_footer" >

    	       <a href="index.php"><img src="logos/handle_logo_final.png" alt="The hangle store" id="store_logo"></a>
    	<p>Aliquam sodales accumsan justo, at fringilla 
elit pulvinar cursus.Aliquam sodales accumsan justo, 
at fringilla elit pulvinar cursus.</p>

    </div>	
    <div class="col-md-2">
    	<ul>
    	<a href=""><li>MAIN DOOR</li></a>
    	<a href=""><li>KITCHEN</li></a>		
    	<a href=""><li>CABINET</li></a>	
    	<a href=""><li>KNOBS</li></a>	
    	<a href=""><li>PROFILES</li></a>	
    	<a href=""><li>MORTICE</li></a>	

    	</ul>
    </div>
    <div class="col-md-2">
    	<ul>
    	<a href=""><li>ABOUT US</li></a>
    	<a href=""><li>CONTACT US</li></a>		
    	<a href=""><li>PRIVACY POLICY</li></a>	
    	<a href=""><li>FAQ'S</li></a>	
    	<a href=""><li>WARRANTY</li></a>	
    	<a href=""><li>T&C</li></a>	
    	<a href=""><li>RETURN POLICY</li></a>	

    	</ul>
    </div>
    <div class="col-md-4"  id="social_media">
    	
    	<h4>Social Media</h4>

    	<div class="btn-group" id="button_group_social">
    		<button></button>
    		<button></button>
    		<button></button>
    		<button></button>
    	</div>


    </div>

    </div>


	</div>
	

</footer>
<script>
const toggleForm = () => {
  const container = document.querySelector('.gt');
  container.classList.toggle('active');
};
</script>
<script src="js/bootstrap.min.js"></script>
<?php
if(isset($_POST['signup']))
{    
	$name = $_POST['name'];
	$email_20 = $_POST['email_20'];
	$password = $_POST['password'];
	
	    $query = "SELECT * FROM `user_db` WHERE `email` = '$email_20'";
        $a = mysqli_query($conn,$query);
		$total = 0;
        while($row = mysqli_fetch_array($a))
	    { 
				$total=1;
				break;
	    }
	    if($total==1)
	    {  
				/*Email Already Exits*/?>
				<script>
					swal("Sign Up Failed!", "Email Id Already Exist", "error");
				</script><?php
	    }
	    else
		{
			/*Register User*/
			
			/*********** Password Generation ***********/
			
				function randoms($length = 8) {
                $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $string = '';

                 for ($i = 0; $i < $length; $i++) {
                 $string .= $characters[mt_rand(0, strlen($characters) - 1)];
                 }
                     return $string;
                }
                $random_p = randoms();
				$hash = sha1($password.$random_p);
				
			/********** Password Generation Ends ***********/
			
			
			/********* Check cookie Status ************/
			if(isset($_COOKIE["thehandlestore_AZwUghJKdP"])){
						/* Unset Cookie */
						setcookie("thehandlestore_AZwUghJKdP", "", time() - 3600);
						function getUserIpAddr(){
					    if(!empty($_SERVER['HTTP_CLIENT_IP'])){
                        //ip from share internet
						$ip = $_SERVER['HTTP_CLIENT_IP'];
						}elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
						//ip pass from proxy
						$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
						}else{
						$ip = $_SERVER['REMOTE_ADDR'];
						}
						return $ip;
						}
						do {
						$ip_address = getUserIpAddr();
						$random = rand(); 
						$csid=substr( md5($random.$ip_address),0,20);
						$_SESSION["CSID"]=$csid;
						
						$regenerateNumber = true;					
						$checkRegNum = "SELECT * FROM `user_db` WHERE `cart_id` = '$csid'";
						$result= mysqli_query($conn, $checkRegNum);
						if (mysqli_num_rows($result) == 0) {
						$regenerateNumber = false;
						setcookie("thehandlestore_AZwUghJKdP", "$csid", time()+(86400 * 30), "/","", 0);
						}
						} while ($regenerateNumber);
			
			}else{
						function getUserIpAddr(){
					    if(!empty($_SERVER['HTTP_CLIENT_IP'])){
                        //ip from share internet
						$ip = $_SERVER['HTTP_CLIENT_IP'];
						}elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
						//ip pass from proxy
						$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
						}else{
						$ip = $_SERVER['REMOTE_ADDR'];
						}
						return $ip;
						}
						do {
						$ip_address = getUserIpAddr();
						$random = rand(); 
						$csid=substr( md5($random.$ip_address),0,20);
						$_SESSION["CSID"]=$csid;
						
						$regenerateNumber = true;					
						$checkRegNum = "SELECT * FROM `user_db` WHERE `cart_id` = '$csid'";
						$result= mysqli_query($conn, $checkRegNum);
						if (mysqli_num_rows($result) == 0) {
						$regenerateNumber = false;
						setcookie("thehandlestore_AZwUghJKdP", "$csid", time()+(86400 * 30), "/","", 0);
						}
						} while ($regenerateNumber);
				
			}
			$date = date_default_timezone_set('Asia/Kolkata');
			$today = date("F j, Y, g:i a");	
			$query = "INSERT INTO `user_db`(`user_id`, `name`, `email`, `password`,`hash_code`, `cart_id`, `forgot_pass_code`, `join_date`, `status`) VALUES ('','$name','$email_20','$hash','$random_p','$cart_id','0','$today','1')";
			$ins = mysqli_query($conn,$query);
			if($ins)
			{   
		        
		        $_SESSION["thehandlestore_AZwUghJKdP"] = $email_20;
				?>
				<script>
				swal({
				  title: "Registration Success!",
				  text: "Redirecting You to Main Store!",
				  type: "success",
				  confirmButtonText: "OK"
				},
				function(isConfirm){
				  if (isConfirm) {
					window.location.href = "index.php";
				  }
				});
				</script><?php		
			}else{
			?>
				<script>
				swal({
				  title: "Something Went Wrong!",
				  text: "Please try agin!",
				  type: "error",
				  confirmButtonText: "OK"
				},
				function(isConfirm){
				  if (isConfirm) {
					window.location.href = "account-login.php";
				  }
				});
				</script><?php			
				
			}
	    }
}else if(isset($_POST['login'])){
	$sh_email_id = $_POST['email_20'];
	$sh_pass = $_POST['password'];
	$y=0;
	$pflag=0;
	$query = "SELECT * FROM `user_db` WHERE `email` = '$sh_email_id'";
	$q = mysqli_query($conn,$query);
	while($row = mysqli_fetch_array($q))
	{
	   $pflag=1;
	   $password = $row['password'];
	   $str = $row['hash_code'];
	   $cart_id = $row['cart_id'];	   
	}
				
					
					/***********  Check Password is Correct ********/
					if($pflag==1){
					
								$from_q = $sh_pass.$str;
								$fk = sha1($from_q);
								/*check password*/
								if($password == $fk)
								{
									
									if(isset($_COOKIE["thehandlestore_AZwUghJKdP"])){
											$cookie_id = $_COOKIE["thehandlestore_AZwUghJKdP"];
									}else{
										setcookie("thehandlestore_AZwUghJKdP", "$cart_id", time()+(86400 * 30), "/","", 0);
									}
										$_SESSION["thehandlestore_AZwUghJKdP"] = $sh_email_id;
										?>
										<script>
										swal({
										  title: "Login Success!",
										  text: "Redirecting You to Main Store!",
										  type: "success",
										  confirmButtonText: "OK"
										},
										function(isConfirm){
										  if (isConfirm) {
											window.location.href = "index.php";
										  }
										});
										</script><?php
									
								}else{
								/****** Password does Not Match *****/	
								?>
										<script>
										swal({
										  title: "Your Password is Incorrect!",
										  text: "Please try agin!",
										  type: "error",
										  confirmButtonText: "OK"
										},
										function(isConfirm){
										  if (isConfirm) {
											window.location.href = "account-login.php";
										  }
										});
										</script><?php		
									
									
								}

						
					}else{
						/****** Email Id does Not Exist *****/
											?>
									<script>
									swal({
									  title: "Sorry we could not found any account with that Email Id!",
									  text: "Please try agin!",
									  type: "error",
									  confirmButtonText: "OK"
									},
									function(isConfirm){
									  if (isConfirm) {
										window.location.href = "account-login.php";
									  }
									});
									</script><?php	
					}
	
}
?>
</body>
</html>