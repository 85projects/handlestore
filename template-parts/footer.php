<?php function site_footer() { ?>
<!---------------------------NEWSLETTER---------------------->
<div id="newsletter">
	<div class="container2">
		

     <div class="row">
     	
     	<div class="col-md-6" id="newsletter_main">
     	
     	<h1>Newsletter</h1>	
     	<div class="row">
     		<div class="col-md-8">
     			<p>Subscribe to the Handle Store list to receive updates 
on new arrivals, special offers and 
other discount information.</p>


<input type="" name="" placeholder="Enter your Email Address" class="form-control commentfix">
 <input type="" name="" placeholder="Your phone number (optional)" class="form-control commentfix" style="margin-top: 14px;">
 
 <a href=""> <button class="newsletter_btn form-control" style="margin-top: 14px;"><i class="fas fa-paper-plane" style="color: #ffffff;"></i> SEND</button></a>



     		</div>
     		<div class="col-md-4"></div>
     	</div>
     	</div>
     	<div class="col-md-6" id="newsletter_image">
     		
     		<img src="background/newsletter_img.png" alt="newsletter">
     	</div>

     </div>

	</div>
</div>
<!---------------------------REASONS TO SHOP--------------------------------->
<div id="reasons_to_shop" style="">
	<div class="container2" >

		<div class="row">
			<div class="col-md-12">	
				<h1>Why Shop With us?</h1>
				<hr/>
			</div>
		</div>
		<div id="three_reasons">
			<div class="row">

				<div class="col-md-6 " id="great_offers">            
					<div class="row">
						<div class="col-md-2 text-center">
						 <img src="icons/reason_1.png" alt="reason1" class="reason_img">             		
						</div>
						<div class="col-md-10">
							<h4>Great Offers</h4>
							<p>Subscribe to the Handle Store
on new arrivals, special offers and 
other discount information.</p>
						</div>
					</div>
				</div>

				<div class="col-md-6" id="free_shipping">
					<div class="row">
						<div class="col-md-2 text-center">  
						<img src="icons/reason_2.png" alt="reason2" class="reason_img">           		
						</div>
						<div class="col-md-10">
							<h4>Trusted by all</h4>
							<p>Subscribe to the Handle Store
on new arrivals, special offers and 
other discount information.</p>
						</div>
					</div>
				</div>

<div class="row" style="margin-top: 40px;">
				<div class="col-md-6" id="safest_online_store">
					<div class="row">
						<div class="col-md-2 text-center"> 
						<img src="icons/reason_3.png" alt="reason3" class="reason_img">             		
						</div>
						<div class="col-md-10">
							<h4>Endless option on the go</h4>
							<p>Subscribe to the Handle Store
on new arrivals, special offers and 
other discount information.</p>
						</div>
					</div>
				</div>

                <div class="col-md-6" id="safest_online_store">
                    <div class="row">
                        <div class="col-md-2 text-center"> 
                        <img src="icons/reason_3.png" alt="reason3" class="reason_img">                    
                        </div>
                        <div class="col-md-10">
                            <h4>Best shopping experience and safe shopping</h4>
                            <p>Subscribe to the Handle Store
on new arrivals, special offers and 
other discount information.</p>
                        </div>
                    </div>
                </div>

</div>

			</div>
		</div>

	</div>	
</div>
<!---------------------------FOOTER AREA--------------------------------->
<footer id="footer_area" style="background-color: #f5f6f8;">

	<div class="container">
		
    
    <div class="row">
    
    <div class="col-md-4" id="left_part_footer" >

    	       <a href="index.php"><img src="logos/handle_logo_final.png" alt="The hangle store" id="store_logo"></a>
    	<p>Aliquam sodales accumsan justo, at fringilla 
elit pulvinar cursus.Aliquam sodales accumsan justo, 
at fringilla elit pulvinar cursus.</p>

    </div>	
    <div class="col-md-2">
    	<ul>
    	<a href="product_list.php"><li><p>MAIN DOOR</p></li></a>
    	<a href="product_list.php"><li><p>KITCHEN</p></li></a>		
    	<a href="product_list.php"><li><p>CABINET</p></li></a>	
    	<a href="product_list.php"><li><p>KNOBS</p></li></a>	
    	<a href="product_list.php"><li><p>PROFILES</p></li></a>	
    	<a href="product_list.php"><li><p>MORTICE</p></li></a>	

    	</ul>
    </div>
    <div class="col-md-2">
    	<ul>
    	<a href="about_us.php"><li>ABOUT US</li></a>
    	<a href="contact_us.php"><li>CONTACT US</li></a>		
    	<a href=""><li>PRIVACY POLICY</li></a>	
    	<a href=""><li>FAQ'S</li></a>	
    	<a href=""><li>WARRANTY</li></a>	
    	<a href=""><li>T&C</li></a>	
    	<a href=""><li>RETURN POLICY</li></a>	

    	</ul>
    </div>
    <div class="col-md-4"  id="social_media">
    	
    	<h4>Social Media</h4>

    	<div class="btn-group" id="button_group_social">
    		<button><i class="fab fa-facebook-f"></i></button>
    		<button><i class="fab fa-twitter"></i></button>
    		<button><i class="fab fa-instagram"></i></button>
    		<button><i class="fab fa-youtube"></i></button>
    	</div>
    </div>
    </div>
	</div>
</footer>

<link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/assets/owl.carousel.min.css" rel="stylesheet" type="text/css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/glightbox.min.js"></script>
<script src="js/index.js"></script>
<?php } ?>