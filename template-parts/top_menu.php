<?php function site_top_menu() { ?>
      <!-- quickview-modal -->
              <nav class="navbar navbar-expand-lg navbar-light header-navigation stricky-menu stricky header-style-four">
            <div class="container clearfix">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="logo-box clearfix">
                    <a class="side-nav-toggler nav-toggler hidden-bar-opener" href="#">
                        <i class="energy-icon-menu"></i>
                    </a>
                    <button class="menu-toggler" data-target="#main-nav-bar-stricky">
                        <span class="fa fa-bars"></span>
                    </button>
                </div><!-- /.logo-box -->
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="main-navigation" id="main-nav-bar-stricky">
                           <ul class="navigation-box">
                                <li class="current"><a href="index.html">Home</a>
                                </li>
                                <li><a href="#">Online Ups</a>
                                    <ul class="sub-menu">
                                        <li><a href="index.html">Product 1</a></li>
										<li><a href="index.html">Product 2</a></li>
										<li><a href="index.html">Product 3</a></li>
										<li><a href="index.html">Product 4</a></li>
										<li><a href="index.html">Product 5</a></li>
                                    </ul>
                                </li>
                                 <li><a href="#">Luminuous Solar Division</a>
                                    <ul class="sub-menu">
                                        <li><a href="index.html">Product 1</a></li>
										<li><a href="index.html">Product 2</a></li>
										<li><a href="index.html">Product 3</a></li>
										<li><a href="index.html">Product 4</a></li>
										<li><a href="index.html">Product 5</a></li>
                                    </ul>
                                </li>
								 <li><a href="#">Stabilizers</a>
                                    <ul class="sub-menu">
                                        <li><a href="index.html">Product 1</a></li>
										<li><a href="index.html">Product 2</a></li>
										<li><a href="index.html">Product 3</a></li>
										<li><a href="index.html">Product 4</a></li>
										<li><a href="index.html">Product 5</a></li>
                                    </ul>
                                </li>
								 <li><a href="#">Inverters & Ups</a>
                                    <ul class="sub-menu">
                                        <li><a href="index.html">Product 1</a></li>
										<li><a href="index.html">Product 2</a></li>
										<li><a href="index.html">Product 3</a></li>
										<li><a href="index.html">Product 4</a></li>
										<li><a href="index.html">Product 5</a></li>
                                    </ul>
                                </li>
								 <li><a href="#">Genset Division</a>
                                    <ul class="sub-menu">
                                        <li><a href="index.html">Product 1</a></li>
										<li><a href="index.html">Product 2</a></li>
										<li><a href="index.html">Product 3</a></li>
										<li><a href="index.html">Product 4</a></li>
										<li><a href="index.html">Product 5</a></li>
                                    </ul>
                                </li>
								 <li><a href="#">More</a>
                                    <ul class="sub-menu">
                                        <li><a href="index.html">Batteries</a></li>
										<li><a href="index.html">Power Transformers</a></li>
                                    </ul>
                                </li>
								 <li class="current"><a href="index.html">Blogs</a>
                            </ul>
                </div><!-- /.navbar-collapse -->
                <div class="right-side-box">
                   <!--
                    --><a href="#test-search" class="search-btn popup-with-zoom-anim"><i class="energy-icon-musica-searcher"></i></a>
					  <a href="index.html" class="cart-btn"><i class="icon-heart"></i><span class="number">0</span></a>&nbsp;<!--
                    --><a href="index.html" class="cart-btn"><i class="icon-cart"></i><span class="number">0</span></a>
                </div>
                <!-- /.right-side-box -->
            </div>
            <!-- /.container -->
        </nav>



<?php } ?>