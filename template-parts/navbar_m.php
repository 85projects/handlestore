<?php function bottom_menu() { 
include_once("main_include.php");
?>

<header>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<nav class="navbar navbar-expand-lg navbar-light">
  <a class="navbar-brand" href="index.php"><img src="logos/handle_logo_final.png" id="store_logo"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
  <!-- 	    <form class="form-inline my-2 my-lg-0 ml-auto">
      <input class="form-control mr-sm-2" type="search" placeholder="Search for Handles,Knobs and More.." aria-label="Search">
    </form> -->
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="index.php">HOME<span class="sr-only">(current)</span></a>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          HANDLES
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Mortice Plates</a>
          <a class="dropdown-item" href="#">Mortice Rose</a>
          <a class="dropdown-item" href="#">Main Door Pull</a>
          <a class="dropdown-item" href="#">Cabinets</a>
          <a class="dropdown-item" href="#">Knobs</a>
          <a class="dropdown-item" href="#">Concealed</a>
          <a class="dropdown-item" href="#">Profile</a>
          <a class="dropdown-item" href="#">Glass door</a>
        </div>
      </li>

            <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          DOOR ACCESSORIES
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
         <a class="dropdown-item" href="#">Towerbolts</a>
          <a class="dropdown-item" href="#">Hinges</a>
          <a class="dropdown-item" href="#">Door Closers</a>
          <a class="dropdown-item" href="#">Aldrops & Latches</a>
          <a class="dropdown-item" href="#">Robe hooks and Keyholders</a>
          <a class="dropdown-item" href="#">Locks</a>
          <a class="dropdown-item" href="#">Magnets and stoppers</a>
          <a class="dropdown-item" href="#">Nails and Knockers</a>
          <a class="dropdown-item" href="#">Nails and Knockers</a>
        </div>
      </li>

            <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          BATH ACCESSORIES
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
           <a class="dropdown-item" href="#">5 piece Sets</a>
          <a class="dropdown-item" href="#">Glass Shelves</a>
          <a class="dropdown-item" href="#">SS Mirror cabinets</a>
          <a class="dropdown-item" href="#">Shower seats and foot rests</a>
          <a class="dropdown-item" href="#">Individual products</a>
        </div>
      </li>
    </ul>

      <form class="form-inline my-2 my-lg-0 ml-auto" id="my_header_form">
      <input class="form-control mr-sm-2" type="search" placeholder="Search for Handles,Knobs and More.." aria-label="Search">
      <button class="cart_btn"><img src="icons/cart_icon.png" id="cart_icon"> cart</button>
      <div class="mydropdown">
      <button class="login_btn"><img src="icons/login_icon.png"> LOGIN / SIGNUP</button>
      <div class="dropdown-content">
      <a href="#">Orders</a>
      <a href="#">Whishlist</a>
      <a href="#">Contact Us</a> 
      <hr style="padding: 0;margin: 0;" />   
      <a href="#" style="color: red;">Logout</a>
  </div>
  </div>
    </form>
  </div>
</nav>
		</div>
	</div>
</div>

</header>

<?php } ?>