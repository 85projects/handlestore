<?php
ob_start();
session_start();
include_once("template-parts/header_links.php");
include_once("template-parts/navbar_m.php");
include_once("template-parts/footer.php");
include_once("includes/main_include.php");

if(isset($_GET["cat"])){
$id = $_GET["cat"];
$salt6="FgHeT789ios245getHJn";
$decrypted_id_raw6 = base64_decode($id);
$decrypted_id6 = preg_replace(sprintf('/%s/', $salt6), '', $decrypted_id_raw6);
$query = "SELECT * FROM `pro_sub_category` WHERE `sub_id` = '$decrypted_id6'";
$c2 = mysqli_query($conn,$query);
while($row = mysqli_fetch_array($c2))
{
	$category = $row["cat_name"];
	$sub_id = $row["sub_id"];
	$main_cat_id = $row["main_cat_id"];
}
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">

  <title>The Handle Store | <?php echo $category ?></title>

  <meta name="description" content="Best handle selling website">
  <meta name="author" content="Handle Store">
  <?php header_links(); ?>
 <link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
 <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
 
</head>
<body>
<!---------------------------HEADER AREA--------------------------------->
<?php bottom_menu(); ?>
<!---------------------------MAIN DOOR HANDLES--------------------------------->
<div id="main_door">
 <div class="container2">
   
   <div id="filters">
  <p> <a href="index.php">Home</a> / <a href=""><?php echo $category ?></a></p>
   <div class="row">
     <div class="col-md-5">
       <h1><?php echo $category ?></h1>
     </div>
     <div class="col-md-7">
       <hr/>
     </div>
   </div>


    <div id="filters_main">
      <div class="row">


        <div class="col-md-3">
            <div class="row">
              <div class="col-md-6">
                <h3>Filters</h3>
              </div>
            </div>
            <div id="view_the_product">
 <div id="select_material">
        <p><strong>SELECT MATERIAL</strong></p>
        <hr/>
				<?php 	
				        $query = "SELECT `mat_id`, `mat_name` FROM `pro_material` WHERE 1";
						$c22 = mysqli_query($conn,$query);
						while($r = mysqli_fetch_array($c22))
						{
                   		  ?>
						<p><label><input type="checkbox" class="common_selector material" value="<?php echo $r['mat_id']; ?>" > <?php echo $r['mat_name']; ?></label></p>
						
				<?php } ?>
      
        </div> 

        <div id="select_finish">
        <p><strong>SELECT FINISH</strong></p>
        <hr/>
				<?php 	
				        $b1 ="SELECT distinct `handle_finishes` FROM `products_db` WHERE `main_category` = '$main_cat_id' AND `status` = '1'";
						$c22 = mysqli_query($conn,$b1);
						while($r = mysqli_fetch_array($c22))
						{
							 $pro_sub = $r['handle_finishes'];
							 $query_00 = "SELECT * FROM `handle_finishes` WHERE `fin_id` = '$pro_sub'";
							 $c22_oo = mysqli_query($conn,$query_00);
							 while($row = mysqli_fetch_array($c22_oo))
							 {
                   		  ?>
						<p><label><input type="checkbox" class="common_selector finish" value="<?php echo $row['fin_id']; ?>" > <?php echo $row['finish_name']; ?></label></p>
						
						<?php } }?>      
        </div> 

         <div id="select_size">
        <p><strong>SELECT SIZE</strong></p>
        <hr/>
       <?php 	
				        $b1_w ="SELECT distinct `handle_sizes` FROM `products_db` WHERE `main_category` = '$main_cat_id' AND `status` = '1'";
						$c22_w = mysqli_query($conn,$b1_w);
						while($r = mysqli_fetch_array($c22_w))
						{
							 $pro_sub_w = $r['handle_sizes'];
							 $query_001 = "SELECT * FROM `handle_sizes` WHERE size_id = '$pro_sub_w'";
							 $c22_oo1 = mysqli_query($conn,$query_001);
							 while($row = mysqli_fetch_array($c22_oo1))
							 {
                   		  ?>
						<p><label><input type="checkbox" class="common_selector size" value="<?php echo $row['size_id']; ?>" > <?php echo $row['size_name']; ?></label></p>
						
						<?php } }?>
        </div>

        <div id="select_finish">
        <p><strong>PRICE RANGE</strong></p>
        <hr/>
							  <input type="hidden" id="hidden_minimum_price" value="25" />
							  <input type="hidden" id="hidden_maximum_price" value="3000" />
							  <p id="price_show">200 - 5000</p>
							  <div id="price_range"></div>
        </div> 
        <div id="offer_div">
          <img src="background/offer_img.png" id="offer_img" class="img-fluid">
          <h1>Buy 1 get 1</h1>
          <h2>Free</h2>
          <p>Aliquam sodales
elit pulvinar cursus.</p>
        </div>

        </div>
      </div>



        <div class="col-md-9">
         <div class="row">
           <div class="col-md-8">
             
           </div>
           <div class="col-md-4">
            <!-- <select name="sort" id="sort">
			 <option value="relevance">Latest</option>
             <option value="popularity">Price Low to High</option>
             <option value="relevance">Price High to Low</option>
             </select>-->
           </div>
         </div>

         <div class="product_display">
           <div class="row filter_data contents">
             
           </div>
          
         </div>

         
        


        </div>
      </div>
    </div>   
 </div>
 
         <div class="pagination_area">
           <div class="row">
             <div class="col-md-5">
               <hr/>
             </div>
             <div class="col-md-7">
               <div class="btn-group pagination_btn_group" id="tab">
                 
               </div>
             </div>
           </div>
         </div>
 
 </div> 
</div>
<script>
$(document).ready(function(){

    filter_data();

    function filter_data()
    {
        $('.filter_data').html('<div id="loading" style="" ></div>');
        var action = <?php echo $sub_id ?>;
        var minimum_price = $('#hidden_minimum_price').val();
        var maximum_price = $('#hidden_maximum_price').val();
        var material = get_filter('material');
        var size = get_filter('size');
		var finish = get_filter('finish');
        $.ajax({
            url:"fetch_h.php",
            method:"POST",
            data:{action:action, minimum_price:minimum_price, maximum_price:maximum_price, material:material, size:size, finish:finish},
            success:function(data){
                $('.filter_data').html(data);
				
            }
        });
    }

    function get_filter(class_name)
    {
        var filter = [];
        $('.'+class_name+':checked').each(function(){
            filter.push($(this).val());
        });
        return filter;
    }

    $('.common_selector').click(function(){
        filter_data();
    });

    $('#price_range').slider({
        range:true,
        min:200,
        max:5000,
        values:[200, 5000],
        step:50,
        stop:function(event, ui)
        {
            $('#price_show').html(ui.values[0] + ' - ' + ui.values[1]);
            $('#hidden_minimum_price').val(ui.values[0]);
            $('#hidden_maximum_price').val(ui.values[1]);
            filter_data();
        }
    });

});
</script>
<?php site_footer(); ?>
</body>
</html>