<?php
session_start(); 
error_reporting(0);
include_once("template-parts/header_links.php");
include_once("template-parts/navbar_m.php");
include_once("template-parts/footer.php");
include_once("includes/main_include.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
  <title>The Handle Store | Order Confirmation</title>
  <meta name="description" content="Best handle selling website">
  <meta name="author" content="Handle Store">
  <link href="https://fonts.googleapis.com/css2?family=Ramaraja&display=swap" rel="stylesheet">
  <?php header_links(); ?>
  <script>
    $(document).ready(function(){
        $("#myModal").modal('show');
    });
</script>
</head>
<body>
<style>
button.filter-button.active:focus {
    background: white;
  color:black;
    border: 1px solid yellow;
}
</style>



<section> 
<header>
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <nav class="navbar navbar-expand-lg navbar-light">
  <a class="navbar-brand" href="index.php"><img src="logos/handle_logo_final.png" id="store_logo"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
  <!--      <form class="form-inline my-2 my-lg-0 ml-auto">
      <input class="form-control mr-sm-2" type="search" placeholder="Search for Handles,Knobs and More.." aria-label="Search">
    </form> -->
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="index.php">HOME<span class="sr-only">(current)</span></a>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          HANDLES
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Mortice Plates</a>
          <a class="dropdown-item" href="#">Mortice Rose</a>
          <a class="dropdown-item" href="#">Main Door Pull</a>
          <a class="dropdown-item" href="#">Cabinets</a>
          <a class="dropdown-item" href="#">Knobs</a>
          <a class="dropdown-item" href="#">Concealed</a>
          <a class="dropdown-item" href="#">Profile</a>
          <a class="dropdown-item" href="#">Glass door</a>
        </div>
      </li>

            <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          DOOR ACCESSORIES
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>

            <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          BATH ACCESSORIES
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
    </ul>

      <form class="form-inline my-2 my-lg-0 ml-auto" id="my_header_form">
      <input class="form-control mr-sm-2" type="search" placeholder="Search for Handles,Knobs and More.." aria-label="Search">
      <button class="cart_btn"><img src="icons/cart_icon.png" id="cart_icon"> cart</button>
      <div class="mydropdown">
      <button class="login_btn"><img src="icons/login_icon.png"> LOGIN / SIGNUP</button>
      <div class="dropdown-content">
      <a href="#">Orders</a>
      <a href="#">Whishlist</a>
      <a href="#">Contact Us</a> 
      <hr style="padding: 0;margin: 0;" />   
      <a href="#" style="color: red;">Logout</a>
  </div>
  </div>
    </form>
  </div>
</nav>
    </div>
  </div>
</div>

</header>





<section>
  <div class="container2" style="padding: 80px 60px 80px 60px;">

    <div class="row">
      <div class="col-md-12">
       


        <div id="myModal" class="modal fade">
    <div class="modal-dialog" style="margin-top: 100px;" >
        <div class="modal-content" style="background-color: #fbd100;">
            <div class="modal-header">
                <h5 class="modal-title">Your Order Has Been Placed Successfully!</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                 <img src="images/order_placed.jpg">
                <p>Subscribe to our mailing list to get the latest updates straight in your inbox.</p>
                <form>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Name">
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" placeholder="Email Address">
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                          <button type="submit" class="btn btn-primary">Subscribe</button>
                      </div>
                      <div class="col-md-6 text-right">
                          <button type="submit" class="btn btn-primary">Back to Home</button>
                      </div>
                    </div>
                  
                </form>
            </div>
        </div>
    </div>
</div>
        


      </div>
    </div>    
    
  </div>
</section>







<!-- Footer Starts Here -->
<?php site_footer(); ?>

</body>
</html>