<?php
session_start(); 
error_reporting(0);
include_once("template-parts/header_links.php");
include_once("template-parts/navbar_m.php");
include_once("template-parts/footer.php");
include_once("includes/main_include.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
  <title>The Handle Store | Order Confirmation</title>
  <meta name="description" content="Best handle selling website">
  <meta name="author" content="Handle Store">
  <link href="https://fonts.googleapis.com/css2?family=Ramaraja&display=swap" rel="stylesheet">
  <?php header_links(); ?>
</head>
<body>
  <style>
    button.filter-button.active:focus {
      background: white;
      color:black;
      border: 1px solid yellow;
    }
  </style>
  <section> 
    <header>
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <nav class="navbar navbar-expand-lg navbar-light">
              <a class="navbar-brand" href="index.php"><img src="logos/handle_logo_final.png" id="store_logo"></a>
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>

              <div class="collapse navbar-collapse" id="navbarSupportedContent">
  <!--      <form class="form-inline my-2 my-lg-0 ml-auto">
      <input class="form-control mr-sm-2" type="search" placeholder="Search for Handles,Knobs and More.." aria-label="Search">
    </form> -->
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="index.php">HOME<span class="sr-only">(current)</span></a>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          HANDLES
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Mortice Plates</a>
          <a class="dropdown-item" href="#">Mortice Rose</a>
          <a class="dropdown-item" href="#">Main Door Pull</a>
          <a class="dropdown-item" href="#">Cabinets</a>
          <a class="dropdown-item" href="#">Knobs</a>
          <a class="dropdown-item" href="#">Concealed</a>
          <a class="dropdown-item" href="#">Profile</a>
          <a class="dropdown-item" href="#">Glass door</a>
        </div>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          DOOR ACCESSORIES
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          BATH ACCESSORIES
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
    </ul>

    <form class="form-inline my-2 my-lg-0 ml-auto" id="my_header_form">
      <input class="form-control mr-sm-2" type="search" placeholder="Search for Handles,Knobs and More.." aria-label="Search">
      <button class="cart_btn"><img src="icons/cart_icon.png" id="cart_icon"> cart</button>
      <div class="mydropdown">
        <button class="login_btn"><img src="icons/login_icon.png"> LOGIN / SIGNUP</button>
        <div class="dropdown-content">
          <a href="#">Orders</a>
          <a href="#">Whishlist</a>
          <a href="#">Contact Us</a> 
          <hr style="padding: 0;margin: 0;" />   
          <a href="#" style="color: red;">Logout</a>
        </div>
      </div>
    </form>
  </div>
</nav>
</div>
</div>
</div>

</header>


<script>
 $(document).ready(function() {
   
  $('.faq_question').click(function() {
   
    if ($(this).parent().is('.open')){
      $(this).closest('.faq').find('.faq_answer_container').animate({'height':'0'},200);
      $(this).closest('.faq').removeClass('open');
      
    }else{
      var newHeight =$(this).closest('.faq').find('.faq_answer').height() +'px';
      $(this).closest('.faq').find('.faq_answer_container').animate({'height':newHeight},200);
      $(this).closest('.faq').addClass('open');
    }
    
  });
  
});
</script>

<style>
  /*FAQS*/
  .faq_question {
    margin: 0px;
    padding: 0px 0px 5px 0px;
    display: inline-block;
    cursor: pointer;
    font-weight: bold;
  }
  
  .faq_answer_container {
    height: 0px;
    overflow: hidden;
    padding: 0px;
  }

  .faq_container1{


    max-width: 640px;
    margin:0 auto;
    padding: 20px 40px 20px 40px;
    margin-top: 10px;
    box-shadow: 0px 0px 5px 0px rgba(195,207,226,1);
  }

  .faq_ques_p{

    font-size: 20px;
    color: #8e9eab !important;
    line-height: 18px;
  }

  .faq_ans_p{
    color: #8e9eab !important;
  }


</style>





<section>
  <div class="container-fluid" style="background-color: #eff7fa;">
    
    
    <div class="container2" style="padding: 80px 10px 80px 10px;">

     
     <div class="row">
       <div class="col-md-6 faq_container1" style="background-color: #ffffff;">
         
        
        <div class="row">
          <div class="col-md-12 faq" style="padding: 20px 20px 20px 20px;border:1px solid #cfd9df;">
            <div class="faq_question">
              <p class="faq_ques_p">Are there any charges for registration?</p>
            </div>
            <div class="faq_answer_container">
              <div class="faq_answer">
                <p class="faq_ans_p">No. Registration on shubhstores.com is absolutely free And there are no charges for it</p>
              </div>
            </div>        

          </div>
        </div>    

        <div class="row">
          <div class="col-md-12 faq" style="padding: 20px 20px 20px 20px;border:1px solid #cfd9df;">
            <div class="faq_question">
              <p class="faq_ques_p">Do I have to necessarily register to shop on shubhstores.com?</p>
            </div>
            <div class="faq_answer_container">
              <div class="faq_answer">
               <p class="faq_ans_p">You can surf and add products to the cart without registration but only registered shoppers will be able to checkout and place orders. Registered members have to be logged in at the time of checking out the cart, they will be prompted to do so if they are not logged in.</p>
             </div>
           </div>        

         </div>
       </div>    
       <div class="row">
        <div class="col-md-12 faq" style="padding: 20px 20px 20px 20px;border:1px solid #cfd9df;">
          <div class="faq_question">
            <p class="faq_ques_p">What are the modes of payment?</p>
          </div>
          <div class="faq_answer_container">
            <div class="faq_answer">
             <p class="faq_ans_p">You can pay for your order on shubhstores.com using the following modes of payment:
              <br>a. Cash on delivery.
              <br>b. Online Payment.
              <br>c. Card On Delivery.</p>
            </div>
          </div>        

        </div>
      </div>    

      <div class="row">
        <div class="col-md-12 faq" style="padding: 20px 20px 20px 20px;border:1px solid #cfd9df;">
          <div class="faq_question">
            <p class="faq_ques_p">What is the meaning of card on delivery?</p>
          </div>
          <div class="faq_answer_container">
            <div class="faq_answer">
             <p class="faq_ans_p">Card on delivery means you can pay for your order at the time of order delivery at your doorstep Using Your Debit/Creadit Card.</p>
           </div>
         </div>        

       </div>
     </div>    

     <div class="row">
      <div class="col-md-12 faq" style="padding: 20px 20px 20px 20px;border:1px solid #cfd9df;">
        <div class="faq_question">
          <p class="faq_ques_p">What Are Our Shipping Cost?</p>
        </div>
        <div class="faq_answer_container">
          <div class="faq_answer">
           <p class="faq_ans_p">We Charge 25 Rupees Below Order of 2000 rupees.</p>
         </div>
       </div>        

     </div>
   </div>        
 </div>


 <div class="col-md-6 faq_container1" style="background-color: #ffffff;">
   
  
  <div class="row">
    <div class="col-md-12 faq" style="padding: 20px 20px 20px 20px;border:1px solid #cfd9df;">
      <div class="faq_question">
        <p class="faq_ques_p">When will I receive my order?</p>
      </div>
      <div class="faq_answer_container">
        <div class="faq_answer">
          <p class="faq_ans_p">Once you are done selecting your products and click on checkout you will be prompted to select delivery slot. Your order will be delivered to you next day As Per slot selected by you. If we are unable to deliver the order during the specified time duration (this sometimes happens due to unforeseen situations) we will Try To Delivery It Next Day(Unless You Cancel Order).</p>
        </div>
      </div>        

    </div>
  </div>    

  <div class="row">
    <div class="col-md-12 faq" style="padding: 20px 20px 20px 20px;border:1px solid #cfd9df;">
      <div class="faq_question">
        <p class="faq_ques_p">How will the delivery be done?</p>
      </div>
      <div class="faq_answer_container">
        <div class="faq_answer">
         <p class="faq_ans_p">We have a dedicated team of delivery personnel and a fleet of vehicles operating across the city which ensures timely and accurate delivery to our customers.</p>
       </div>
     </div>        

   </div>
 </div>    
 <div class="row">
  <div class="col-md-12 faq" style="padding: 20px 20px 20px 20px;border:1px solid #cfd9df;">
    <div class="faq_question">
      <p class="faq_ques_p">Will someone inform me if my order delivery gets delayed?</p>
    </div>
    <div class="faq_answer_container">
      <div class="faq_answer">
       <p class="faq_ans_p">In case of a delay, our customer support team will keep you updated about your delivery.</p>
     </div>
   </div>        

 </div>
</div>    

<div class="row">
  <div class="col-md-12 faq" style="padding: 20px 20px 20px 20px;border:1px solid #cfd9df;">
    <div class="faq_question">
      <p class="faq_ques_p">When and how can I cancel an order?</p>
    </div>
    <div class="faq_answer_container">
      <div class="faq_answer">
       <p class="faq_ans_p">You can cancel an order before Shipping Or Slot time of your slot by Visiting Here Cancel Order</p>
     </div>
   </div>        

 </div>
</div>    

<div class="row">
  <div class="col-md-12 faq" style="padding: 20px 20px 20px 20px;border:1px solid #cfd9df;">
    <div class="faq_question">
      <p class="faq_ques_p">Which Products Can I return When Delivered To My House?</p>
    </div>
    <div class="faq_answer_container">
      <div class="faq_answer">
       <p class="faq_ans_p">You can cancel All Products Which fall in Grocery Category. But You can not return Products Which fall in Vegetables And Sweets Categories.</p>
     </div>
   </div>        

 </div>
</div>    


</div>




</div>



</div>

</div>
</section>







<!-- Footer Starts Here -->
<?php site_footer(); ?>

</body>
</html>