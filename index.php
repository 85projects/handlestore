<?php
session_start(); 
error_reporting(0);
include_once("template-parts/header_links.php");
include_once("template-parts/navbar_m.php");
include_once("template-parts/footer.php");
include_once("includes/main_include.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
  <title>The Handle Store | Main</title>
  <meta name="description" content="Best handle selling website">
  <meta name="author" content="Handle Store">
  <link href="https://fonts.googleapis.com/css2?family=Ramaraja&display=swap" rel="stylesheet">
  <?php header_links(); ?>
</head>
<body>
<style>
button.filter-button.active:focus {
    background: white;
	color:black;
    border: 1px solid yellow;
}
</style>
<section> 
<header>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<nav class="navbar navbar-expand-lg navbar-light">
  <a class="navbar-brand" href="index.php"><img src="logos/handle_logo_final.png" id="store_logo"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
  <!-- 	    <form class="form-inline my-2 my-lg-0 ml-auto">
      <input class="form-control mr-sm-2" type="search" placeholder="Search for Handles,Knobs and More.." aria-label="Search">
    </form> -->
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="index.php">HOME<span class="sr-only">(current)</span></a>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          HANDLES
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Mortice Plates</a>
          <a class="dropdown-item" href="#">Mortice Rose</a>
          <a class="dropdown-item" href="#">Main Door Pull</a>
          <a class="dropdown-item" href="#">Cabinets</a>
          <a class="dropdown-item" href="#">Knobs</a>
          <a class="dropdown-item" href="#">Concealed</a>
          <a class="dropdown-item" href="#">Profile</a>
          <a class="dropdown-item" href="#">Glass door</a>
        </div>
      </li>

            <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          DOOR ACCESSORIES
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>

            <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          BATH ACCESSORIES
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
    </ul>

      <form class="form-inline my-2 my-lg-0 ml-auto" id="my_header_form">
      <input class="form-control mr-sm-2" type="search" placeholder="Search for Handles,Knobs and More.." aria-label="Search">
      <button class="cart_btn"><img src="icons/cart_icon.png" id="cart_icon"> cart</button>
      <div class="mydropdown">
      <button class="login_btn"><img src="icons/login_icon.png"> LOGIN / SIGNUP</button>
      <div class="dropdown-content">
      <a href="#">Orders</a>
      <a href="#">Whishlist</a>
      <a href="#">Contact Us</a> 
      <hr style="padding: 0;margin: 0;" />   
      <a href="#" style="color: red;">Logout</a>
  </div>
  </div>
    </form>
  </div>
</nav>
		</div>
	</div>
</div>

</header>
<section> 
	
<div class="main_banner">
 <div class="container">
  <div id="carousel" class="carousel slide hero-slides" data-ride="carousel">
  <ol class="carousel-indicators">
    <li class="active" data-target="#carousel" data-slide-to="0"></li>
    <li data-target="#carousel" data-slide-to="1"></li>
    <li data-target="#carousel" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner" role="listbox">
    <div class="carousel-item active boat">
      <div class="container h-100 d-none d-md-block">
        <div class="row align-items-center h-100">
          <div class="col-12 col-md-9 col-lg-7 col-xl-6">
            <div class="caption animated fadeIn">
              <h2 class="animated fadeInLeft">Handle Store</h2>
              <h2 class="animated fadeInLeft">Door Handles</h2>
              <p class="animated fadeInRight">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tristique nisl vitae luctus sollicitudin. Fusce consectetur sem eget dui tristique, ac posuere arcu varius.</p>
            
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="carousel-item sea">
      <div class="container h-100 d-none d-md-block">
        <div class="row align-items-center h-100">
          <div class="col-12 col-md-9 col-lg-7 col-xl-6">
            <div class="caption animated fadeIn">
              <h2 class="animated fadeInLeft">Discover the canyon by the sea</h2>
              <p class="animated fadeInRight">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tristique nisl vitae luctus sollicitudin. Fusce consectetur sem eget dui tristique, ac posuere arcu varius.</p>
           
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="carousel-item river">
      <div class="container h-100 d-none d-md-block">
        <div class="row align-items-center h-100">
          <div class="col-12 col-md-9 col-lg-7 col-xl-6">
            <div class="caption animated fadeIn">
              <h2 class="animated fadeInLeft">Explore the river valley</h2>
              <p class="animated fadeInRight">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tristique nisl vitae luctus sollicitudin. Fusce consectetur sem eget dui tristique, ac posuere arcu varius.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
   
 </div> 
</div>


<div class="main_div">
  
<div class="sliding_gallery">
 <div class="container2">
   
 <div class="framebox">
       <div class="owl-carousel">
      <a class="item item1" id="owl_item">
      	<div class="popular_container">
       <img src="background/image1.png" alt="">
       <h3 class="main_lables">Main Door <br>Handles</h3>
       <p class="explore_p">EXPLORE</p>
       <img src="icons/explore_icon.png" class="explore_icon">
       </div>
      </a>
      <a class="item item2" id="owl_item">
         <div class="popular_container">
       <img src="background/image2.png" alt="">
       <h3 class="main_lables">Kitchen <br>Handles</h3>
       <p class="explore_p">EXPLORE</p>
       <img src="icons/explore_icon.png" class="explore_icon">
       </div>
      </a>
      <a class="item item3" id="owl_item">
         <div class="popular_container">
       <img src="background/image4.png" alt="">
       <h3 class="main_lables">Cabinet <br>Handles</h3>
       <p class="explore_p">EXPLORE</p>
       <img src="icons/explore_icon.png" class="explore_icon">
       </div>
      </a>
      <a class="item item4" id="owl_item">
         <div class="popular_container">
       <img src="background/image3.png" alt="">
       <h3 class="main_lables">Knobs</h3>
       <p class="explore_p">EXPLORE</p>
       <img src="icons/explore_icon.png" class="explore_icon">
       </div>
      </a>
      <a class="item item5" id="owl_item">
         <div class="popular_container">
       <img src="background/image1.png" alt="">
       <h3 class="main_lables">Main Door <br>Handles</h3>
       <p class="explore_p">EXPLORE</p>
       <img src="icons/explore_icon.png" class="explore_icon">
       </div>
      </a>
      <a class="item item6" id="owl_item">
         <div class="popular_container">
       <img src="background/image3.png" alt="">
       <h3 class="main_lables">Kitchen <br>Handles</h3>
       <p class="explore_p">EXPLORE</p>
       <img src="icons/explore_icon.png" class="explore_icon">
       </div>
      </a>
  </div>
  </div>

 </div> 
</div>
<div class="offer_main">
 <div class="container2"> 
  <div class="row">
      <div class="col-md-4">
                   <div class="popular_container">
                   <img src="background/banner_product1.png" alt="product_img" class="popular_img"> 
                   <h3 class="popular_offer_name1"><strong>Upto 25% off <br>on orders above</strong></h3>
                  <!--  <h3 class="popular_offer_name2"><strong></strong></h3> -->
                   <h3 class="popular_offer_price">2499/-</h3>
                   </div>                  
               </div>

               <div class="col-md-4">
                   <div class="popular_container">
                    <img src="background/banner_product2.png" alt="product_img" class="popular_img">
                    <h3 class="popular_offer_name3"><strong>Buy 1 get 1<br> <span style="color: #419ac4;">Free</span></strong></h3>
                    <!-- <h3 class="popular_offer_name4"><strong>Free</strong></h3> -->
                    <p class="popular_offer_p">For More Exciting<br> Offers Call Us Now.</p>                  
                   </div>
               </div>

               <div class="col-md-4">
                   <div class="popular_container2">
                    <img src="background/banner_product3.png" alt="product_img" class="popular_img">
                    <h3 class="popular_product_name5"><strong>Free Standard Delivery <br>on orders over</strong></h3>
         <!--            <h3 class="popular_product_name6"><strong>on orders over</strong></h3> -->
                   <h3 class="popular_offer_price2">5000/-</h3>    
                   <img src="icons/square_icon.png" class="square_icon">           
                   </div>
               </div>

  </div>
</div>
</div>
</div>
</section>
<!--------------------------------OUR CLIENTS--------------------------------------------->
  <div class="container2">
<div class="our_clients">
    <div class="row">
      <div class="col-md-4" id="our_clients_div1">
        <h3>Brands available on site</h3>
      </div>
      <div class="col-md-8" id="our_clients_div2">
        <div class="row">
    <div class="container">
      <section class="customer-logos slider">
	  <?php $b1 ="SELECT `client_id`, `client_images` FROM `our_clients` WHERE 1";
			$c22 = mysqli_query($conn,$b1);
			while($r = mysqli_fetch_array($c22))
			{
							 $client_images = $r['client_images'];
						?>
        <div class="slide"><img src="images/<?php echo $client_images ?>"></div>
		<?php } ?>
      </section>
    </div>
  </div>
    </div>
    </div>
  </div>
</div>
<style>

.nav-pills .nav-link{
	padding-bottom: 30px;
	border-bottom: 3px solid #ffffff;
}	

.nav-pills .nav-link.active, .nav-pills .show>.nav-link {
    background-color: #ffffff!important;
    color: black;
    margin-bottom: 0;
    padding-bottom: 30px;
    border-bottom: 3px solid #fbd100;
    transition: 0.5s;
    border-radius: 0px;
}
.nav-pills li a{
    color: #000;
    font-weight: 500;
}
</style>



<!------------------------------FEATURED PRODUCTS------------------------------------>
<div id="new_arrivals" style="">
    <div class="container2">
        <div class="row">
            <div class="col-md-5">
             
               <div class="row">
               	<div class="col-md-1" style="max-width: 45px;margin: 0 auto;padding: 0;">
               		<img src="icons/featured.png" alt="featured_logo" style="margin-top: 10px;">
               	</div>
               	<div class="col-md-11" style="padding: 0;">
               		<h1>Featured Products</h1>
               	</div>
               </div>

           </div>
           <div class="col-md-7"></div>
       </div>
        <ul class="nav nav-pills">
          <li class="nav-item">
            <a class="nav-link active" data-toggle="pill" href="#all" role="tab" aria-controls="pills-flamingo" aria-selected="true">ALL</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="pill" href="#maindoor" role="tab" aria-controls="pills-cuckoo" aria-selected="false">MAIN DOOR</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="pill" href="#knobs" role="tab" aria-controls="pills-ostrich" aria-selected="false">KNOBS</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="pill" href="#cabinate" role="tab" aria-controls="pills-tropicbird" aria-selected="false">CABINET</a>
          </li>
		  <li class="nav-item">
            <a class="nav-link" data-toggle="pill" href="#profile" role="tab" aria-controls="pills-tropicbird" aria-selected="false">PROFILES</a>
          </li>
		  <li class="nav-item">
            <a class="nav-link" data-toggle="pill" href="#mortice" role="tab" aria-controls="pills-tropicbird" aria-selected="false">MORTICE ROSE</a>
          </li>
        </ul>
        <hr style="  padding: 0;
  margin: 0;"/>


        <div class="tab-content mt-3">
		     <div class="tab-pane fade show active" id="all" role="tabpanel" aria-labelledby="flamingo-tab">
          <div class="container2">
			<div class="row align-items-center">
				<div class="col-12 col-carousel" >
					<div class="owl-carousel carousel-main" style="padding-top: 30px;">
					 <?php	
				        $query = "SELECT * FROM `products_db` WHERE `pro_parent_id` = '0' ORDER BY pro_id DESC LIMIT 0,10";
						$c22 = mysqli_query($conn,$query);
						while($r = mysqli_fetch_array($c22))
						{
							$pro_id = $r["pro_id"];
							$pro_name = $r["pro_name"];
							$pro_price = $r["pro_price"];
							$pro_c_id = $r["pro_c_id"];
							$is_discount = $r["is_discount"];
							$discount_price = $r["discount_price"];
							$pro_image_1 = $r["pro_image_1"];
							$handle_finishes = $r["handle_finishes"];
							$handle_sizes = $r["handle_sizes"];
							$material = $r["material"];
							$salt="FgHeT789ios245getHJn";
							$salt2="FgHdT78rios2getHgn2";
							$salt3="FgHdT78rios2getHgn3";
							$salt4="FgHdT78rios2getHgn4";
							$salt5="FgHdT78rios2getHgn5";
							$salt6="FgHdT78rios2getHgn6";
							$id = base64_encode($pro_id . $salt);
							$title = base64_encode($pro_name . $salt2);
							$pro_c_id = base64_encode($pro_c_id . $salt3);
							$handle_finishes = base64_encode($handle_finishes . $salt4);
							$handle_sizes = base64_encode($handle_sizes . $salt5);
							$material = base64_encode($material . $salt6);
                        ?>
						<div><a class="glightbox gallery_product item item1 filter door" href="images/products_images/<?php echo $pro_image_1 ?>" style="color: #000000;">
						  <div class="container3">
						   <img src="images/products_images/<?php echo $pro_image_1 ?>" alt="<?php echo $pro_name ?>" class="product_img">
						   <p class="centered"><strong><?php echo $pro_name ?></strong></p>
						   <?php if($is_discount == 1){ ?>
						   <p class="centered2"><del>&#8377;<?php echo $pro_price ?></del>&nbsp;&#8377;<?php echo $discount_price ?></p>
						   <?php }else if($is_discount == 0) { ?>
						   <p class="centered2">&#8377;<?php echo $pro_price ?></p>
						   <?php } ?>
						   <hr/>
						   <div class="btn-group color_pallets">
						   <?php $query_1 = "SELECT * FROM `products_db` WHERE `pro_parent_id` = '$pro_id'";
											$c2 = mysqli_query($conn,$query_1);
											while($row = mysqli_fetch_array($c2))
											{
												$handle_finishes = $row["handle_finishes"]; ?>
						   <?php if($handle_finishes == 1){ ?>
							<button class="btn_yellow"></button>
						   <?php }else if($handle_finishes == 2){  ?>
							<button class="btn_black"></button><?php  ?>
						   <?php }else if($handle_finishes == 3){  ?>
							<button class="btn_grey"></button><?php } ?>
											<?php } ?>
						  </div>
						  
						</div>
						<a href="product_display.php?cat=<?php echo $id ?>&id=<?php echo $title ?>&p=<?php echo $pro_c_id ?>&h=<?php echo $handle_finishes ?>&s=<?php echo $handle_sizes ?>&m=<?php echo $material ?>" class="popular_add_to_cart_2">View Product</a>
					  </a></div>
						<?php } ?>
						
					</div>
					
				</div>
			</div>
		</div>
        </div>
        <div class="tab-pane fade show active" id="maindoor" role="tabpanel" aria-labelledby="flamingo-tab">
          <div class="container">
			<div class="row align-items-center">
				<div class="col-12 col-carousel">
					<div class="owl-carousel carousel-main">
					 <?php	
				        $query = "SELECT * FROM `products_db` WHERE  `sub_category` = '4' AND `pro_parent_id` = '0'";
						$c22 = mysqli_query($conn,$query);
						while($r = mysqli_fetch_array($c22))
						{
							$pro_id = $r["pro_id"];
							$pro_name = $r["pro_name"];
							$pro_price = $r["pro_price"];
							$pro_c_id = $r["pro_c_id"];
							$is_discount = $r["is_discount"];
							$discount_price = $r["discount_price"];
							$pro_image_1 = $r["pro_image_1"];
							$handle_finishes = $r["handle_finishes"];
							$handle_sizes = $r["handle_sizes"];
							$material = $r["material"];
							$salt="FgHeT789ios245getHJn";
							$salt2="FgHdT78rios2getHgn2";
							$salt3="FgHdT78rios2getHgn3";
							$salt4="FgHdT78rios2getHgn4";
							$salt5="FgHdT78rios2getHgn5";
							$salt6="FgHdT78rios2getHgn6";
							$id = base64_encode($pro_id . $salt);
							$title = base64_encode($pro_name . $salt2);
							$pro_c_id = base64_encode($pro_c_id . $salt3);
							$handle_finishes = base64_encode($handle_finishes . $salt4);
							$handle_sizes = base64_encode($handle_sizes . $salt5);
							$material = base64_encode($material . $salt6);
                        ?>
						<div><a class="glightbox gallery_product item item1 filter door" href="images/products_images/<?php echo $pro_image_1 ?>" style="color: #000000;">
						  <div class="container3">
						   <img src="images/products_images/<?php echo $pro_image_1 ?>" alt="<?php echo $pro_name ?>" class="product_img">
						   <p class="centered"><strong><?php echo $pro_name ?></strong></p>
						   <?php if($is_discount == 1){ ?>
						   <p class="centered2"><del>&#8377;<?php echo $pro_price ?></del>&nbsp;&#8377;<?php echo $discount_price ?></p>
						   <?php }else if($is_discount == 0) { ?>
						   <p class="centered2">&#8377;<?php echo $pro_price ?></p>
						   <?php } ?>
						   <hr/>
						   <div class="btn-group color_pallets">
						   <?php $query_1 = "SELECT * FROM `products_db` WHERE `sub_category` = '4' AND `pro_parent_id` = '$pro_id'";
											$c2 = mysqli_query($conn,$query_1);
											while($row = mysqli_fetch_array($c2))
											{
												$handle_finishes = $row["handle_finishes"]; ?>
						   <?php if($handle_finishes == 1){ ?>
							<button class="btn_yellow"></button>
						   <?php }else if($handle_finishes == 2){  ?>
							<button class="btn_black"></button><?php  ?>
						   <?php }else if($handle_finishes == 3){  ?>
							<button class="btn_grey"></button><?php } ?>
											<?php } ?>
						  </div>
						  
						</div>
						<a href="product_display.php?cat=<?php echo $id ?>&id=<?php echo $title ?>&p=<?php echo $pro_c_id ?>&h=<?php echo $handle_finishes ?>&s=<?php echo $handle_sizes ?>&m=<?php echo $material ?>" class="popular_add_to_cart_2">View Product</a>
					  </a></div>
						<?php } ?>
						
					</div>
					
				</div>
			</div>
		</div>
        </div>
        <div class="tab-pane fade" id="knobs" role="tabpanel" aria-labelledby="profile-tab">
                    <div class="container">
			<div class="row align-items-center">
				<div class="col-12 col-carousel">
					<div class="owl-carousel carousel-main">
					 <?php	
				        $query = "SELECT * FROM `products_db` WHERE  `sub_category` = '6' AND `pro_parent_id` = '0'";
						$c22 = mysqli_query($conn,$query);
						while($r = mysqli_fetch_array($c22))
						{
							$pro_id = $r["pro_id"];
							$pro_name = $r["pro_name"];
							$pro_price = $r["pro_price"];
							$pro_c_id = $r["pro_c_id"];
							$is_discount = $r["is_discount"];
							$discount_price = $r["discount_price"];
							$pro_image_1 = $r["pro_image_1"];
							$handle_finishes = $r["handle_finishes"];
							$handle_sizes = $r["handle_sizes"];
							$material = $r["material"];
							$salt="FgHeT789ios245getHJn";
							$salt2="FgHdT78rios2getHgn2";
							$salt3="FgHdT78rios2getHgn3";
							$salt4="FgHdT78rios2getHgn4";
							$salt5="FgHdT78rios2getHgn5";
							$salt6="FgHdT78rios2getHgn6";
							$id = base64_encode($pro_id . $salt);
							$title = base64_encode($pro_name . $salt2);
							$pro_c_id = base64_encode($pro_c_id . $salt3);
							$handle_finishes = base64_encode($handle_finishes . $salt4);
							$handle_sizes = base64_encode($handle_sizes . $salt5);
							$material = base64_encode($material . $salt6);
                        ?>
						<div><a class="glightbox gallery_product item item1 filter door" href="images/products_images/<?php echo $pro_image_1 ?>" style="color: #000000;">
						  <div class="container3">
						   <img src="images/products_images/<?php echo $pro_image_1 ?>" alt="<?php echo $pro_name ?>" class="product_img">
						   <p class="centered"><strong><?php echo $pro_name ?></strong></p>
						   <?php if($is_discount == 1){ ?>
						   <p class="centered2"><del>&#8377;<?php echo $pro_price ?></del>&nbsp;&#8377;<?php echo $discount_price ?></p>
						   <?php }else if($is_discount == 0) { ?>
						   <p class="centered2">&#8377;<?php echo $pro_price ?></p>
						   <?php } ?>
						   <hr/>
						   <div class="btn-group color_pallets">
						   <?php $query_1 = "SELECT * FROM `products_db` WHERE `sub_category` = '6' AND `pro_parent_id` = '$pro_id'";
											$c2 = mysqli_query($conn,$query_1);
											while($row = mysqli_fetch_array($c2))
											{
												$handle_finishes = $row["handle_finishes"]; ?>
						   <?php if($handle_finishes == 1){ ?>
							<button class="btn_yellow"></button>
						   <?php }else if($handle_finishes == 2){  ?>
							<button class="btn_black"></button><?php  ?>
						   <?php }else if($handle_finishes == 3){  ?>
							<button class="btn_grey"></button><?php } ?>
											<?php } ?>
						  </div>
						  
						</div>
						<a href="product_display.php?cat=<?php echo $id ?>&id=<?php echo $title ?>&p=<?php echo $pro_c_id ?>&h=<?php echo $handle_finishes ?>&s=<?php echo $handle_sizes ?>&m=<?php echo $material ?>" class="popular_add_to_cart_2">View Product</a>
					  </a></div>
						<?php } ?>
						
					</div>
					
				</div>
			</div>
		</div>
        </div>
        <div class="tab-pane fade" id="cabinate" role="tabpanel" aria-labelledby="ostrich-tab">
        <div class="container">
			<div class="row align-items-center">
				<div class="col-12 col-carousel">
					<div class="owl-carousel carousel-main">
					 <?php	
				        $query = "SELECT * FROM `products_db` WHERE  `sub_category` = '5' AND `pro_parent_id` = '0'";
						$c22 = mysqli_query($conn,$query);
						while($r = mysqli_fetch_array($c22))
						{
							$pro_id = $r["pro_id"];
							$pro_name = $r["pro_name"];
							$pro_price = $r["pro_price"];
							$pro_c_id = $r["pro_c_id"];
							$is_discount = $r["is_discount"];
							$discount_price = $r["discount_price"];
							$pro_image_1 = $r["pro_image_1"];
							$handle_finishes = $r["handle_finishes"];
							$handle_sizes = $r["handle_sizes"];
							$material = $r["material"];
							$salt="FgHeT789ios245getHJn";
							$salt2="FgHdT78rios2getHgn2";
							$salt3="FgHdT78rios2getHgn3";
							$salt4="FgHdT78rios2getHgn4";
							$salt5="FgHdT78rios2getHgn5";
							$salt6="FgHdT78rios2getHgn6";
							$id = base64_encode($pro_id . $salt);
							$title = base64_encode($pro_name . $salt2);
							$pro_c_id = base64_encode($pro_c_id . $salt3);
							$handle_finishes = base64_encode($handle_finishes . $salt4);
							$handle_sizes = base64_encode($handle_sizes . $salt5);
							$material = base64_encode($material . $salt6);
                        ?>
						<div><a class="glightbox gallery_product item item1 filter door" href="images/products_images/<?php echo $pro_image_1 ?>" style="color: #000000;">
						  <div class="container3">
						   <img src="images/products_images/<?php echo $pro_image_1 ?>" alt="<?php echo $pro_name ?>" class="product_img">
						   <p class="centered"><strong><?php echo $pro_name ?></strong></p>
						   <?php if($is_discount == 1){ ?>
						   <p class="centered2"><del>&#8377;<?php echo $pro_price ?></del>&nbsp;&#8377;<?php echo $discount_price ?></p>
						   <?php }else if($is_discount == 0) { ?>
						   <p class="centered2">&#8377;<?php echo $pro_price ?></p>
						   <?php } ?>
						   <hr/>
						   <div class="btn-group color_pallets">
						   <?php $query_1 = "SELECT * FROM `products_db` WHERE `sub_category` = '5' AND `pro_parent_id` = '$pro_id'";
											$c2 = mysqli_query($conn,$query_1);
											while($row = mysqli_fetch_array($c2))
											{
												$handle_finishes = $row["handle_finishes"]; ?>
						   <?php if($handle_finishes == 1){ ?>
							<button class="btn_yellow"></button>
						   <?php }else if($handle_finishes == 2){  ?>
							<button class="btn_black"></button><?php  ?>
						   <?php }else if($handle_finishes == 3){  ?>
							<button class="btn_grey"></button><?php } ?>
											<?php } ?>
						  </div>
						  
						</div>
						<a href="product_display.php?cat=<?php echo $id ?>&id=<?php echo $title ?>&p=<?php echo $pro_c_id ?>&h=<?php echo $handle_finishes ?>&s=<?php echo $handle_sizes ?>&m=<?php echo $material ?>" class="popular_add_to_cart_2">View Product</a>
					  </a></div>
						<?php } ?>
						
					</div>
					
				</div>
			</div>
		</div>
        </div>
        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="tropicbird-tab">
                             <div class="container">
			<div class="row align-items-center">
				<div class="col-12 col-carousel">
					<div class="owl-carousel carousel-main">
					 <?php	
				        $query = "SELECT * FROM `products_db` WHERE  `sub_category` = '8' AND `pro_parent_id` = '0'";
						$c22 = mysqli_query($conn,$query);
						while($r = mysqli_fetch_array($c22))
						{
							$pro_id = $r["pro_id"];
							$pro_name = $r["pro_name"];
							$pro_price = $r["pro_price"];
							$pro_c_id = $r["pro_c_id"];
							$is_discount = $r["is_discount"];
							$discount_price = $r["discount_price"];
							$pro_image_1 = $r["pro_image_1"];
							$handle_finishes = $r["handle_finishes"];
							$handle_sizes = $r["handle_sizes"];
							$material = $r["material"];
							$salt="FgHeT789ios245getHJn";
							$salt2="FgHdT78rios2getHgn2";
							$salt3="FgHdT78rios2getHgn3";
							$salt4="FgHdT78rios2getHgn4";
							$salt5="FgHdT78rios2getHgn5";
							$salt6="FgHdT78rios2getHgn6";
							$id = base64_encode($pro_id . $salt);
							$title = base64_encode($pro_name . $salt2);
							$pro_c_id = base64_encode($pro_c_id . $salt3);
							$handle_finishes = base64_encode($handle_finishes . $salt4);
							$handle_sizes = base64_encode($handle_sizes . $salt5);
							$material = base64_encode($material . $salt6);
                        ?>
						<div><a class="glightbox gallery_product item item1 filter door" href="images/products_images/<?php echo $pro_image_1 ?>" style="color: #000000;">
						  <div class="container3">
						   <img src="images/products_images/<?php echo $pro_image_1 ?>" alt="<?php echo $pro_name ?>" class="product_img">
						   <p class="centered"><strong><?php echo $pro_name ?></strong></p>
						   <?php if($is_discount == 1){ ?>
						   <p class="centered2"><del>&#8377;<?php echo $pro_price ?></del>&nbsp;&#8377;<?php echo $discount_price ?></p>
						   <?php }else if($is_discount == 0) { ?>
						   <p class="centered2">&#8377;<?php echo $pro_price ?></p>
						   <?php } ?>
						   <hr/>
						   <div class="btn-group color_pallets">
						   <?php $query_1 = "SELECT * FROM `products_db` WHERE `sub_category` = '8' AND `pro_parent_id` = '$pro_id'";
											$c2 = mysqli_query($conn,$query_1);
											while($row = mysqli_fetch_array($c2))
											{
												$handle_finishes = $row["handle_finishes"]; ?>
						   <?php if($handle_finishes == 1){ ?>
							<button class="btn_yellow"></button>
						   <?php }else if($handle_finishes == 2){  ?>
							<button class="btn_black"></button><?php  ?>
						   <?php }else if($handle_finishes == 3){  ?>
							<button class="btn_grey"></button><?php } ?>
											<?php } ?>
						  </div>
						  
						</div>
						<a href="product_display.php?cat=<?php echo $id ?>&id=<?php echo $title ?>&p=<?php echo $pro_c_id ?>&h=<?php echo $handle_finishes ?>&s=<?php echo $handle_sizes ?>&m=<?php echo $material ?>" class="popular_add_to_cart_2">View Product</a>
					  </a></div>
						<?php } ?>
						
					</div>
				</div>
			</div>
		  </div>
        </div>
		        <div class="tab-pane fade" id="mortice" role="tabpanel" aria-labelledby="ostrich-tab">
                             <div class="container">
			<div class="row align-items-center">
				<div class="col-12 col-carousel">
					<div class="owl-carousel carousel-main">
					 <?php	
				        $query = "SELECT * FROM `products_db` WHERE  `sub_category` = '3' AND `pro_parent_id` = '0'";
						$c22 = mysqli_query($conn,$query);
						while($r = mysqli_fetch_array($c22))
						{
							$pro_id = $r["pro_id"];
							$pro_name = $r["pro_name"];
							$pro_price = $r["pro_price"];
							$pro_c_id = $r["pro_c_id"];
							$is_discount = $r["is_discount"];
							$discount_price = $r["discount_price"];
							$pro_image_1 = $r["pro_image_1"];
							$handle_finishes = $r["handle_finishes"];
							$handle_sizes = $r["handle_sizes"];
							$material = $r["material"];
							$salt="FgHeT789ios245getHJn";
							$salt2="FgHdT78rios2getHgn2";
							$salt3="FgHdT78rios2getHgn3";
							$salt4="FgHdT78rios2getHgn4";
							$salt5="FgHdT78rios2getHgn5";
							$salt6="FgHdT78rios2getHgn6";
							$id = base64_encode($pro_id . $salt);
							$title = base64_encode($pro_name . $salt2);
							$pro_c_id = base64_encode($pro_c_id . $salt3);
							$handle_finishes = base64_encode($handle_finishes . $salt4);
							$handle_sizes = base64_encode($handle_sizes . $salt5);
							$material = base64_encode($material . $salt6);
                        ?>
						<div><a class="glightbox gallery_product item item1 filter door" href="images/products_images/<?php echo $pro_image_1 ?>" style="color: #000000;">
						  <div class="container3">
						   <img src="images/products_images/<?php echo $pro_image_1 ?>" alt="<?php echo $pro_name ?>" class="product_img">
						   <p class="centered"><strong><?php echo $pro_name ?></strong></p>
						   <?php if($is_discount == 1){ ?>
						   <p class="centered2"><del>&#8377;<?php echo $pro_price ?></del>&nbsp;&#8377;<?php echo $discount_price ?></p>
						   <?php }else if($is_discount == 0) { ?>
						   <p class="centered2">&#8377;<?php echo $pro_price ?></p>
						   <?php } ?>
						   <hr/>
						   <div class="btn-group color_pallets">
						   <?php $query_1 = "SELECT * FROM `products_db` WHERE `sub_category` = '3' AND `pro_parent_id` = '$pro_id'";
											$c2 = mysqli_query($conn,$query_1);
											while($row = mysqli_fetch_array($c2))
											{
												$handle_finishes = $row["handle_finishes"]; ?>
						   <?php if($handle_finishes == 1){ ?>
							<button class="btn_yellow"></button>
						   <?php }else if($handle_finishes == 2){  ?>
							<button class="btn_black"></button><?php  ?>
						   <?php }else if($handle_finishes == 3){  ?>
							<button class="btn_grey"></button><?php } ?>
											<?php } ?>
						  </div>
						  
						</div>
						<a href="product_display.php?cat=<?php echo $id ?>&id=<?php echo $title ?>&p=<?php echo $pro_c_id ?>&h=<?php echo $handle_finishes ?>&s=<?php echo $handle_sizes ?>&m=<?php echo $material ?>" class="popular_add_to_cart_2">View Product</a>
					  </a></div>
						<?php } ?>
						
					</div>
					
				</div>
			</div>
		</div>
        </div>
      </div>        
    </div>
</div>    
<!------------------------------POPULAR PRODUCTS-------------------------------------->
<div id="popular_products">
    <div class="container2">
       

        <div class="row">
            <div class="col-md-5">
               <div class="row">
               	<div class="col-md-1" style="max-width: 45px;margin: 0 auto;padding: 0;">
               		<img src="icons/popular.png" alt="featured_logo" style="margin-top: 10px;">
               	</div>
               	<div class="col-md-11" style="padding: 0;">
               		<h1>Popular Products</h1>
               	</div>
               </div>

           </div>
           <div class="col-md-7"></div>
       </div>

       <div id="popular_items">
           <div class="row">
				<?php	
				        $query = "SELECT * FROM `products_db` WHERE `pro_parent_id` = '0' ORDER BY RAND() LIMIT 3";
						$c22 = mysqli_query($conn,$query);
						while($r = mysqli_fetch_array($c22))
						{
						    $pro_id = $r["pro_id"];
							$pro_name = $r["pro_name"];
							$pro_price = $r["pro_price"];
							$pro_c_id = $r["pro_c_id"];
							$is_discount = $r["is_discount"];
							$discount_price = $r["discount_price"];
							$pro_image_1 = $r["pro_image_1"];
							$handle_finishes = $r["handle_finishes"];
							$handle_sizes = $r["handle_sizes"];
							$material = $r["material"];
							$salt="FgHeT789ios245getHJn";
							$salt2="FgHdT78rios2getHgn2";
							$salt3="FgHdT78rios2getHgn3";
							$salt4="FgHdT78rios2getHgn4";
							$salt5="FgHdT78rios2getHgn5";
							$salt6="FgHdT78rios2getHgn6";
							$id = base64_encode($pro_id . $salt);
							$title = base64_encode($pro_name . $salt2);
							$pro_c_id = base64_encode($pro_c_id . $salt3);
							$handle_finishes = base64_encode($handle_finishes . $salt4);
							$handle_sizes = base64_encode($handle_sizes . $salt5);
							$material = base64_encode($material . $salt6);
							
						
		
				?>
			
               <div class="col-md-4">
			   <form action="" method="post">
                   <div class="popular_container">
                   <img src="images/products_images/<?php echo $pro_image_1 ?>" alt="<?php echo $pro_name ?>" class="popular_img2"> 
                   <h3 class="popular_product_name"><strong><?php echo $pro_name ?></strong></h3>
				   <?php if($is_discount == 1){ ?>
				   <h3 class="popular_product_price"><del>&#8377;<?php echo $pro_price ?></del>&nbsp;&#8377;<?php echo $discount_price ?></h3>
				   <?php }else if($is_discount == 0) { ?>
				   <h3 class="popular_product_price">&#8377;<?php echo $pro_price ?></h3>
				   <?php } ?>
				   <div class="btn-group color_pallets2">
				   <?php $query_1 = "SELECT * FROM `products_db` WHERE `pro_parent_id` = '$pro_id'";
							$c2 = mysqli_query($conn,$query_1);
							while($row = mysqli_fetch_array($c2))
							{
		                        $handle_finishes = $row["handle_finishes"]; ?>
                       <div class="btn-group color_pallets">
					   <?php if($handle_finishes == 1){ ?>
						<button class="btn_yellow"></button>
					   <?php }else if($handle_finishes == 2){  ?>
						<button class="btn_black"></button><?php  ?>
					   <?php }else if($handle_finishes == 3){  ?>
						<button class="btn_grey"></button><?php } ?>
					   </div>
							<?php } ?>
                   </div>
                   <div class="row">
                   <a href="product_display.php?cat=<?php echo $id ?>&id=<?php echo $title ?>&p=<?php echo $pro_c_id ?>&h=<?php echo $handle_finishes ?>&s=<?php echo $handle_sizes ?>&m=<?php echo $material ?>" class="popular_add_to_cart">View Product</a>
                   </div>
                   </div>
				</form>
               </div>
						<?php }?>

           </div>
       </div>


    </div>  
</div>



<!-------------------------------CLEARANCE SALE----------------------------------------->


<div id="clearance_sale" style="">

   <div class="container2">

      <div class="row">
         <div class="col-md-7" id="sale_one">

            <div id="sale_one_div">
                <h1>Clearance</h1>
                <h1 class="sale">Sale</h1>
                <p>Aliquam sodales accumsan justo, at fringilla </p>
                  <p style="margin-top: -14px;">  elit pulvinar cursus.Aliquam </p>
                  <p style="margin-top: -14px;">  sodales accumsan justo, at fringilla </p>
               <p style="margin-top: -14px;"> elit pulvinar cursus.</p>

                <button>EXPLORE NOW</button>
            </div>

        </div>
        <div class="col-md-5">

            <div id="sale_two">
                <div id="sale_two_div">
                    <h1>Something</h1>
                    <h1 class="sale">Else</h1>
                    <p>Aliquam sodales accumsan justo, at fringilla 
                        elit pulvinar cursus.Aliquam 
                        sodales accumsan justo, at fringilla 
                    elit pulvinar cursus.</p>
                </div>
            </div>


            <div id="sale_three_gap">
               <div id="sale_three">
                <div id="sale_three_div">
                    <h1>Something</h1>

                </div>
            </div>
        </div>



    </div>
</div>
</div>
</div>




<!--------------------------NEW ARRIVALS----------------------------------->
<div id="new_arrivals" style="">
    <div class="container2">
        <div class="row">
            <div class="col-md-5">
                <div class="row">
               	<div class="col-md-1" style="max-width: 45px;margin: 0 auto;padding: 0;">
               		<img src="icons/popular.png" alt="featured_logo" style="margin-top: 10px;">
               	</div>
               	<div class="col-md-11" style="padding: 0;">
               		<h1>New Arrivals</h1>
               	</div>
               </div>
           </div>
           <div class="col-md-7"></div>
       </div>
        <ul class="nav nav-pills">
          <li class="nav-item">
            <a class="nav-link" data-toggle="pill" href="#handles" role="tab" aria-controls="pills-cuckoo" aria-selected="false">HANDLES</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="pill" href="#doors" role="tab" aria-controls="pills-ostrich" aria-selected="false">DOOR ACCESSORIES</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="pill" href="#baths" role="tab" aria-controls="pills-tropicbird" aria-selected="false">BATH ACCESSORIES</a>
          </li>
        </ul>
        <hr style="margin: 0;padding: 0;" />
        <div class="tab-content mt-3">
        <div class="tab-pane fade show active" id="handles" role="tabpanel" aria-labelledby="flamingo-tab">
          <div class="container">
			<div class="row align-items-center">
				<div class="col-12 col-carousel">
								<div class="owl-carousel carousel-main" style="padding-top: 30px;">
					 <?php	
				        $query = "SELECT * FROM `products_db` WHERE `main_category`='1' AND `pro_parent_id` = '0' ORDER BY pro_id ASC LIMIT 0,10";
						$c22 = mysqli_query($conn,$query);
						while($r = mysqli_fetch_array($c22))
						{
							$pro_id = $r["pro_id"];
							$pro_name = $r["pro_name"];
							$pro_price = $r["pro_price"];
							$pro_c_id = $r["pro_c_id"];
							$is_discount = $r["is_discount"];
							$discount_price = $r["discount_price"];
							$pro_image_1 = $r["pro_image_1"];
							$handle_finishes = $r["handle_finishes"];
							$handle_sizes = $r["handle_sizes"];
							$material = $r["material"];
							$salt="FgHeT789ios245getHJn";
							$salt2="FgHdT78rios2getHgn2";
							$salt3="FgHdT78rios2getHgn3";
							$salt4="FgHdT78rios2getHgn4";
							$salt5="FgHdT78rios2getHgn5";
							$salt6="FgHdT78rios2getHgn6";
							$id = base64_encode($pro_id . $salt);
							$title = base64_encode($pro_name . $salt2);
							$pro_c_id = base64_encode($pro_c_id . $salt3);
							$handle_finishes = base64_encode($handle_finishes . $salt4);
							$handle_sizes = base64_encode($handle_sizes . $salt5);
							$material = base64_encode($material . $salt6);
                        ?>
						<div><a class="glightbox gallery_product item item1 filter door" href="images/products_images/<?php echo $pro_image_1 ?>" style="color: #000000;">
						  <div class="container3">
						   <img src="images/products_images/<?php echo $pro_image_1 ?>" alt="<?php echo $pro_name ?>" class="product_img">
						   <p class="centered"><strong><?php echo $pro_name ?></strong></p>
						   <?php if($is_discount == 1){ ?>
						   <p class="centered2"><del>&#8377;<?php echo $pro_price ?></del>&nbsp;&#8377;<?php echo $discount_price ?></p>
						   <?php }else if($is_discount == 0) { ?>
						   <p class="centered2">&#8377;<?php echo $pro_price ?></p>
						   <?php } ?>
						   <hr/>
						   <div class="btn-group color_pallets">
						   <?php $query_1 = "SELECT * FROM `products_db` WHERE `pro_parent_id` = '$pro_id'";
											$c2 = mysqli_query($conn,$query_1);
											while($row = mysqli_fetch_array($c2))
											{
												$handle_finishes = $row["handle_finishes"]; ?>
						   <?php if($handle_finishes == 1){ ?>
							<button class="btn_yellow"></button>
						   <?php }else if($handle_finishes == 2){  ?>
							<button class="btn_black"></button><?php  ?>
						   <?php }else if($handle_finishes == 3){  ?>
							<button class="btn_grey"></button><?php } ?>
											<?php } ?>
						  </div>
						  
						</div>
						<a href="product_display.php?cat=<?php echo $id ?>&id=<?php echo $title ?>&p=<?php echo $pro_c_id ?>&h=<?php echo $handle_finishes ?>&s=<?php echo $handle_sizes ?>&m=<?php echo $material ?>" class="popular_add_to_cart_2">View Product</a>
					  </a></div>
						<?php } ?>
						
					</div>
					
				</div>
			</div>
		</div>
        </div>
        <div class="tab-pane fade" id="doors" role="tabpanel" aria-labelledby="profile-tab">
                    <div class="container">
			<div class="row align-items-center">
				<div class="col-12 col-carousel">
							<div class="owl-carousel carousel-main">
					 <?php	
				        $query = "SELECT * FROM `products_db` WHERE `main_category`='2' AND `pro_parent_id` = '0' ORDER BY pro_id ASC LIMIT 0,10";
						$c22 = mysqli_query($conn,$query);
						while($r = mysqli_fetch_array($c22))
						{
							$pro_id = $r["pro_id"];
							$pro_name = $r["pro_name"];
							$pro_price = $r["pro_price"];
							$pro_c_id = $r["pro_c_id"];
							$is_discount = $r["is_discount"];
							$discount_price = $r["discount_price"];
							$pro_image_1 = $r["pro_image_1"];
							$handle_finishes = $r["handle_finishes"];
							$handle_sizes = $r["handle_sizes"];
							$material = $r["material"];
							$salt="FgHeT789ios245getHJn";
							$salt2="FgHdT78rios2getHgn2";
							$salt3="FgHdT78rios2getHgn3";
							$salt4="FgHdT78rios2getHgn4";
							$salt5="FgHdT78rios2getHgn5";
							$salt6="FgHdT78rios2getHgn6";
							$id = base64_encode($pro_id . $salt);
							$title = base64_encode($pro_name . $salt2);
							$pro_c_id = base64_encode($pro_c_id . $salt3);
							$handle_finishes = base64_encode($handle_finishes . $salt4);
							$handle_sizes = base64_encode($handle_sizes . $salt5);
							$material = base64_encode($material . $salt6);
                        ?>
						<div><a class="glightbox gallery_product item item1 filter door" href="images/products_images/<?php echo $pro_image_1 ?>" style="color: #000000;">
						  <div class="container3">
						   <img src="images/products_images/<?php echo $pro_image_1 ?>" alt="<?php echo $pro_name ?>" class="product_img">
						   <p class="centered"><strong><?php echo $pro_name ?></strong></p>
						   <?php if($is_discount == 1){ ?>
						   <p class="centered2"><del>&#8377;<?php echo $pro_price ?></del>&nbsp;&#8377;<?php echo $discount_price ?></p>
						   <?php }else if($is_discount == 0) { ?>
						   <p class="centered2">&#8377;<?php echo $pro_price ?></p>
						   <?php } ?>
						   <hr/>
						   <div class="btn-group color_pallets">
						   <?php $query_1 = "SELECT * FROM `products_db` WHERE `pro_parent_id` = '$pro_id'";
											$c2 = mysqli_query($conn,$query_1);
											while($row = mysqli_fetch_array($c2))
											{
												$handle_finishes = $row["handle_finishes"]; ?>
						   <?php if($handle_finishes == 1){ ?>
							<button class="btn_yellow"></button>
						   <?php }else if($handle_finishes == 2){  ?>
							<button class="btn_black"></button><?php  ?>
						   <?php }else if($handle_finishes == 3){  ?>
							<button class="btn_grey"></button><?php } ?>
											<?php } ?>
						  </div>
						  
						</div>
					<a href="product_display.php?cat=<?php echo $id ?>&id=<?php echo $title ?>&p=<?php echo $pro_c_id ?>&h=<?php echo $handle_finishes ?>&s=<?php echo $handle_sizes ?>&m=<?php echo $material ?>" class="popular_add_to_cart_2">View Product</a>
					  </a></div>
						<?php } ?>
						
					</div>
					
				</div>
			</div>
		</div>
        </div>
        <div class="tab-pane fade" id="cabinate" role="tabpanel" aria-labelledby="ostrich-tab">
        <div class="container">
			<div class="row align-items-center">
				<div class="col-12 col-carousel">
								<div class="owl-carousel carousel-main">
					 <?php	
				        $query = "SELECT * FROM `products_db` WHERE `main_category`='3' AND `pro_parent_id` = '0' ORDER BY pro_id ASC LIMIT 0,10";
						$c22 = mysqli_query($conn,$query);
						while($r = mysqli_fetch_array($c22))
						{
							$pro_id = $r["pro_id"];
							$pro_name = $r["pro_name"];
							$pro_price = $r["pro_price"];
							$pro_c_id = $r["pro_c_id"];
							$is_discount = $r["is_discount"];
							$discount_price = $r["discount_price"];
							$pro_image_1 = $r["pro_image_1"];
							$handle_finishes = $r["handle_finishes"];
							$handle_sizes = $r["handle_sizes"];
							$material = $r["material"];
							$salt="FgHeT789ios245getHJn";
							$salt2="FgHdT78rios2getHgn2";
							$salt3="FgHdT78rios2getHgn3";
							$salt4="FgHdT78rios2getHgn4";
							$salt5="FgHdT78rios2getHgn5";
							$salt6="FgHdT78rios2getHgn6";
							$id = base64_encode($pro_id . $salt);
							$title = base64_encode($pro_name . $salt2);
							$pro_c_id = base64_encode($pro_c_id . $salt3);
							$handle_finishes = base64_encode($handle_finishes . $salt4);
							$handle_sizes = base64_encode($handle_sizes . $salt5);
							$material = base64_encode($material . $salt6);
                        ?>
						<div><a class="glightbox gallery_product item item1 filter door" href="images/products_images/<?php echo $pro_image_1 ?>" style="color: #000000;">
						  <div class="container3">
						   <img src="images/products_images/<?php echo $pro_image_1 ?>" alt="<?php echo $pro_name ?>" class="product_img">
						   <p class="centered"><strong><?php echo $pro_name ?></strong></p>
						   <?php if($is_discount == 1){ ?>
						   <p class="centered2"><del>&#8377;<?php echo $pro_price ?></del>&nbsp;&#8377;<?php echo $discount_price ?></p>
						   <?php }else if($is_discount == 0) { ?>
						   <p class="centered2">&#8377;<?php echo $pro_price ?></p>
						   <?php } ?>
						   <hr/>
						   <div class="btn-group color_pallets">
						   <?php $query_1 = "SELECT * FROM `products_db` WHERE `pro_parent_id` = '$pro_id'";
											$c2 = mysqli_query($conn,$query_1);
											while($row = mysqli_fetch_array($c2))
											{
												$handle_finishes = $row["handle_finishes"]; ?>
						   <?php if($handle_finishes == 1){ ?>
							<button class="btn_yellow"></button>
						   <?php }else if($handle_finishes == 2){  ?>
							<button class="btn_black"></button><?php  ?>
						   <?php }else if($handle_finishes == 3){  ?>
							<button class="btn_grey"></button><?php } ?>
											<?php } ?>
						  </div>
						  
						</div>
						<a href="product_display.php?cat=<?php echo $id ?>&id=<?php echo $title ?>&p=<?php echo $pro_c_id ?>&h=<?php echo $handle_finishes ?>&s=<?php echo $handle_sizes ?>&m=<?php echo $material ?>" class="popular_add_to_cart_2">View Product</a>
					  </a></div>
						<?php } ?>
						
					</div>
					
				</div>
			</div>
		</div>
        </div>
      </div>        
    </div>
</div>   
<!-- Footer Starts Here -->
<?php site_footer(); ?>
<script type="text/javascript">
    $(document).ready(function(){
      $('.customer-logos').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 1000,
        arrows: false,
        dots: false,
          pauseOnHover: false,
          responsive: [{
          breakpoint: 768,
          settings: {
            slidesToShow: 4
          }
        }, {
          breakpoint: 520,
          settings: {
            slidesToShow: 3
          }
        }]
      });
    });
  </script>
  <script type="text/javascript">

   jQuery(document).ready(function($) {
    var $owl = $('.owl-carousel');
      $owl.children().each( function( index ) {
        jQuery(this).attr( 'data-position', index ); 
      });
      
      $owl.owlCarousel({
        center: true,
        nav:true,
        loop: true,
        items: 5,
        margin:10,
        responsive:{
          0:{
              items:1
          },
          600:{
              items:3
          },
          1000:{
              items:5
          }
       }
      });
    $(document).on('click', '.item', function() {
      $owl.trigger('to.owl.carousel', $(this).data( 'position' ) ); 
    });
          });
 </script>
</body>
</html>