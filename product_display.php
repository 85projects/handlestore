<?php
session_start(); 
include_once("template-parts/header_links.php");
include_once("template-parts/navbar_m.php");
include_once("template-parts/footer.php");
include_once("includes/main_include.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
  <title>The Handle Store | Main</title>
  <meta name="description" content="Best handle selling website">
  <meta name="author" content="Handle Store">
  <?php header_links(); ?>
</head>
<body>
<script>
       $(document).ready(function(){		   
		$(".form5").submit(function(e){
		  var form_data = $(this).serialize();
		  event.preventDefault();
          $.ajax({
            type: 'POST',
            url: 'get_val.php',
            data: form_data,
            success: function (response) {
			 var result = $.parseJSON(response);
			 $('#cart_display_2026').show().delay(3000).fadeOut(400);
			 document.getElementById("cart_display_2026").innerHTML=result.name;
			 document.getElementById("cart-value").innerHTML=result.credit;
            },
          });
		});
      });
</script>
<!---------------------------HEADER AREA--------------------------------->
<?php bottom_menu(); ?>
<style>
img#display_img {
    height: 600px;
}
.btn-outline-primary{
	 color: #000!important;
    border:1px solid #fbd100!important;
    border-color: #fbd100!important;
}
label.btn {
	  color: #000;
  border:1px solid #fbd100;
  padding: 0;
}
label.btn span:hover {
    color: #000;
    border:1px solid #fbd100!important;
    background: #fbd100!important;
}

label.btn input {
  opacity: 0;
  position: absolute;
}

label.btn span {
  text-align: center;
  padding: 6px 12px;
  display: block;
}

label.btn input:checked+span {
border:1px solid #fbd100;
  background-color: #fbd100;
  color: #fff;
}
label.btn input:hover {
border:1px solid #fbd100;
  background-color: #fbd100;
  color: #fff;
}

/* Product Slider in Here*/
.glass-case * {
    -webkit-box-sizing: border-box !important;
    -moz-box-sizing: border-box !important;
    box-sizing: border-box !important;
    padding: 0;
    margin: 0;
    border: 0;
}

    .glass-case *:before, .glass-case *:after {
        -webkit-box-sizing: border-box !important;
        -moz-box-sizing: border-box !important;
        box-sizing: border-box !important;
    }

.glass-case {
    position: relative;
}
/*********************DISPLAY***************************/
.gc-display-area {
    position: absolute;
    overflow: hidden;
    border: 2px solid #F5F5F5;
    padding: 2px;
    background-color: #fff;
}

.gc-display-container {
    position: relative;
    overflow: hidden;
    top: 50%;
    left: 50%;
    width: 100%;
    height: 100%;
    max-width: 100%;
    max-height: 100%;
}

.gc-display-display {
    position: relative;
    height: 100%;
    width: 100%;
    max-height: 100%;
    max-width: 100%;
    cursor: crosshair;
    opacity: 1;
}
/*********************LENS***************************/
.gc-lens {
    position: absolute;
    overflow: hidden;
    z-index: 1000;
    cursor: crosshair;
    background: #CECECE;
    opacity: .5 !important; /* IE 8 */
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=50)"; /* IE 5-7 */
    filter: alpha(opacity=50) !important; /* Netscape */
    -moz-opacity: 0.5; /* Safari 1.x */
    -khtml-opacity: 0.5;
}
/*********************ZOOM***************************/
.gc-zoom {
    position: absolute;
    /overflow: hidden;
    width: 100%;
    height: auto;
    z-index: 1040 !important;
    border: 2px solid #f5f5f5;
    padding: 2px;
    background-color: #fff;
}

.gc-zoom-inner {
    cursor: crosshair;
    border: 0 !important;
    left: -2px !important;
    top: -2px !important;
    margin: 0 !important;
    z-index: 1010 !important;
}

.gc-zoom-container {
    position: relative;
    top: 0;
    left: 0;
    overflow: hidden;
    width: 100%;
    height: 100%;
    max-width: 100%;
    max-height: 100%;
}

    .gc-zoom-container img {
        position: relative;
        max-width: none !important;
    }
/*********************CAPTION**********************/
.gc-caption-container {
    position: absolute;
    overflow: hidden;
    width: 100%;
    padding: 2px;
    z-index: 1050 !important;
    background-color: rgba(255, 255, 255, 0.5);
    line-height: 1.6;
    font-family: Georgia, "Times New Roman", Times, serif;
    font-style: italic;
    color: #333;
    font-size: 0.9em;
}

    .gc-caption-container div {
        position: relative;
        padding: 0 12px 0 12px;
    }

.gc-caption-intop {
    left: 0;
    top: 0;
}

.gc-caption-inbottom {
    left: 0;
    bottom: 0;
}

.gc-caption-outtop, .gc-caption-outbottom {
    width: 100%;
    width: calc(100% + 4px);
    border: 2px solid #f5f5f5;
    background: #fff;
}

.gc-caption-outtop {
    top: 0;
    left: -2px;
    -webkit-transform: translate(0, -100%);
    -moz-transform: translate(0, -100%);
    -ms-transform: translate(0, -100%);
    -o-transform: translate(0, -100%);
    transform: translate(0, -100%);
}

.gc-caption-outbottom {
    bottom: 0;
    left: -2px;
    -webkit-transform: translate(0, 100%);
    -moz-transform: translate(0, 100%);
    -ms-transform: translate(0, 100%);
    -o-transform: translate(0, 100%);
    transform: translate(0, 100%);
}

.gc-alignment-center {
    text-align: center;
}

.gc-alignment-left {
    text-align: left;
}

.gc-alignment-right {
    text-align: right;
}

/*********************UL***************************/
.gc-thumbs-area {
    position: absolute;
    overflow: hidden;
    background-color: transparent;
}

.gc-thumbs-area-prev, .gc-thumbs-area-next {
    position: absolute;
    z-index: 1010;
}

.gc-thumbs-area.gc-hz .gc-thumbs-area-prev, .gc-thumbs-area.gc-hz .gc-thumbs-area-next {
    top: 0;
    height: 100%;
    min-height: 100%;
    width: 5%;
}

.gc-thumbs-area.gc-vt .gc-thumbs-area-prev, .gc-thumbs-area.gc-vt .gc-thumbs-area-next {
    left: 0;
    width: 100%;
    min-width: 100%;
    height: 5%;
}

.gc-thumbs-area.gc-hz .gc-thumbs-area-prev {
    left: 0;
}

.gc-thumbs-area.gc-hz .gc-thumbs-area-next {
    right: 0;
}

.gc-thumbs-area.gc-vt .gc-thumbs-area-prev {
    top: 0;
}

.gc-thumbs-area.gc-vt .gc-thumbs-area-next {
    bottom: 0;
}

.gc-thumbs-area-prev:hover, .gc-thumbs-area-next:hover {
    opacity: 1;
    cursor: pointer;
    cursor: hand;
}

.gc-thumbs-area-prev .gc-disabled, .gc-thumbs-area-next .gc-disabled, .gc-thumbs-area-prev .gc-disabled:hover, .gc-thumbs-area-next .gc-disabled:hover {
    opacity: .3; /* IE 8 */
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=30)"; /* IE 5-7 */
    filter: alpha(opacity=30); /* Netscape */
    -moz-opacity: 0.3; /* Safari 1.x */
    -khtml-opacity: 0.3;
    cursor: default;
}

.glass-case ul {
    position: relative;
    top: 0;
    left: 0;
    margin: 0;
    border: 0;
    padding: 0;
    height: 100%;
    list-style: none;
}

    .glass-case ul li {
        position: relative;
        overflow: hidden;
        width: 100%;
        height: 100%;
        border: 2px solid #f5f5f5;
        padding: 2px;
        background-color: #fff;
    }

.gc-thumbs-area.gc-hz li {
    display: inline;
    float: left;
    max-width: 100%;
    max-height: 100%;
    margin-right: 5px;
}

.gc-thumbs-area.gc-vt li {
    margin-bottom: 5px;
}

.glass-case ul li .gc-li-display-container {
    position: relative;
    overflow: hidden;
    height: 100%;
    width: 100%;
    display: inline-block;
}

.glass-case ul li:hover {
    border-color: #4f4f4f;
    cursor: pointer;
    cursor: hand;
}

.glass-case ul li.gc-active, .glass-case ul li.gc-active:hover {
    border-color: #669966;
    cursor: default;
}

.glass-case ul li .gc-li-display-container img {
    position: relative;
    top: 50%;
    left: 50%;
}
/*********************OVERLAY***************************/
.gc-overlay-area {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 2147483647 !important;
    background-color: rgb(204, 204, 204); /*the falback for ie7, ie8*/
    background-color: rgba(51, 51, 51, 0.95);
}

.gc-overlay-gcontainer {
    position: relative;
    width: 100%;
    height: 100%;
    padding: 0;
}

.gc-overlay-container {
    position: relative;
    overflow: auto;
    width: 100%;
    height: 100%;
    padding: 0;
}

.gc-overlay-container-display {
    position: absolute;
    overflow: auto;
    width: 100%;
    height: 100%;
    padding: 0;
}

.gc-ocd-top {
    top: 0;
}

.gc-ocd-bottom {
    bottom: 0;
}

.gc-overlay-fit {
    padding: 4em 5.5em 1em 5.5em;
    overflow: hidden;
}

.gc-overlay-display {
    position: relative;
    top: 0;
    left: 0;
    display: block;
    cursor: crosshair;
}

.gc-overlay-display-center {
    max-width: 100%;
    max-height: 100%;
    top: 50%;
    left: 50%;
    -webkit-transform: translate(-50%, -50%);
    -moz-transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    -o-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
}

.gc-overlay-display-vcenter {
    top: 50%;
    -webkit-transform: translateY(-50%);
    -moz-transform: translateY(-50%);
    -ms-transform: translateY(-50%);
    -o-transform: translateY(-50%);
    transform: translateY(-50%);
}

.gc-overlay-display-hcenter {
    left: 50%;
    -webkit-transform: translateX(-50%);
    -moz-transform: translateX(-50%);
    -ms-transform: translateX(-50%);
    -o-transform: translateX(-50%);
    transform: translateX(-50%);
}

.gc-overlay-top-icons {
    position: absolute;
    top: 0;
    padding: 1em 1.5em 1em 0;
    width: 100%;
    height: 4em;
}

.gc-overlay-left-icons {
    position: absolute;
    height: 100%;
    top: 0;
    left: 0;
    padding: 0 1em 0 1.5em;
    height: 100%;
    width: 5.5em;
}

.gc-overlay-right-icons {
    position: absolute;
    top: 0;
    right: 0;
    padding: 0 1.5em 0 1em;
    height: 100%;
    width: 5.5em;
}
/*********************ICONS***************************/
@charset "UTF-8";

@font-face {
    font-family: "linea-arrows-10";
    src: url("../fonts/linea-arrows-10.eot");
    src: url("../fonts/linea-arrows-10.eot?#iefix") format("embedded-opentype"), url("../fonts/linea-arrows-10.woff") format("woff"), url("../fonts/linea-arrows-10.ttf") format("truetype"), url("../fonts/linea-arrows-10.svg#linea-arrows-10") format("svg");
    font-weight: normal;
    font-style: normal;
}

.gc-icon:before {
    line-height: 1;
}

.gc-icon {
    font-family: "linea-arrows-10" !important;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    position: absolute;
    z-index: 1020;
    cursor: pointer;
    cursor: hand;
    color: #fff;
    opacity: .7; /* IE 8 */
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=70)"; /* IE 5-7 */
    filter: alpha(opacity=70); /* Netscape */
    -moz-opacity: 0.7; /* Safari 1.x */
    -khtml-opacity: 0.7;
    height: 1em;
    width: 1em;
    line-height: 0;
    background: rgb(204, 204, 204);
    background: rgba(204, 204, 204, 0.5);
    border: 2px solid #F5F5F5;
}

    .gc-icon:hover {
        opacity: 1; /* IE 8 */
        -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)"; /* IE 5-7 */
        filter: alpha(opacity=100); /* Netscape */
        -moz-opacity: 1; /* Safari 1.x */
        -khtml-opacity: 1;
    }

.gc-disabled .gc-icon:hover, .gc-disabled .gc-icon:hover, .gc-disabled .gc-icon, .gc-disabled .gc-icon {
    opacity: .3; /* IE 8 */
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=30)"; /* IE 5-7 */
    filter: alpha(opacity=30); /* Netscape */
    -moz-opacity: 0.3; /* Safari 1.x */
    -khtml-opacity: 0.3;
    cursor: default;
}

/* *** ICON NEXT *** */
.gc-icon-next {
    top: 50%;
    right: -2px;
}

    .gc-icon-next:before {
        position: absolute;
        top: -2px;
        right: -2px;
        content: "\e04b";
    }

.gc-icon-next-vt {
    bottom: 0;
    left: 50%;
}

    .gc-icon-next-vt:before {
        position: absolute;
        top: -2px;
        left: -2px;
        content: "\e019";
    }

.gc-display-area .gc-icon-next {
    font-size: 2em !important;
}

.gc-thumbs-area-next .gc-icon-next {
    right: 0;
    font-size: 2em !important;
}

.gc-thumbs-area-next .gc-icon-next-vt {
    font-size: 1.5em !important;
}

.gc-overlay-area .gc-icon-next {
    position: relative;
    right: 0;
    font-size: 3em;
}
/* *** ICON PREV *** */
.gc-icon-prev {
    top: 50%;
    left: -2px;
}

    .gc-icon-prev:before {
        position: absolute;
        top: -2px;
        left: -2px;
        content: "\e03f";
    }

.gc-icon-prev-vt {
    top: 0;
    left: 50%;
}

    .gc-icon-prev-vt:before {
        position: absolute;
        top: -2px;
        left: -2px;
        content: "\e083";
    }

.gc-display-area .gc-icon-prev {
    font-size: 2em !important;
}

.gc-thumbs-area-prev .gc-icon-prev {
    font-size: 2em !important;
}

.gc-thumbs-area-prev .gc-icon-prev-vt {
    font-size: 1.5em !important;
}

.gc-overlay-area .gc-icon-prev {
    position: relative;
    left: 0;
    font-size: 3em;
}
/* *** ICON DOWNLOAD *** */
.gc-icon-download {
    right: -2px;
    bottom: 0;
    font-size: 2em;
    overflow: hidden;
}

    .gc-icon-download:before {
        position: absolute;
        top: -4px;
        left: -2px;
        content: "\e064";
    }
/* *** ICON CLOSE *** */
.gc-icon-close {
    position: relative;
    font-size: 2em;
    float: right;
}

    .gc-icon-close:before {
        position: absolute;
        content: "\e04a";
        top: -2px;
        right: -2px;
    }

.gc-icon-enlarge {
    position: relative;
    float: right;
    margin-right: 0.25em;
    font-size: 2em;
}

    .gc-icon-enlarge:before {
        position: absolute;
        content: '\e017';
        top: -2px;
        right: -2px;
    }

.gc-icon-compress {
    position: relative;
    float: right;
    margin-right: 0.25em;
    font-size: 2em;
}

    .gc-icon-compress:before {
        position: absolute;
        content: '\e053';
        top: -2px;
        right: -2px;
    }
/*********************LOADING***************************/
.gc-loading {
    position: absolute;
    top: 50%;
    left: 50%;
    width: 31px;
    height: 31px;
    margin-left: -15px;
    margin-top: -15px;
    background-image: url(../images/loader.gif);
}

.gc-loading3 {
    position: absolute;
    top: 50%;
    left: 50%;
    margin-left: -15px;
    margin-top: -15px;
    font-size: 0.25em;
    text-indent: -9999em;
    border-top: 1.1em solid rgba(102, 153, 102, 0.2);
    border-right: 1.1em solid rgba(102, 153, 102, 0.2);
    border-bottom: 1.1em solid rgba(102, 153, 102, 0.2);
    border-left: 1.1em solid #669966;
    -webkit-animation: loadGC 1.1s infinite linear;
    animation: loadGC 1.1s infinite linear;
}

    .gc-loading3,
    .gc-loading3:after {
        border-radius: 50%;
        width: 30px;
        height: 30px;
    }

@-webkit-keyframes loadGC {
    0% {
        -webkit-transform: rotate(0deg);
        transform: rotate(0deg);
    }

    100% {
        -webkit-transform: rotate(360deg);
        transform: rotate(360deg);
    }
}

@keyframes loadGC {
    0% {
        -webkit-transform: rotate(0deg);
        transform: rotate(0deg);
    }

    100% {
        -webkit-transform: rotate(360deg);
        transform: rotate(360deg);
    }
}
/*********************GENERAL***************************/
.gc-hide {
    display: none;
    z-index: 0;
    opacity: 0; /* IE 8 */
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)"; /* IE 5-7 */
    filter: alpha(opacity=0); /* Netscape */
    -moz-opacity: 0; /* Safari 1.x */
    -khtml-opacity: 0;
}

.gc-noscroll {
    overflow: hidden;
}

.gc-start {
    display: none;
    z-index: 0;
    opacity: 0; /* IE 8 */
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)"; /* IE 5-7 */
    filter: alpha(opacity=0); /* Netscape */
    -moz-opacity: 0; /* Safari 1.x */
    -khtml-opacity: 0;
}
#cart_display_2026{
	position: fixed;
    bottom:40px;
    right: 20px;
	padding:10px;
	box-shadow: 0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22);
	font-size:1em;
	z-index:50;
	background:#fff;
	text-align: center;
	line-height: 1;
	color:#000;
	width:300px;
	height:auto;
}
</style>


<!-------------------------------PRODUCT SHOWCASE------------------------------------->

<div class="product_showcase">
  <div class="container2">
    <?php	
	if(isset($_GET["cat"])){
	$main_pro_id  = 0;
	$id = $_GET["cat"];
	$title = $_GET["id"];
	$pro_c_id = $_GET["p"];
	$handle_finishes = $_GET["h"];
	$handle_sizes = $_GET["s"];
	$material = $_GET["m"];
	/*1st*/
	$salt="FgHeT789ios245getHJn";
	$decrypted_id_raw = base64_decode($id);
    $decrypted_id = preg_replace(sprintf('/%s/', $salt), '', $decrypted_id_raw);
	
	/*2nd*/
	$salt2="FgHeT789ios245getHJn2";
	$decrypted_id_raw2 = base64_decode($title);
	$decrypted_id2 = preg_replace(sprintf('/%s/', $salt2), '', $decrypted_id_raw2);
	
	/*3rd*/
	$salt3="FgHdT78rios2getHgn3";
	$decrypted_id_raw3 = base64_decode($pro_c_id);
	$decrypted_id3 = preg_replace(sprintf('/%s/', $salt3), '', $decrypted_id_raw3);
	
	/*4th*/
	$salt4="FgHdT78rios2getHgn4";
	$decrypted_id_raw4 = base64_decode($handle_finishes);
	$decrypted_id4 = preg_replace(sprintf('/%s/', $salt4), '', $decrypted_id_raw4);
	
	/*5th*/
	$salt5="FgHdT78rios2getHgn5";
	$decrypted_id_raw5 = base64_decode($handle_sizes);
	$decrypted_id5 = preg_replace(sprintf('/%s/', $salt5), '', $decrypted_id_raw5);
	
	/*6th*/
	$salt6="FgHdT78rios2getHgn6";
	$decrypted_id_raw6 = base64_decode($material);
	$decrypted_id6 = preg_replace(sprintf('/%s/', $salt6), '', $decrypted_id_raw6);
	$query = "SELECT * FROM `products_db` WHERE `pro_c_id` = '$decrypted_id' AND `handle_finishes`='$decrypted_id4' AND `handle_sizes`='$decrypted_id5' AND `material`='$decrypted_id6' AND `status` = '1'"; 
	$c2 = mysqli_query($conn,$query);
	$my_num = mysqli_num_rows($c2);
	if($my_num > 0){
	while($row = mysqli_fetch_array($c2))
	{
						$pro_id = $row["pro_id"];
						$pro_c_id = $row["pro_c_id"];
						$pro_name = $row["pro_name"];
						$pro_details = $row["pro_details"];
						$pro_price = $row["pro_price"];
						$main_category = $row["main_category"];
						$is_discount = $row["is_discount"];
						$discount_price = $row["discount_price"];
						$discount_perc = $row["discount_perc"];
						$pro_image_1 = $row["pro_image_1"];
						$pro_image_2 = $row["pro_image_2"];
						$pro_image_3 = $row["pro_image_3"];
						$pro_image_4 = $row["pro_image_4"];
					    $material_m = $row["material"];
						$handle_finishes_m = $row["handle_finishes"];
						$handle_sizes_m = $row["handle_sizes"];	
						$pro_parent_id = $row["pro_parent_id"];	
						if($pro_parent_id == 0){
							/*Main Product*/
						$zx = 0;
					    $pro_p_id =$pro_c_id;
							
						}else{
							
							$zx = 1;
						    $pro_p_id = $pro_c_id;
							
						}
						$salt="FgHeT789ios245getHJn";
						$salt2="FgHdT78rios2getHgn2";
						$salt3="FgHdT78rios2getHgn3";
						
						$id = base64_encode($pro_p_id . $salt);
						$title = base64_encode($pro_name . $salt2);
						$pro_c_id = base64_encode($pro_c_id . $salt3);
						
    ?>
    <div class="row">
      <div class="col-md-6">

       <!-- Image start here -->
	   <div class="container">
	<div class="">
                <ul id="glasscase" class="gc-start">
                    <li><img src="images/products_images/<?php echo $pro_image_1 ?>" alt="Text"/></li>
                    <li><img src="images/products_images/<?php echo $pro_image_2 ?>" alt="Text" /></li>
                    <li><img src="images/products_images/<?php echo $pro_image_2 ?>" alt="Text" /></li>
                    <li><img src="images/products_images/<?php echo $pro_image_4 ?>" alt="Text" /></li>
                </ul>
            </div>
</div>
<script type="text/javascript">
        $(document).ready( function () {
            //If your <ul> has the id "glasscase"
            $('#glasscase').glassCase({ 'thumbsPosition': 'bottom', 'widthDisplay' : 560});
        });
    </script>
	<!-- Image Ends Here -->
      </div>
	
      <div class="col-md-6" id="product_details" style="padding-left: 40px;padding-right: 40px;">    
<form action="" method="GET">	  
<input type="hidden" value="<?php echo $id ?>" name="cat">
<input type="hidden" value="<?php echo $title ?>" name="id">
<input type="hidden" value="<?php echo $pro_c_id ?>" name="p">
<input type="hidden" value="<?php echo $zx ?>" name="zx">
<input type="hidden" value="<?php echo $zx ?>" name="zx">

        <div class="row">
          <div class="col-md-12">
            <h1><?php echo $pro_name ?></h1>
            <!--<p class="code">CODE : KH114</p> -->
            <h4 class="price">&#8377; <?php echo $pro_price ?></h4>
           <!-- <p class="offer">15% off</p> -->
          </div>
        </div>
         <hr/>
         <div class="row">
            <div class="col-md-6">
             <p class="select_material_p">MATERIAL</p>
            <div class="radio-toolbar-1">
			<?php 
				
			$query_1 = "SELECT distinct `material` FROM `products_db` WHERE `pro_c_id` = '$decrypted_id3' AND `status` = '1'";				 
				  $c2_1 = mysqli_query($conn,$query_1);
				  while($row = mysqli_fetch_array($c2_1))
			      {		$material = $row["material"];
						$query = "SELECT `mat_id`, `mat_name` FROM `pro_material` WHERE `mat_id` = '$material'";
						$c22 = mysqli_query($conn,$query);
						while($r = mysqli_fetch_array($c22))
						{
						    $mat_id = $r["mat_id"];
							$material_name = $r["mat_name"];
						    $salt6="FgHdT78rios2getHgn6";
						    $material = base64_encode($mat_id . $salt6);
							
			?>
			
			   <label class="btn"><input  type="radio" name="m" value="<?php echo $material ?>"  <?php if ($mat_id == $material_m) {
            echo ' checked="checked" ';
			} ?>/>
				  <span><?php echo $material_name ?></span></label> <?php }} ?>
             </div>
			</div>
			<div class="col-md-6" >
             <p class="select_size_p">SELECT SIZE</p>
             <div class="radio-toolbar-1">
			 <?php 

				
			      $query_1 = "SELECT distinct `handle_sizes` FROM `products_db` WHERE `pro_c_id` = '$decrypted_id3' AND `status` = '1'";				
				  $c2_1 = mysqli_query($conn,$query_1);
				  while($row = mysqli_fetch_array($c2_1))
			      {		$handle_sizes = $row["handle_sizes"];
						$query = "SELECT * FROM `handle_sizes` WHERE `size_id` = '$handle_sizes'";
						$c22 = mysqli_query($conn,$query);
						while($r = mysqli_fetch_array($c22))
						{
						    $size_id = $r["size_id"];
							$size_name = $r["size_name"];
						    $salt5="FgHdT78rios2getHgn5";
						    $handle_sizes = base64_encode($size_id . $salt5); 
							
			?>
			 <label class="btn"> <input  type="radio" name="s" value="<?php echo $handle_sizes ?>" <?php if ($size_id == $handle_sizes_m) {
            echo ' checked="checked" ';
			} ?>>
               <span><?php echo $size_name ?></span></label> 
				  <?php }} ?>
             </div>
           </div> </div>
		    <div class="row">
              <div class="col-md-12">
               <p class="select_finish_p">SELECT FINISH</p>
                    <div class="radio-toolbar-1">
			   <?php 
                
$query_1 = "SELECT distinct `handle_finishes` FROM `products_db` WHERE `pro_c_id` = '$decrypted_id3' AND `status` = '1'";			
			
			   
				  $c2_1 = mysqli_query($conn,$query_1);
				  while($row = mysqli_fetch_array($c2_1))
			      {		$handle_finishes = $row["handle_finishes"];
					    $query = "SELECT `fin_id`, `finish_name` FROM `handle_finishes` WHERE `fin_id` = '$handle_finishes'";
						$c22 = mysqli_query($conn,$query);
						while($r = mysqli_fetch_array($c22))
						{
						    $fin_id = $r["fin_id"];
							$finish_name = $r["finish_name"];
							$salt4="FgHdT78rios2getHgn4";
							$handle_finishes = base64_encode($fin_id . $salt4);
							
			?>
			   <label class="btn"><input type="radio" name="h" value="<?php echo $handle_finishes ?>" <?php if ($fin_id == $handle_finishes_m) {
            echo ' checked="checked" ';
			} ?>>
			   
               <span><?php echo $finish_name ?></span></label> 
				  <?php }} ?>
             </div>
             </div>
            </div>
			 <div class="row">
           <div class="col-md-6 delivery_option">
		    
             <input type="submit" name="sub-x" class="final_add_cart" value="Check Variations" style="width: 320px;height: 68px;background-color: #fbd100;border:none;border-radius: 6px;"></div></div>
           </form>
        
        

      <!--   <div class="row">
           <div class="col-md-12 delivery_option">
             <p class="delivery_p">DELIVERY OPTION</p>
             <div class="btn-group">
               <input type="number" name="" placeholder="PIN CODE">
               <button>check</button>
             </div>
             <p>Please enter your pincode to check delivery options</p> -->
             <hr/>
             
	<form name="form5" method="post" class="form5">
	  <input name="s_id" id="" type="hidden" value="<?php echo $pro_id ?>"/>
	  <input type="submit" class="final_add_cart" value="Add to Cart" name="add-to-cart" style="width: 320px;height: 68px;background-color: #fbd100;
	  border:none;
	  border-radius: 6px;">
	  </form>
           </div>
         </div>

	
  
 
      </div>
	
    </div>
	
   

  </div> 
</div>
<div>
</div>
 <div class="bottom_div">
  <div class="container2">
      <div class="row">
        <div class="col-md-6">
         <ul>
           <li><h6>PRODUCT DESCRIPTION</h6></li>
           <li><h6>REVIEWS</h6></li>
           <li><h6>FAQ's</h6></li>
         </ul>
         <p><?php echo $pro_details ?></p>
        </div>
        <div class="col-md-6">
          <img src="background/popular1.png" class="img-fluid" id="add_banner">
        </div>
      </div>  
      </div>   
    </div>
	<?php }  }}else{
		echo "No Product Found";
	} ?>














<!------------------------------SIMILAR PRODUCTS------------------------------------------>


<div class="similar_products">
  
  <div class="container2">
   
   <div class="row">
    <div class="col-md-5">
      <h1>Similar Products</h1>
    </div>
    <div class="col-md-7"></div>
   </div> 

   <div class="similar_products_display">
     <div class="row">
	<?php 	$query = "SELECT * FROM `products_db` WHERE `main_category` = '2' AND `status` = '1' ORDER BY `pro_id` LIMIT 0,6";
			$c2 = mysqli_query($conn,$query);
			while($row = mysqli_fetch_array($c2))
			{
						$pro_id = $row["pro_id"];
						$pro_name = $row["pro_name"];
						$pro_details = $row["pro_details"];
						$pro_price = $row["pro_price"];
						$is_discount = $row["is_discount"];
						$discount_price = $row["discount_price"];
						$discount_perc = $row["discount_perc"];
						$pro_image_1 = $row["pro_image_1"]; ?>
       <div class="col-md-3 text-center">
            <div class="container3">                     
                     <img src="images/products_images/<?php echo $pro_image_1 ?>" alt="product_img" class="product_img">
                     <h5 class="arrival_product_text1"><strong><?php echo $pro_name ?></strong></h5>
                     <h5 class="arrival_product_text2">&#8377;<?php echo $pro_price ?></h5>
                     <hr/>
                    <div class="btn-group color_pallets">
                        <button class="btn_yellow"></button>
                        <button class="btn_black"></button>
                        <button class="btn_grey"></button>
                    </div>

                       </div>  
                       <button class="add_to_cart" type="button">ADD TO CART+</button>
       </div>
			<?php } ?>
       
      
      
     </div>



   </div>

  </div>

</div>
<div class="success-msg" id="cart_display_2026">
</div>
<!-- Footer Starts Here -->
<script>
	$(document).ready(function(){
	document.getElementById('cart_display_2026').style.display = "none";
	});
</script> 
<script src="js/zoom-slider.js"></script>
<?php site_footer(); ?>
</body>
</html>