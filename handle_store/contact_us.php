<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">

  <title>The Handle Store | Contact US</title>

  <link href="https://fonts.googleapis.com/css?family=Roboto|Courgette|Pacifico:400,700" rel="stylesheet">
  <meta name="description" content="Best handle selling website">
  <meta name="author" content="Handle Store">

  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="style.css" rel="stylesheet">

<!--   <script src="https://kit.fontawesome.com/f000cd9cea.js" crossorigin="anonymous"></script> -->

<!----created date:15 sep 2020-------->
</head>
<body>

<!---------------------------HEADER AREA--------------------------------->

<header id="header_area">
	<div class="container">
     <div class="row">


     <div class="col-md-5" >
     	<a href="index.php"><img src="logos/handle_logo.png" alt="The hangle store" id="store_logo"></a>

     	<input type="" name="" placeholder="Search for handles,knobs and more;">
     </div>	
     <div id="main_menu" class="col-md-7">
     	
     	<nav id="menu">
     		<ul id="nav">
     			<li><a href="">MAIN DOOR</a></li>
     			<li><a href="">KITCHEN</a></li>
     			<li><a href="">CABINET</a></li>
     			<li><a href="">KNOBS</a></li>
     			<li><a href="">PROFILES</a></li>
     			<li><a href="">MORTICE</a></li>
     			<li><button>Cart</button></li>

     		</ul>
     	</nav>

     </div>	

     </div>		
	</div>	
</header>













<div class="contact">
  <div class="container2">
    
    <div class="container">
  <div class="row">
    <div class="col-md-8 col-md-offset-2 m-auto">
      <div class="contact-form">
        <h1>Get in Touch</h1>
        <form action="/examples/actions/confirmation.php" method="post">
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label for="inputName">Name</label>
                <input type="text" class="form-control" id="inputName" required>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label for="inputEmail">Email</label>
                <input type="email" class="form-control" id="inputEmail" required>
              </div>
            </div>
          </div>            
          <div class="form-group">
            <label for="inputSubject">Subject</label>
            <input type="text" class="form-control" id="inputSubject" required>
          </div>
          <div class="form-group">
            <label for="inputMessage">Message</label>
            <textarea class="form-control" id="inputMessage" rows="5" required></textarea>
          </div>
          <div class="text-center">
            <button type="submit" class="btn btn-primary"><i class="fa fa-paper-plane"></i> Send</button>
          </div>            
        </form>
      </div>
    </div>
  </div>
</div>
  </div>
</div>



















































<!---------------------------FOOTER AREA--------------------------------->

<footer id="footer_area" style="background-color: #f5f6f8;">

  <div class="container">
    
    
    <div class="row">
    
    <div class="col-md-4" id="left_part_footer" >

             <a href="index.php"><img src="logos/handle_logo.png" alt="The hangle store" id="store_logo"></a>
      <p>Aliquam sodales accumsan justo, at fringilla 
elit pulvinar cursus.Aliquam sodales accumsan justo, 
at fringilla elit pulvinar cursus.</p>

    </div>  
    <div class="col-md-2">
      <ul>
      <a href=""><li>MAIN DOOR</li></a>
      <a href=""><li>KITCHEN</li></a>   
      <a href=""><li>CABINET</li></a> 
      <a href=""><li>KNOBS</li></a> 
      <a href=""><li>PROFILES</li></a>  
      <a href=""><li>MORTICE</li></a> 

      </ul>
    </div>
    <div class="col-md-2">
      <ul>
      <a href=""><li>ABOUT US</li></a>
      <a href=""><li>CONTACT US</li></a>    
      <a href=""><li>PRIVACY POLICY</li></a>  
      <a href=""><li>FAQ'S</li></a> 
      <a href=""><li>WARRANTY</li></a>  
      <a href=""><li>T&C</li></a> 
      <a href=""><li>RETURN POLICY</li></a> 

      </ul>
    </div>
    <div class="col-md-4"  id="social_media">
      
      <h4>Social Media</h4>

      <div class="btn-group" id="button_group_social">
        <button></button>
        <button></button>
        <button></button>
        <button></button>
      </div>


    </div>

    </div>


  </div>
  

</footer>































<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>

</body>
</html>