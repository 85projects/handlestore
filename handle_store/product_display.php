<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">

  <title>The Handle Store | Product Display</title>

  <meta name="description" content="Best handle selling website">
  <meta name="author" content="Handle Store">

  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="style.css" rel="stylesheet">

  <script src="https://kit.fontawesome.com/f000cd9cea.js" crossorigin="anonymous"></script>

<!----created date:17 sep 2020-------->
</head>
<body>

<!---------------------------HEADER AREA--------------------------------->

<header id="header_area">
  <div class="container">
     <div class="row">


     <div class="col-md-5" >
            <a href="index.php" class="navbar-brand"><img src="logos/handle_logo.png" alt="The hangle store" id="store_logo"></a>

      <input type="" name="" placeholder="Search for handles,knobs and more;">
     </div> 
     <div id="main_menu" class="col-md-7">
      
      <nav id="menu" class="navbar navbar-expand-sm ">
        <div class="row">
          <div class="col-md-12 text-right">
            
          </div>
        </div>
         <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar" >
        <i class="fas fa-bars"></i>
  </button>

        <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul id="nav" class="navbar-nav">
          <li class="nav-item"><a href="" class="nav-link">MAIN DOOR</a></li>
          <li class="nav-item"><a href="" class="nav-link">KITCHEN</a></li>
          <li class="nav-item"><a href="" class="nav-link">CABINET</a></li>
          <li class="nav-item"><a href="" class="nav-link">KNOBS</a></li>
          <li class="nav-item"><a href="" class="nav-link">PROFILES</a></li>
          <li class="nav-item"><a href="" class="nav-link">MORTICE</a></li>
          <li class="nav-item"><button id="main_cart_btn"><i class="fas fa-shopping-cart"></i> Cart</button></li>

        </ul>
      </div>
      </nav>

     </div> 

     </div>   
  </div>  
</header>


<!-------------------------------PRODUCT SHOWCASE------------------------------------->

<div class="product_showcase">
  <div class="container2">
    
    <div class="row">
      <div class="col-md-6">
        <img src="background/display_img.png" alt="product img" id="display_img">
      </div>

      <div class="col-md-6" id="product_details" style="padding-left: 40px;padding-right: 40px;">      
        <div class="row">
          <div class="col-md-6">
            <h1>Zig Zag</h1>
            <p class="code">CODE : KH114</p>
            <h4 class="price">&#8377;1599</h4>
            <p class="offer">15% off</p>
          </div>
          <div class="col-md-6">
            <div class="btn-group product_display_toggle">
              <button class="prev_product">PREV</button>
              <button class="next_product">NEXT</button>
            </div>
          </div>
        </div>
         <hr/>

         <div class="row">
           <div class="col-md-6">
             <p class="select_material_p">SELECT MATERIAL</p>
             <div class="btn-group select_material_radio">
               <input class="brown_radio_btn" type="radio" name="">
               <input class="back_radio_btn" type="radio" name="">
               <input class="grey_radio_btn" type="radio" name="">
             </div>

             <p class="select_finish_p">SELECT FINISH</p>
             <div class="btn-group select_material_radio">
               <input class="brown_radio_btn" type="radio" name="">
               <input class="back_radio_btn" type="radio" name="">
               <input class="grey_radio_btn" type="radio" name="">
               <input class="grey_radio_btn" type="radio" name="">
               <input class="grey_radio_btn" type="radio" name="">
             </div>
           </div>
           <div class="col-md-6">
             
             <p class="select_size_p">SELECT SIZE</p>
             <div class="btn-group select_size_btn">
               <button>8 inch</button>
               <button>10 inch</button>
               <button>12 inch</button>
             </div>
             <div class="row">
              <div class="col-md-12">
               <div class="btn-group bottom_size_buttons">
                  <button>16 inch</button>
               <button>24 inch</button>
               </div>
               </div>
             </div>
           </div>
         </div>

         <div class="row">
           <div class="col-md-12 delivery_option">
             <p class="delivery_p">DELIVERY OPTION</p>
             <div class="btn-group">
               <input type="number" name="" placeholder="PIN CODE">
               <button>check</button>
             </div>
             <p>Please enter your pincode to check delivery options</p>
             <hr/>
             <button type="button" class="final_add_cart" style="width: 320px;height: 68px;background-color: #fbd100;
  border:none;
  border-radius: 6px;">ADD TO CART</button>
           </div>
         </div>


      </div>
    </div>

   

  </div> 
</div>


 <div class="bottom_div">
  <div class="container2">
      <div class="row">
        <div class="col-md-6">
         <ul>
           <li><h6>PRODUCT DESCRIPTION</h6></li>
           <li><h6>REVIEWS</h6></li>
           <li><h6>FAQ's</h6></li>
         </ul>
         <p>Aenean hendrerit sem tellus, eget egestas erat viverra vitae. 
Mauris quis aliquet felis, eu lobortis ex. Phasellus volutpat, lorem eget 
consectetur luctus, odio magna ultrices risus, eu laoreet ligula 
diam vitae erat. Nunc id tellus ut eros scelerisque suscipit ut ac 
nunc. Pellentesque eget blandit nulla.</p>
        </div>
        <div class="col-md-6">
          <img src="background/popular1.png" class="img-fluid" id="add_banner">
        </div>
      </div>  
      </div>   
    </div>














<!------------------------------SIMILAR PRODUCTS------------------------------------------>


<div class="similar_products">
  
  <div class="container2">
   
   <div class="row">
    <div class="col-md-5">
      <h1>Similar Products</h1>
    </div>
    <div class="col-md-7"></div>
   </div> 

   <div class="similar_products_display">
     <div class="row">
       <div class="col-md-3 text-center">
            <div class="container3">                     
                     <img src="background/arrival1.png" alt="product_img" class="product_img">
                     <h5 class="arrival_product_text1"><strong>Deep Rope</strong></h5>
                     <h5 class="arrival_product_text2">&#8377;1599/-</h5>
                     <hr/>
                    <div class="btn-group color_pallets">
                        <button class="btn_yellow"></button>
                        <button class="btn_black"></button>
                        <button class="btn_grey"></button>
                    </div>

                       </div>  
                       <button class="add_to_cart" type="button">ADD TO CART+</button>
       </div>
       <div class="col-md-3 text-center">
            <div class="container3">                     
                     <img src="background/arrival2.png" alt="product_img" class="product_img">
                     <h5 class="arrival_product_text1"><strong>Deep Rope</strong></h5>
                     <h5 class="arrival_product_text2">&#8377;1599/-</h5>
                     <hr/>
                    <div class="btn-group color_pallets">
                        <button class="btn_yellow"></button>
                        <button class="btn_black"></button>
                        <button class="btn_grey"></button>
                    </div>

                       </div>  
                       <button class="add_to_cart" type="button">ADD TO CART+</button>
       </div>
       <div class="col-md-3 text-center">
            <div class="container3">                     
                     <img src="background/arrival3.png" alt="product_img" class="product_img">
                     <h5 class="arrival_product_text1"><strong>Deep Rope</strong></h5>
                     <h5 class="arrival_product_text2">&#8377;1599/-</h5>
                     <hr/>
                    <div class="btn-group color_pallets">
                        <button class="btn_yellow"></button>
                        <button class="btn_black"></button>
                        <button class="btn_grey"></button>
                    </div>

                       </div>  
                       <button class="add_to_cart" type="button">ADD TO CART+</button>
       </div>
       <div class="col-md-3 text-center">
            <div class="container3">                     
                     <img src="background/arrival4.png" alt="product_img" class="product_img">
                     <h5 class="arrival_product_text1"><strong>Deep Rope</strong></h5>
                     <h5 class="arrival_product_text2">&#8377;1599/-</h5>
                     <hr/>
                    <div class="btn-group color_pallets">
                        <button class="btn_yellow"></button>
                        <button class="btn_black"></button>
                        <button class="btn_grey"></button>
                    </div>

                       </div>  
                       <button class="add_to_cart" type="button">ADD TO CART+</button>
       </div>
     </div>

       <div class="row2">
          <div class="row">
       <div class="col-md-3 text-center">
            <div class="container3">                     
                     <img src="background/arrival1.png" alt="product_img" class="product_img">
                     <h5 class="arrival_product_text1"><strong>Deep Rope</strong></h5>
                     <h5 class="arrival_product_text2">&#8377;1599/-</h5>
                     <hr/>
                    <div class="btn-group color_pallets">
                        <button class="btn_yellow"></button>
                        <button class="btn_black"></button>
                        <button class="btn_grey"></button>
                    </div>

                       </div>  
                       <button class="add_to_cart" type="button">ADD TO CART+</button>
       </div>
       <div class="col-md-3 text-center">
            <div class="container3">                     
                     <img src="background/arrival2.png" alt="product_img" class="product_img">
                     <h5 class="arrival_product_text1"><strong>Deep Rope</strong></h5>
                     <h5 class="arrival_product_text2">&#8377;1599/-</h5>
                     <hr/>
                    <div class="btn-group color_pallets">
                        <button class="btn_yellow"></button>
                        <button class="btn_black"></button>
                        <button class="btn_grey"></button>
                    </div>

                       </div>  
                       <button class="add_to_cart" type="button">ADD TO CART+</button>
       </div>
       <div class="col-md-3 text-center">
            <div class="container3">                     
                     <img src="background/arrival3.png" alt="product_img" class="product_img">
                     <h5 class="arrival_product_text1"><strong>Deep Rope</strong></h5>
                     <h5 class="arrival_product_text2">&#8377;1599/-</h5>
                     <hr/>
                    <div class="btn-group color_pallets">
                        <button class="btn_yellow"></button>
                        <button class="btn_black"></button>
                        <button class="btn_grey"></button>
                    </div>

                       </div>  
                       <button class="add_to_cart" type="button">ADD TO CART+</button>
       </div>
       <div class="col-md-3 text-center">
            <div class="container3">                     
                     <img src="background/arrival4.png" alt="product_img" class="product_img">
                     <h5 class="arrival_product_text1"><strong>Deep Rope</strong></h5>
                     <h5 class="arrival_product_text2">&#8377;1599/-</h5>
                     <hr/>
                    <div class="btn-group color_pallets">
                        <button class="btn_yellow"></button>
                        <button class="btn_black"></button>
                        <button class="btn_grey"></button>
                    </div>

                       </div>  
                       <button class="add_to_cart" type="button">ADD TO CART+</button>
       </div>
     </div>
       </div>

   </div>

  </div>

</div>

























<!---------------------------NEWSLETTER---------------------->


<div id="newsletter">
  <div class="container2">
    

     <div class="row">
      
      <div class="col-md-6" id="newsletter_main">
      
      <h1>Newsletter</h1> 
      <div class="row">
        <div class="col-md-8">
          <p>Subscribe to the Handle Store list to receive updates 
on new arrivals, special offers and 
other discount information.</p>

 <input type="" name="" placeholder="Enter your Email Address">
        </div>
        <div class="col-md-4"></div>
      </div>
      

        


      </div>
      <div class="col-md-6" id="newsletter_image">
        
        <img src="background/newsletter_img.png" alt="newsletter">
      </div>

     </div>

  </div>
</div>





<!---------------------------REASONS TO SHOP--------------------------------->


<div id="reasons_to_shop" style="">
  <div class="container2" >

    <div class="row">
      <div class="col-md-12"> 
        <h1>Reasons to shop at</h1> 
        <h1 id="the_handle_store">The Handle Store</h1> 
        <hr/>
      </div>
    </div>


    <div id="three_reasons">
      <div class="row">

        <div class="col-md-4" id="great_offers">            
          <div class="row">
            <div class="col-md-2">
             <img src="icons/reason_1.png" alt="reason1">                 
            </div>
            <div class="col-md-10">
              <h4>Great Offers</h4>
              <p>Subscribe to the Handle Store
on new arrivals, special offers and 
other discount information.</p>
            </div>
          </div>
        </div>

        <div class="col-md-4" id="free_shipping">
          <div class="row">
            <div class="col-md-2">  
            <img src="icons/reason_2.png" alt="reason2">              
            </div>
            <div class="col-md-10">
              <h4>Free Shipping</h4>
              <p>Subscribe to the Handle Store
on new arrivals, special offers and 
other discount information.</p>
            </div>
          </div>
        </div>

        <div class="col-md-4" id="safest_online_store">
          <div class="row">
            <div class="col-md-2"> 
            <img src="icons/reason_3.png" alt="reason3">                
            </div>
            <div class="col-md-10">
              <h4>Safest Online Store</h4>
              <p>Subscribe to the Handle Store
on new arrivals, special offers and 
other discount information.</p>
            </div>
          </div>
        </div>

      </div>
    </div>

  </div>  
</div>



<!---------------------------FOOTER AREA--------------------------------->

<footer id="footer_area" style="background-color: #f5f6f8;">

  <div class="container">
    
    
    <div class="row">
    
    <div class="col-md-4" id="left_part_footer" >

      <a href="index.php"><img src="logos/handle_logo.png" alt="The hangle store" id="store_logo"></a>
      <p>Aliquam sodales accumsan justo, at fringilla 
elit pulvinar cursus.Aliquam sodales accumsan justo, 
at fringilla elit pulvinar cursus.</p>

    </div>  
    <div class="col-md-2">
      <ul>
      <a href=""><li>MAIN DOOR</li></a>
      <a href=""><li>KITCHEN</li></a>   
      <a href=""><li>CABINET</li></a> 
      <a href=""><li>KNOBS</li></a> 
      <a href=""><li>PROFILES</li></a>  
      <a href=""><li>MORTICE</li></a> 

      </ul>
    </div>
    <div class="col-md-2">
      <ul>
      <a href=""><li>ABOUT US</li></a>
      <a href=""><li>CONTACT US</li></a>    
      <a href=""><li>PRIVACY POLICY</li></a>  
      <a href=""><li>FAQ'S</li></a> 
      <a href=""><li>WARRANTY</li></a>  
      <a href=""><li>T&C</li></a> 
      <a href=""><li>RETURN POLICY</li></a> 

      </ul>
    </div>
    <div class="col-md-4"  id="social_media">
      
      <h4>Social Media</h4>

      <div class="btn-group" id="button_group_social">
        <button><i class="fab fa-facebook-f"></i></button>
        <button><i class="fab fa-twitter"></i></button>
        <button><i class="fab fa-instagram"></i></button>
        <button><i class="fab fa-youtube"></i></button>
      </div>


    </div>

    </div>


  </div>
  

</footer>

































<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>

</body>
</html>