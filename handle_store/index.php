<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">

  <title>The Handle Store | Main</title>

  <meta name="description" content="Best handle selling website">
  <meta name="author" content="Handle Store">

  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="style.css" rel="stylesheet">




<link rel="stylesheet" type="text/css" href="carousel.css" rel="stylesheet" type="text/css" >




<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js" type="text/javascript" ></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" type="text/javascript"></script>
<script src="slick.js" type="text/javascript" charset="utf-8"></script>

  <script src="https://kit.fontawesome.com/f000cd9cea.js" crossorigin="anonymous"></script>

<!----created date:15 sep 2020-------->
</head>
<body>

<!---------------------------HEADER AREA--------------------------------->

<header id="header_area">
  <div class="container">
     <div class="row">


     <div class="col-md-5">
            <a href="index.php" class="navbar-brand"><img src="logos/handle_logo.png" alt="The hangle store" id="store_logo"></a>
       

          <!-- <button>search</button> -->
           <input type="" name="" placeholder="Search for handles,knobs and more;">
      
      
     </div> 
     <div id="main_menu" class="col-md-7">
      
      <nav id="menu" class="navbar navbar-expand-sm ">
        <div class="row">
          <div class="col-md-12 text-right">
            
          </div>
        </div>
         <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar" >
        <i class="fas fa-bars"></i>
  </button>

        <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul id="nav" class="navbar-nav">
          <li class="nav-item"><a href="" class="nav-link">MAIN DOOR</a></li>
          <li class="nav-item"><a href="" class="nav-link">KITCHEN</a></li>
          <li class="nav-item"><a href="" class="nav-link">CABINET</a></li>
          <li class="nav-item"><a href="" class="nav-link">KNOBS</a></li>
          <li class="nav-item"><a href="" class="nav-link">PROFILES</a></li>
          <li class="nav-item"><a href="" class="nav-link">MORTICE</a></li>
          <li class="nav-item"><button id="main_cart_btn"><i class="fas fa-shopping-cart"></i> Cart</button></li>

        </ul>
      </div>
      </nav>

     </div> 

     </div>   
  </div>  
</header>




<!----------------------------HEADER FIX------------------------------------------>


<!---------------------------MAIN BANNER IMAGE ------------------------------------->



<section>
  
<div class="main_banner">
 <div class="container2">

  <div id="carousel" class="carousel slide hero-slides" data-ride="carousel">
  <ol class="carousel-indicators">
    <li class="active" data-target="#carousel" data-slide-to="0"></li>
    <li data-target="#carousel" data-slide-to="1"></li>
    <li data-target="#carousel" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner" role="listbox">
    <div class="carousel-item active boat">
      <div class="container h-100 d-none d-md-block">
        <div class="row align-items-center h-100">
          <div class="col-12 col-md-9 col-lg-7 col-xl-6">
            <div class="caption animated fadeIn">
              <h2 class="animated fadeInLeft">Best Main</h2>
              <h2 class="animated fadeInLeft">Door Handles</h2>
              <p class="animated fadeInRight">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tristique nisl vitae luctus sollicitudin. Fusce consectetur sem eget dui tristique, ac posuere arcu varius.</p>
            
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="carousel-item sea">
      <div class="container h-100 d-none d-md-block">
        <div class="row align-items-center h-100">
          <div class="col-12 col-md-9 col-lg-7 col-xl-6">
            <div class="caption animated fadeIn">
              <h2 class="animated fadeInLeft">Discover the canyon by the sea</h2>
              <p class="animated fadeInRight">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tristique nisl vitae luctus sollicitudin. Fusce consectetur sem eget dui tristique, ac posuere arcu varius.</p>
           
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="carousel-item river">
      <div class="container h-100 d-none d-md-block">
        <div class="row align-items-center h-100">
          <div class="col-12 col-md-9 col-lg-7 col-xl-6">
            <div class="caption animated fadeIn">
              <h2 class="animated fadeInLeft">Explore the river valley</h2>
              <p class="animated fadeInRight">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tristique nisl vitae luctus sollicitudin. Fusce consectetur sem eget dui tristique, ac posuere arcu varius.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
   
 </div> 
</div>


<div class="main_div">
  
<div class="sliding_gallery">
 <div class="container2">
   
 <div class="framebox">
       <div class="owl-carousel">
      <a class="item item1" >
       <img src="background/image1.png" alt="">
      </a>
      <a class="item item2" >
         <img src="background/image2.png" alt="">
      </a>
      <a class="item item3" >
         <img src="background/image3.png" alt="">
      </a>
      <a class="item item4" >
         <img src="background/image4.png" alt="">
      </a>
      <a class="item item5" >
         <img src="background/image2.png" alt="">
      </a>
      <a class="item item6" >
         <img src="background/image1.png" alt="">
      </a>
  </div>
  </div>

 </div> 
</div>

<div class="offer_main">
 <div class="container2"> 
  <div class="row">
      <div class="col-md-4" style="padding-top: 20px;">
                   <div class="popular_container">
                   <img src="background/popular1.png" alt="product_img" class="popular_img"> 
                   <h3 class="popular_offer_name1"><strong>Upto 25% off</strong></h3>
                   <h3 class="popular_offer_name2"><strong>on orders above</strong></h3>
                   <h3 class="popular_offer_price">2499/-</h3>
                   </div>                  
               </div>

               <div class="col-md-4" style="padding-top: 20px;">
                   <div class="popular_container">
                    <img src="background/popular2.png" alt="product_img" class="popular_img">
                    <h3 class="popular_offer_name3"><strong>Buy 1 get 1</strong></h3>
                    <h3 class="popular_offer_name4"><strong>Free</strong></h3>
                    <p class="popular_offer_p">Aliquam sodales
elit pulvinar cursus.</p>                  
                   </div>
               </div>

               <div class="col-md-4">
                   <div class="popular_container2" style="padding-top: 20px;">
                    <img src="background/popular3.png" alt="product_img" class="popular_img">
                    <h3 class="popular_product_name5"><strong>Free Standard Delivery</strong></h3>
                    <h3 class="popular_product_name6"><strong>on orders over</strong></h3>
                   <h3 class="popular_offer_price">5000/-</h3>               
                   </div>
               </div>

  </div>
</div>
</div>

</div>






</section>





<!--------------------------------OUR CLIENTS--------------------------------------------->



<div class="our_clients">
  <div class="container2">
    <div class="row">
      <div class="col-md-2" id="our_clients_div1">
        <h3>Our Clients</h3>
      </div>
      <div class="col-md-10" id="our_clients_div2">
        <div class="row">
    <div class="container">
      <section class="customer-logos slider">
        <div class="slide"><img src="images/image1.png"></div>
        <div class="slide"><img src="images/image2.png"></div>
        <div class="slide"><img src="images/image3.png"></div>
        <div class="slide"><img src="images/image4.png"></div>
        <div class="slide"><img src="images/image5.png"></div>
        <div class="slide"><img src="images/image6.png"></div>
        <div class="slide"><img src="images/image7.png"></div>
        <div class="slide"><img src="images/image8.png"></div>
      </section>
    </div>
  </div>
      </div>
    </div>
  </div>
</div>



<!-----------------------OWL-------------------------------------------------------->


<div class="container2">
  
    
</div>







<!------------------------------FEATURED PRODUCTS------------------------------------>



<div id="new_arrivals" style="">
    <div class="container2">

        <div class="row">
            <div class="col-md-5">
                <ul>
                    <li>
                       <img src="icons/featured.png" alt="new_arrivals">   
                   </li>
                   <li><h1>Featured Products</h1></li>
               </ul>
           </div>
           <div class="col-md-7"></div>
       </div>

       <div id="new_arrivals_nav" > 
           <div class="row">

               <div class="col-md-10" >         
                <ul>
                    <li><a href="">MAIN DOOR</li>
                    <li><a href="">KITCHEN</li>
                    <li><a href="">CABINET</li>
                    <li><a href="">KNOBS</li>
                    <li><a href="">PROFILES</li>
                    <li><a href="">MORTICE</li>
                
                </ul>
                <hr/>
            </div>

            <div class="col-md-2" >
               <div class="btn-group float-right">
                   <button class="slide_buttons"><a href="">&#8592;</a></button>
                   <button class="slide_buttons"><a href="">&#8594;</a></button>
               </div>
           </div>

       </div>   
   </div>



         <div id="new_arrival_items">
             <div class="row">
             
             <div class="col-md-3 text-center">                     
                        <div class="container3">                     
                     <img src="background/arrival1.png" alt="product_img" class="product_img">
                     <h5 class="arrival_product_text1"><strong>Deep Rope</strong></h5>
                     <h5 class="arrival_product_text2">&#8377;1599/-</h5>
                     <hr/>
                    <div class="btn-group color_pallets">
                        <button class="btn_yellow"></button>
                        <button class="btn_black"></button>
                        <button class="btn_grey"></button>;
                    </div>

                       </div>  
                       <button class="add_to_cart" type="button">ADD TO CART+</button>                                           
             </div>    

             <div class="col-md-3 text-center">                
                <div class="container3">                     
                   <img src="background/arrival2.png" alt="product_img" class="product_img">
                   <h5 class="arrival_product_text1"><strong>Cute Lady</strong></h5>
                   <h5 class="arrival_product_text2">&#8377;1299/-</h5>
                   <hr/>
                   <div class="btn-group color_pallets">
                        <button class="btn_yellow"></button>
                        <button class="btn_black"></button>
                        <button class="btn_grey"></button>
                    </div>
               </div> 
               <button class="add_to_cart" type="button">ADD TO CART+</button>                                  
             </div>  

             <div class="col-md-3 text-center">                 
                        <div class="container3">                     
                     <img src="background/arrival3.png" alt="product_img" class="product_img">
                     <h5 class="arrival_product_text1"><strong>Zig Zag</strong></h5>
                     <h5 class="arrival_product_text2">&#8377;2599/-</h5>
                     <hr/>
                     <div class="btn-group color_pallets">
                        <button class="btn_yellow"></button>
                        <button class="btn_black"></button>
                        <button class="btn_grey"></button>
                    </div>
                       </div>  
                       <button class="add_to_cart" type="button">ADD TO CART+</button>                             
             </div>  

             <div class="col-md-3 text-center">                    
                        <div class="container3">                     
                     <img src="background/arrival4.png" alt="product_img" class="product_img">
                     <h5 class="arrival_product_text1"><strong>Corola</strong></h5>
                     <h5 class="arrival_product_text2">&#8377;5499/-</h5>
                     <hr/>
                     <div class="btn-group color_pallets">
                        <button class="btn_yellow"></button>
                        <button class="btn_black"></button>
                        <button class="btn_grey"></button>
                    </div>

                       </div>      
                       <button class="add_to_cart" type="button">ADD TO CART+</button>            
             </div>  

             </div>
         </div>



</div>
</div>    







<!------------------------------POPULAR PRODUCTS-------------------------------------->


<div id="popular_products">
    <div class="container2">
       

        <div class="row">
            <div class="col-md-5">
                <ul>
                    <li>
                       <img src="icons/popular.png" alt="new_arrivals">   
                   </li>
                   <li><h1>Popular Products</h1></li>
               </ul>
           </div>
           <div class="col-md-7"></div>
       </div>

       <div id="popular_items">
           <div class="row">

               <div class="col-md-4">
                   <div class="popular_container">
                   <img src="background/popular1.png" alt="product_img" class="popular_img"> 
                   <h3 class="popular_product_name"><strong>WOW</strong></h3>
                   <h3 class="popular_product_price">2499/-</h3>
                   <div class="btn-group color_pallets2">
                       <button class="btn_peach"></button>
                       <button class="btn_yellow"></button>
                       <button class="btn_brown"></button>
                       <button class="btn_black"></button>
                       <button class="btn_grey"></button>
                   </div>
                   <div class="row">
                   <button class="popular_add_to_cart">ADD TO CART</button>
                   </div>
                   </div>                  
               </div>

               <div class="col-md-4">
                   <div class="popular_container">
                    <img src="background/popular2.png" alt="product_img" class="popular_img">
                    <h3 class="popular_product_name"><strong>Pavone</strong></h3>
                   <h3 class="popular_product_price">2499/-</h3>
                   <div class="btn-group color_pallets2">
                       <button class="btn_peach"></button>
                       <button class="btn_yellow"></button>
                       <button class="btn_brown"></button>
                       <button class="btn_black"></button>
                       <button class="btn_grey"></button>
                   </div>
                   <div class="row">
                   <button class="popular_add_to_cart">ADD TO CART</button>
                   </div>   
                   </div>
               </div>

               <div class="col-md-4">
                   <div class="popular_container">
                    <img src="background/popular3.png" alt="product_img" class="popular_img">
                    <h3 class="popular_product_name"><strong>Pomegranate</strong></h3>
                   <h3 class="popular_product_price">499/-</h3>
                   <div class="btn-group color_pallets2">
                       <button class="btn_peach"></button>
                       <button class="btn_yellow"></button>
                       <button class="btn_brown"></button>
                       <button class="btn_black"></button>
                       <button class="btn_grey"></button>
                   </div>
                   <div class="row">
                   <button class="popular_add_to_cart">ADD TO CART</button>
                   </div>   
                   </div>
               </div>

           </div>
       </div>


    </div>  
</div>



<!-------------------------------CLEARANCE SALE----------------------------------------->


<div id="clearance_sale" style="">

   <div class="container2">

      <div class="row">
         <div class="col-md-7" id="sale_one">

            <div id="sale_one_div">
                <h1>Clearance</h1>
                <h1 class="sale">Sale</h1>
                <p>Aliquam sodales accumsan justo, at fringilla </p>
                  <p style="margin-top: -14px;">  elit pulvinar cursus.Aliquam </p>
                  <p style="margin-top: -14px;">  sodales accumsan justo, at fringilla </p>
               <p style="margin-top: -14px;"> elit pulvinar cursus.</p>

                <button>EXPLORE NOW</button>
            </div>

        </div>
        <div class="col-md-5">

            <div id="sale_two">
                <div id="sale_two_div">
                    <h1>Something</h1>
                    <h1 class="sale">Else</h1>
                    <p>Aliquam sodales accumsan justo, at fringilla 
                        elit pulvinar cursus.Aliquam 
                        sodales accumsan justo, at fringilla 
                    elit pulvinar cursus.</p>
                </div>
            </div>


            <div id="sale_three_gap">
               <div id="sale_three">
                <div id="sale_three_div">
                    <h1>Something</h1>

                </div>
            </div>
        </div>



    </div>
</div>
</div>
</div>




<!--------------------------NEW ARRIVALS----------------------------------->



<div id="new_arrivals" style="">
    <div class="container2">

        <div class="row">
            <div class="col-md-5">
                <ul>
                    <li>
                       <img src="icons/new_arrivals.png" alt="new_arrivals">   
                   </li>
                   <li><h1>New Arrivals</h1></li>
               </ul>
           </div>
           <div class="col-md-7"></div>
       </div>

       <div id="new_arrivals_nav" > 
           <div class="row">

               <div class="col-md-10" >         
                <ul>
                    <li><a href="">MAIN DOOR</li>
                    <li><a href="">KITCHEN</li>
                    <li><a href="">CABINET</li>
                    <li><a href="">KNOBS</li>
                    <li><a href="">PROFILES</li>
                    <li><a href="">MORTICE</li>
                
                </ul>
                <hr/>
            </div>

            <div class="col-md-2" >
               <div class="btn-group float-right">
                   <button class="slide_buttons"><a href="">&#8592;</a></button>
                   <button class="slide_buttons"><a href="">&#8594;</a></button>
               </div>
           </div>

       </div>   
   </div>



         <div id="new_arrival_items">
             <div class="row">
             
             <div class="col-md-3 text-center">                     
                        <div class="container3">                     
                     <img src="background/arrival1.png" alt="product_img" class="product_img">
                     <h5 class="arrival_product_text1"><strong>Deep Rope</strong></h5>
                     <h5 class="arrival_product_text2">&#8377;1599/-</h5>
                     <hr/>
                    <div class="btn-group color_pallets">
                        <button class="btn_yellow"></button>
                        <button class="btn_black"></button>
                        <button class="btn_grey"></button>
                    </div>

                       </div>  
                       <button class="add_to_cart" type="button">ADD TO CART+</button>                                           
             </div>    

             <div class="col-md-3 text-center">                
                <div class="container3">                     
                   <img src="background/arrival2.png" alt="product_img" class="product_img">
                   <h5 class="arrival_product_text1"><strong>Cute Lady</strong></h5>
                   <h5 class="arrival_product_text2">&#8377;1299/-</h5>
                   <hr/>
                   <div class="btn-group color_pallets">
                        <button class="btn_yellow"></button>
                        <button class="btn_black"></button>
                        <button class="btn_grey"></button>
                    </div>
               </div> 
               <button class="add_to_cart" type="button">ADD TO CART+</button>                                  
             </div>  

             <div class="col-md-3 text-center">                 
                        <div class="container3">                     
                     <img src="background/arrival3.png" alt="product_img" class="product_img">
                     <h5 class="arrival_product_text1"><strong>Zig Zag</strong></h5>
                     <h5 class="arrival_product_text2">&#8377;2599/-</h5>
                     <hr/>
                     <div class="btn-group color_pallets">
                        <button class="btn_yellow"></button>
                        <button class="btn_black"></button>
                        <button class="btn_grey"></button>
                    </div>
                       </div>  
                       <button class="add_to_cart" type="button">ADD TO CART+</button>                             
             </div>  

             <div class="col-md-3 text-center">                    
                        <div class="container3">                     
                     <img src="background/arrival4.png" alt="product_img" class="product_img">
                     <h5 class="arrival_product_text1"><strong>Corola</strong></h5>
                     <h5 class="arrival_product_text2">&#8377;5499/-</h5>
                     <hr/>
                     <div class="btn-group color_pallets">
                        <button class="btn_yellow"></button>
                        <button class="btn_black"></button>
                        <button class="btn_grey"></button>
                    </div>

                       </div>      
                       <button class="add_to_cart" type="button">ADD TO CART+</button>            
             </div>  

             </div>
         </div>



</div>
</div>    








<!---------------------------NEWSLETTER---------------------->


<div id="newsletter">
	<div class="container2">
		

     <div class="row">
     	
     	<div class="col-md-6" id="newsletter_main">
     	
     	<h1>Newsletter</h1>	
     	<div class="row">
     		<div class="col-md-8">
     			<p>Subscribe to the Handle Store list to receive updates 
on new arrivals, special offers and 
other discount information.</p>

<div class="row">
  <div class="col-md-12" id="commentDiv">
 <input type="" name="" placeholder="Enter your Email Address" class="form-control commentfix">
 <button><i class="fas fa-paper-plane"></i></button>
 </div>
 </div>

     		</div>
     		<div class="col-md-4"></div>
     	</div>
     	

        


     	</div>
     	<div class="col-md-6" id="newsletter_image">
     		
     		<img src="background/newsletter_img.png" alt="newsletter">
     	</div>

     </div>

	</div>
</div>





<!---------------------------REASONS TO SHOP--------------------------------->


<div id="reasons_to_shop" style="">
	<div class="container2" >

		<div class="row">
			<div class="col-md-12">	
				<h1>Reasons to shop at</h1>	
				<h1 id="the_handle_store">The Handle Store</h1>	
				<hr/>
			</div>
		</div>


		<div id="three_reasons">
			<div class="row">

				<div class="col-md-4" id="great_offers">            
					<div class="row">
						<div class="col-md-2">
						 <img src="icons/reason_1.png" alt="reason1">             		
						</div>
						<div class="col-md-10">
							<h4>Great Offers</h4>
							<p>Subscribe to the Handle Store
on new arrivals, special offers and 
other discount information.</p>
						</div>
					</div>
				</div>

				<div class="col-md-4" id="free_shipping">
					<div class="row">
						<div class="col-md-2">  
						<img src="icons/reason_2.png" alt="reason2">           		
						</div>
						<div class="col-md-10">
							<h4>Free Shipping</h4>
							<p>Subscribe to the Handle Store
on new arrivals, special offers and 
other discount information.</p>
						</div>
					</div>
				</div>

				<div class="col-md-4" id="safest_online_store">
					<div class="row">
						<div class="col-md-2"> 
						<img src="icons/reason_3.png" alt="reason3">             		
						</div>
						<div class="col-md-10">
							<h4>Safest Online Store</h4>
							<p>Subscribe to the Handle Store
on new arrivals, special offers and 
other discount information.</p>
						</div>
					</div>
				</div>

			</div>
		</div>

	</div>	
</div>










<!---------------------------FOOTER AREA--------------------------------->

<footer id="footer_area" style="background-color: #f5f6f8;">

	<div class="container">
		
    
    <div class="row">
    
    <div class="col-md-4" id="left_part_footer" >

    	       <a href="index.php"><img src="logos/handle_logo.png" alt="The hangle store" id="store_logo"></a>
    	<p>Aliquam sodales accumsan justo, at fringilla 
elit pulvinar cursus.Aliquam sodales accumsan justo, 
at fringilla elit pulvinar cursus.</p>

    </div>	
    <div class="col-md-2">
    	<ul>
    	<a href=""><li><p>MAIN DOOR</p></li></a>
    	<a href=""><li><p>KITCHEN</p></li></a>		
    	<a href=""><li><p>CABINET</p></li></a>	
    	<a href=""><li><p>KNOBS</p></li></a>	
    	<a href=""><li><p>PROFILES</p></li></a>	
    	<a href=""><li><p>MORTICE</p></li></a>	

    	</ul>
    </div>
    <div class="col-md-2">
    	<ul>
    	<a href=""><li>ABOUT US</li></a>
    	<a href=""><li>CONTACT US</li></a>		
    	<a href=""><li>PRIVACY POLICY</li></a>	
    	<a href=""><li>FAQ'S</li></a>	
    	<a href=""><li>WARRANTY</li></a>	
    	<a href=""><li>T&C</li></a>	
    	<a href=""><li>RETURN POLICY</li></a>	

    	</ul>
    </div>
    <div class="col-md-4"  id="social_media">
    	
    	<h4>Social Media</h4>

    	<div class="btn-group" id="button_group_social">
    		<button><i class="fab fa-facebook-f"></i></button>
    		<button><i class="fab fa-twitter"></i></button>
    		<button><i class="fab fa-instagram"></i></button>
    		<button><i class="fab fa-youtube"></i></button>
    	</div>


    </div>

    </div>


	</div>
</footer>






<script type="text/javascript">
    $(document).ready(function(){
      $('.customer-logos').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 1000,
        arrows: false,
        dots: false,
          pauseOnHover: false,
          responsive: [{
          breakpoint: 768,
          settings: {
            slidesToShow: 4
          }
        }, {
          breakpoint: 520,
          settings: {
            slidesToShow: 3
          }
        }]
      });
    });
  </script>



 <script type="text/javascript">
   jQuery(document).ready(function($) {
    var $owl = $('.owl-carousel');
      $owl.children().each( function( index ) {
        jQuery(this).attr( 'data-position', index ); 
      });
      
      $owl.owlCarousel({
        center: true,
        nav:true,
        loop: true,
        items: 5,
      margin:10,
        navText: ["<i class='fa arrow-circle-left'><</i>","<i class='fa arrow-right'>></i>"],
        responsive:{
          0:{
              items:1
          },
          600:{
              items:3
          },
          1000:{
              items:5
          }
       }
      });
    $(document).on('click', '.item', function() {
      $owl.trigger('to.owl.carousel', $(this).data( 'position' ) ); 
    });
          });
 </script>





 <link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/assets/owl.carousel.min.css" rel="stylesheet" type="text/css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<!-- 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script> -->



</body>
</html>