<?php 
session_start(); 
include_once("template-parts/header_links.php");
include_once("template-parts/navbar_m.php");
include_once("template-parts/footer.php");
include_once("includes/main_include.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">

  <title>The Handle Store | Yout Cart</title>

  <meta name="description" content="Best handle selling website">
  <meta name="author" content="Handle Store">
  <script src="js/sweetalert.min.js"></script>
  <link href="css/sweetalert.css" rel="stylesheet">
  <?php header_links(); ?>
</head>
<body>
<?php bottom_menu(); ?>
<!---------------------------------CART PAGE------------------------------------>
<?php     $csid = $_COOKIE["thehandlestore_AZwUghJKdP"];
		  if(isset($_SESSION["thehandlestore_AZwUghJKdP"])){
	      $email = $_SESSION["thehandlestore_AZwUghJKdP"]; }
		  $total = 0;
		  $q = "SELECT `cart_id`, `pro_id`, `pro_qty`, `pro_price`, `pro_total_price`, `csid` FROM `user_cart` WHERE `csid` = '$csid'";
		  $b = mysqli_query($conn,$q);
		  $count = mysqli_num_rows($b);
		  if($count > 0){
         ?>

<div id="cart">
  
  <div class="container2">
    
    <div class="cart_title">     
      <h1>YOUR CART(<?php echo $count ?>)</h1>
    </div>

<?php    while($row = mysqli_fetch_array($b))
	      {
		   $pflag=1;
		$cart_idx = $row['cart_id'];
		$pro_idx = $row['pro_id'];
		   $pro_qty = $row['pro_qty'];
		   $pro_toal_price =  $row["pro_total_price"]; 
									$total_price = array($row["pro_total_price"]);
									$values = array_sum($total_price);
									$total+=$values;
		  

   $q1 = "SELECT * FROM `products_db` WHERE `pro_id` = '$pro_idx'";
		   $b1 = mysqli_query($conn,$q1);	
		   while($row = mysqli_fetch_array($b1))
	       {	$pro_id = $row['pro_id'];
				$pro_name = $row['pro_name'];
				$pro_image_1 = $row['pro_image_1'];
				$handle_finishes = $row['handle_finishes'];
				$handle_sizes = $row['handle_sizes'];
				$material = $row['material'];
				$pro_price = $row['pro_price']; 
					
				$q1 = "SELECT * FROM `handle_finishes` WHERE `fin_id` = '$handle_finishes'";
				$b1 = mysqli_query($conn,$q1);	
				while($row = mysqli_fetch_array($b1))
				{
				   $fin_name = $row['finish_name'];
				}
				$q2 = "SELECT `size_id`, `size_name` FROM `handle_sizes` WHERE `size_id` = '$handle_sizes'";
				$b2 = mysqli_query($conn,$q2);	
				while($row = mysqli_fetch_array($b2))
				{
				   $size_name = $row['size_name'];
				}
				$q2 = "SELECT * FROM `pro_material` WHERE `mat_id` ='$material'";
				$b2 = mysqli_query($conn,$q2);	
				while($row = mysqli_fetch_array($b2))
				{
				   $mat_name = $row['mat_name'];
				}
				
				
				?>
	  <form action="" method="post">
  <input type="hidden" name="cart_id" value="<?php echo $cart_idx ?>">
  <input type="hidden" name="pro_id" value="<?php echo $pro_idx ?>">
  <div class="cart_items">
    <div class="row">
      <div class="col-md-3">
        <img src="images/products_images/<?php echo $pro_image_1 ?>" alt="product_img" class="cart_img">
      </div>
      <div class="col-md-9">
       

        <div class="row">
          <div class="col-md-6" style="padding-top: 20px;">
            <h3><?php echo $pro_name ?></h3>
            <p>Material: <?php echo $mat_name ?></p>
            <p>Size: <?php echo $size_name ?></p>
			<p>Finish:<?php echo $fin_name ?></p>
          </div>
          <div class="col-md-6" style="padding-top: 20px;">
            <div class="btn-group" style="float: right;">
              <button type="submit" name="sub" class="btn" id="subtract"><strong>-</strong></button>
              <button class="btn" id="quantity"><?php echo $pro_qty; ?></button>
              <button type="submit" name="add" class="btn" id="add"><strong>+</strong></button>
            </div>            
          </div>
        </div>

        <div class="row" style="padding-top: 80px;">
          <div class="col-md-6">
            <div class="btn-group">
              <button type="submit" name="remove" class="btn" id="remove_item"><i class="fas fa-trash-alt"></i> REMOVE ITEM</button>
             <!-- <button class="btn" id="wishlist"><i class="fas fa-heart"></i> MOVE TO WISHLIST</button>-->
            </div>
          </div> 
          <div class="col-md-6 text-right" >
            <h3>&#8377;<?php echo $pro_toal_price ?></h3>
          </div>
        </div>
      </div>
    </div>  
  </div> <hr/></form>
		  <?php }} ?>
  
  <div class="checkout_proceed">
    
    <div class="row">
      <div class="col-md-3"></div>
      <div class="col-md-9">
        <h3>The total amount of your order</h3>

        <div class="row">
          <div class="col-md-6">
            <p>Amount</p>
          </div>
          <div class="col-md-6">
            <p>&#8377;<?php echo $total ?></p>
          </div>
        </div>

     <!--   <div class="row">
          <div class="col-md-6">
            <p>Shipping Charges</p>
          </div>
          <div class="col-md-6">
            <p>Amount</p>
          </div>
        </div>
        <hr/>
        <div class="row">
          <div class="col-md-6">
            The total amount includeing charges
          </div>-->
          <div class="col-md-6">
            <style>
			#checkout{
				background:lightgrey;
				height:40px;
				width:120px;
				color:#000;
			}
			</style>
            <div class="row">
             <a href="checkout.php" class="btn" id="checkout"><strong>CHECKOUT</strong></a>
            </div>
          </div>
        </div>



      </div>
    </div>

    
  </div>


</div>
</div>

		  <?php } else{ ?>
		   <div class="container2">
		  <?php echo "No Product in Cart"; }?>
		   </div>
		 


<?php
if(isset($_POST["add"])){
	
	$cart_id = $_POST["cart_id"];
	$pro_id = $_POST["pro_id"];
	$pro_final_price =0;
	$pro_qty_1 = 0;
	$csid = $_COOKIE["thehandlestore_AZwUghJKdP"];
 echo   $q = "SELECT `cart_id`, `pro_id`, `pro_qty`, `pro_price`, `pro_total_price`, `csid` FROM `user_cart` WHERE `cart_id` = '$cart_id' AND `pro_id` = '$pro_id' AND `csid` = '$csid'";
	$b = mysqli_query($conn,$q);
	while($row = mysqli_fetch_array($b))
	{
				   $pro_qty = $row['pro_qty'];
				   $pro_price = $row['pro_price'];
				   $pro_total_price = $row['pro_total_price'];
				   $pro_qty_1 = $pro_qty + 1;
	               $pro_final_price = $pro_total_price + $pro_price;
	}
	
	
    $update = "UPDATE `user_cart` SET `pro_qty` = '$pro_qty_1',`pro_total_price`='$pro_final_price' WHERE `cart_id` = '$cart_id' AND `pro_id` = '$pro_id' AND `csid`='$csid'";
	$update_w = mysqli_query($conn,$update);
	if($update_w){ ?>
										<script>
										swal({
										  title: "Product Has been Updated Successfully!",
										  type: "success",
										  confirmButtonText: "OK"
										},
										function(isConfirm){
										  if (isConfirm) {
											window.location.href = "cart.php";
										  }
										});
										</script><?php
	}
	
	
}else if(isset($_POST["sub"])){
	
	$cart_id = $_POST["cart_id"];
	$pro_id = $_POST["pro_id"];
	$pro_final_price =0;
	$pro_qty_1 = 0;
	$csid = $_COOKIE["thehandlestore_AZwUghJKdP"];
    $q = "SELECT `cart_id`, `pro_id`, `pro_qty`, `pro_price`, `pro_total_price`, `csid` FROM `user_cart` WHERE `cart_id` = '$cart_id' AND `pro_id` = '$pro_id' AND `csid` = '$csid'";
	$b = mysqli_query($conn,$q);
	while($row = mysqli_fetch_array($b))
	{
				   $pro_qty = $row['pro_qty'];
				   $pro_price = $row['pro_price'];
				   $pro_total_price = $row['pro_total_price'];
				   $pro_qty_1 = $pro_qty - 1;
	               $pro_final_price = $pro_total_price - $pro_price;
	}
	if($pro_qty_1 == 0){
		$del = "DELETE FROM `user_cart` WHERE `cart_id` = '$cart_id' AND `pro_id` = '$pro_id' AND `csid`='$csid'";
		$update_w = mysqli_query($conn,$del);
		?><script>
										swal({
										  title: "Product Has been Removed Successfully!",
										  type: "success",
										  confirmButtonText: "OK"
										},
										function(isConfirm){
										  if (isConfirm) {
											window.location.href = "cart.php";
										  }
										});
										</script>
										<?php
		
	}else{
	
    $update = "UPDATE `user_cart` SET `pro_qty` = '$pro_qty_1',`pro_total_price`='$pro_final_price' WHERE `cart_id` = '$cart_id' AND `pro_id` = '$pro_id' AND `csid`='$csid'";
	$update_w = mysqli_query($conn,$update);
	if($update_w){  ?><script>
										swal({
										  title: "Product Has been Updated Successfully!",
										  type: "success",
										  confirmButtonText: "OK"
										},
										function(isConfirm){
										  if (isConfirm) {
											window.location.href = "cart.php";
										  }
										});
										</script>
										<?php
		
	}
	
	
}}else if(isset($_POST["remove"])){
	
	$cart_id = $_POST["cart_id"];
	$pro_id = $_POST["pro_id"];
	$pro_final_price =0;
	$pro_qty_1 = 0;
	$csid = $_COOKIE["thehandlestore_AZwUghJKdP"];
	$del = "DELETE FROM `user_cart` WHERE `cart_id` = '$cart_id' AND `pro_id` = '$pro_id' AND `csid`='$csid'";
		$update_w = mysqli_query($conn,$del);
		?><script>
										swal({
										  title: "Product Has been Removed Successfully!",
										  type: "success",
										  confirmButtonText: "OK"
										},
										function(isConfirm){
										  if (isConfirm) {
											window.location.href = "cart.php";
										  }
										});
										</script>
										<?php
	
	
}
?><!---------------------------FOOTER AREA--------------------------------->
<?php site_footer(); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>