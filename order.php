<?php
session_start(); 
error_reporting(0);
include_once("template-parts/header_links.php");
include_once("template-parts/navbar_m.php");
include_once("template-parts/footer.php");
include_once("includes/main_include.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
  <title>The Handle Store | Order Confirmation</title>
  <meta name="description" content="Best handle selling website">
  <meta name="author" content="Handle Store">
  <link href="https://fonts.googleapis.com/css2?family=Ramaraja&display=swap" rel="stylesheet">
  <?php header_links(); ?>
</head>
<body>
<style>
button.filter-button.active:focus {
    background: white;
  color:black;
    border: 1px solid yellow;
}
</style>
<section> 
<header>
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <nav class="navbar navbar-expand-lg navbar-light">
  <a class="navbar-brand" href="index.php"><img src="logos/handle_logo_final.png" id="store_logo"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
  <!--      <form class="form-inline my-2 my-lg-0 ml-auto">
      <input class="form-control mr-sm-2" type="search" placeholder="Search for Handles,Knobs and More.." aria-label="Search">
    </form> -->
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="index.php">HOME<span class="sr-only">(current)</span></a>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          HANDLES
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Mortice Plates</a>
          <a class="dropdown-item" href="#">Mortice Rose</a>
          <a class="dropdown-item" href="#">Main Door Pull</a>
          <a class="dropdown-item" href="#">Cabinets</a>
          <a class="dropdown-item" href="#">Knobs</a>
          <a class="dropdown-item" href="#">Concealed</a>
          <a class="dropdown-item" href="#">Profile</a>
          <a class="dropdown-item" href="#">Glass door</a>
        </div>
      </li>

            <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          DOOR ACCESSORIES
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>

            <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          BATH ACCESSORIES
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
    </ul>

      <form class="form-inline my-2 my-lg-0 ml-auto" id="my_header_form">
      <input class="form-control mr-sm-2" type="search" placeholder="Search for Handles,Knobs and More.." aria-label="Search">
      <button class="cart_btn"><img src="icons/cart_icon.png" id="cart_icon"> cart</button>
      <div class="mydropdown">
      <button class="login_btn"><img src="icons/login_icon.png"> LOGIN / SIGNUP</button>
      <div class="dropdown-content">
      <a href="#">Orders</a>
      <a href="#">Whishlist</a>
      <a href="#">Contact Us</a> 
      <hr style="padding: 0;margin: 0;" />   
      <a href="#" style="color: red;">Logout</a>
  </div>
  </div>
    </form>
  </div>
</nav>
    </div>
  </div>
</div>

</header>





<section>
  <div class="container2" style="padding: 80px 60px 80px 60px;">

    <div class="row">
      <div class="col-md-12">
        <h2 style="color: #fbd100;font-size: 40px;">Hi USER,</h2>

     <h6>Your Handle Store <strong>order #C64627882</strong> has shipped and its on its way! If you chose an expedited service (like UPS), then a link is provided below to track your package.</h6>
     <h6>If you so not see your entire  order listed below not to worry. You will receive a separate notification when rest of your order is ready.</h6>


     <div class="row" style="margin-top: 40px;">
       <div class="col-md-6">
         <h2><strong>Order #C64627882</strong></h2>
       </div>
       <div class="col-md-6 text-right">
         <p>You placed this order on <strong>03/10/2020</strong></p>
       </div>
     </div>

     <hr style="margin: 0;padding: 0;" />

     <div class="row" style="margin-top: 20px;">
       <div class="col-md-4">
         <p>Est.Delivery Date </p>
         <p style="font-size: 18px;"><strong>07/10/2020 by 8pm</strong></p>
       </div>
       <div class="col-md-4">
         <p>Shipping destination</p>
         <p style="font-size: 18px;"><strong>H.no 12321312<br>Carriamoddi Curchore<br>Goa</strong></p>
       </div>
       <div class="col-md-4">
         <p>Shipping Type</p>
         <p style="font-size: 18px;"><strong>Standard Shipping</strong></p>
         <h3 style="color: green;">TRACK NOW</h3>
       </div>
     </div>

      <hr style="margin: 0;padding: 0;" />

      <div class="row" style="margin-top: 20px;">
        <div class="col-md-8 text-center">

          <p style="font-size: 20px;">PRODUCT</p>
          <div class="row">
             <div class="col-md-4" style="padding: 40px;background-color: grey;border-radius: 8px;">
            
            <img src="images/products_images/model-2008.png" class="img-fluid order_conf_img">

          </div>
          <div class="col-md-8 text-left" style="padding: 40px;">
            <h3 style="margin-top: 50px;">MODEL 3008</h3>
            <p>ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
          </div>
          </div>
         

        </div>
        <div class="col-md-2 text-center">
          <p style="font-size: 20px;">QUANTITY</p>

          <div class="row">
            <div class="col-md-12"  style="padding: 100px;">
              <h3>1</h3>
            </div>
          </div>
        </div>
        <div class="col-md-2 text-center">
          <p style="font-size: 20px;">PRICE</p>

          <div class="row">
            <div class="col-md-12"  style="padding-top:100px;padding-bottom: 100px;">
               <h3> &#8377; 1500</h3>
            </div>
          </div>
        </div>
      </div>

      <hr style="margin: 0;padding: 0;" />

      <div class="row" style="margin-top: 20px;">
        <div class="col-md-8" style="padding: 10px;background-color: #fbd100;border-radius: 8px;">
          <div class="row">
            <div class="col-md-12 text-center" style="padding: 30px;border:2px solid #ffffff;"> 
                 
                 <h2 >Be a part of the family!</h2>
                 <p >JOIN OUR FACEBOOK PAGE</p>

            </div>
          </div>
        </div>
        <div class="col-md-2 text-center" style="margin-top: 20px;">
          <p style="line-height: 10px;">Subtotal:</p>
          <p style="line-height: 10px;">Sales Tax:</p>
          <p style="line-height: 10px;">Shipping:</p>
           <h3 style="line-height: 10px;margin-top: 40px"><strong>TOTAL:</strong></h3>
        </div>
        <div class="col-md-2 text-center" style="margin-top: 20px;">
          <p style="line-height: 10px;">&#8377; 1500</p>
          <p style="line-height: 10px;">&#8377; 200</p>
          <p style="line-height: 10px;">&#8377; 0</p>
          <h3 style="line-height: 10px;margin-top: 40px;"><strong>&#8377; 1700</strong></h3>
        </div>
      </div>


      <div class="row" style="margin-top: 40px;">
        <div class="col-md-12">
          <h6>You can always check the status of your order by clicking the <a href=""><u>My Account</u></a> linklocated at thetop of every page. After signing in , your most recent order status will appear on the order history page</h6>
        </div>
      </div>

      <h3 style="text-align: center;margin-top: 40px;" >Thank you for shopping with THE HANDLE STORE</h3>

        <div class="row">
          <div class="col-md-12 text-right">
                  <button class="btn btn-primary cancel_btn" >Cancel Order</button>
          </div>
        </div>

                       



      </div>
    </div>    
    
  </div>
</section>







<!-- Footer Starts Here -->
<?php site_footer(); ?>

</body>
</html>