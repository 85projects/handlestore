<header>
  <div class="container">
    <div class="row">
      <div class="col-md-12" style="z-index: 5000;">
        <nav class="navbar navbar-expand-lg navbar-light bg-white">
  <a href="index.php" class="navbar-brand"><img src="logos/handle_logo.png" alt="The hangle store" id="store_logo"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="index.php">Home<span class="sr-only">(current)</span></a>
      </li>
       <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Handles
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Mortice Plates</a>
          <a class="dropdown-item" href="#">Mortice Rose</a>
          <a class="dropdown-item" href="#">Main Door Pull</a>
           <a class="dropdown-item" href="#">Cabinets</a>
          <a class="dropdown-item" href="#">Knobs</a>
          <a class="dropdown-item" href="#">Concealed</a>
           <a class="dropdown-item" href="#">Profile</a>
          <a class="dropdown-item" href="#">Glass door</a>
        </div>
      </li>
       <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Door Accessories
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Towerbolts</a>
          <a class="dropdown-item" href="#">Hinges</a>
          <a class="dropdown-item" href="#">Door Closers</a>
          <a class="dropdown-item" href="#">Aldrops & Latches</a>
          <a class="dropdown-item" href="#">Robe hooks and Keyholders</a>
          <a class="dropdown-item" href="#">Locks</a>
          <a class="dropdown-item" href="#">Magnets and stoppers</a>
          <a class="dropdown-item" href="#">Nails and Knockers</a>
          <a class="dropdown-item" href="#">Nails and Knockers</a>
        </div>
      </li>
       <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Bath Accessories
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">5 piece Sets</a>
          <a class="dropdown-item" href="#">Glass Shelves</a>
          <a class="dropdown-item" href="#">SS Mirror cabinets</a>
          <a class="dropdown-item" href="#">Shower seats and foot rests</a>
          <a class="dropdown-item" href="#">Individual products</a>
        </div>
      </li>
      
      <li class="nav-item">
        <button id="main_cart_btn"><i class="fas fa-shopping-cart"></i> Cart</button>
      </li>
      
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" id="search_btn" type="submit">Search</button>
    </form>
  </div>
</nav>
      </div>
    </div>
  </div>
</header>