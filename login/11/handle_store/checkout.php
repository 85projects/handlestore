<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">

  <title>The Handle Store | Checkout</title>

  <meta name="description" content="Best handle selling website">
  <meta name="author" content="Handle Store">

  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="style.css" rel="stylesheet">

  <script src="https://kit.fontawesome.com/f000cd9cea.js" crossorigin="anonymous"></script>

<!----created date:15 sep 2020-------->
</head>
<body>

<!---------------------------HEADER AREA--------------------------------->

<!-- <header id="header_area">
  <div class="container">
     <div class="row">


     <div class="col-md-5" >
            <a href="index.php" class="navbar-brand"><img src="logos/handle_logo.png" alt="The hangle store" id="store_logo"></a>

      <input type="" name="" placeholder="Search for handles,knobs and more;">
     </div> 
     <div id="main_menu" class="col-md-7">
      
      <nav id="menu" class="navbar navbar-expand-sm ">
        <div class="row">
          <div class="col-md-12 text-right">
            
          </div>
        </div>
         <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar" >
        <i class="fas fa-bars"></i>
  </button>

        <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul id="nav" class="navbar-nav">
          <li class="nav-item"><a href="" class="nav-link">MAIN DOOR</a></li>
          <li class="nav-item"><a href="" class="nav-link">KITCHEN</a></li>
          <li class="nav-item"><a href="" class="nav-link">CABINET</a></li>
          <li class="nav-item"><a href="" class="nav-link">KNOBS</a></li>
          <li class="nav-item"><a href="" class="nav-link">PROFILES</a></li>
          <li class="nav-item"><a href="" class="nav-link">MORTICE</a></li>
          <li class="nav-item"><button id="main_cart_btn"><i class="fas fa-shopping-cart"></i> Cart</button></li>

        </ul>
      </div>
      </nav>

     </div> 

     </div>   
  </div>  
</header> -->

<header>
  <div class="container">
    <div class="row">
      <div class="col-md-12" style="z-index: 5000;">
        <nav class="navbar navbar-expand-lg navbar-light bg-white" style="padding-top: 20px;padding-bottom: 20px;">
  <a href="index.php" class="navbar-brand"><img src="logos/handle_logo.png" alt="The hangle store" id="store_logo"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
    <!--   <li class="nav-item active">
        <a class="nav-link" href="index.php" style="font-size: 20px;color: #000000;">Home<span class="sr-only">(current)</span></a>
      </li> -->
       <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-size: 20px;color: #323637;">
          Handles
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Mortice Plates</a>
          <a class="dropdown-item" href="#">Mortice Rose</a>
          <a class="dropdown-item" href="#">Main Door Pull</a>
           <a class="dropdown-item" href="#">Cabinets</a>
          <a class="dropdown-item" href="#">Knobs</a>
          <a class="dropdown-item" href="#">Concealed</a>
           <a class="dropdown-item" href="#">Profile</a>
          <a class="dropdown-item" href="#">Glass door</a>
        </div>
      </li>
       <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-size: 20px;color: #323637;">
          Door Accessories
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Towerbolts</a>
          <a class="dropdown-item" href="#">Hinges</a>
          <a class="dropdown-item" href="#">Door Closers</a>
          <a class="dropdown-item" href="#">Aldrops & Latches</a>
          <a class="dropdown-item" href="#">Robe hooks and Keyholders</a>
          <a class="dropdown-item" href="#">Locks</a>
          <a class="dropdown-item" href="#">Magnets and stoppers</a>
          <a class="dropdown-item" href="#">Nails and Knockers</a>
          <a class="dropdown-item" href="#">Nails and Knockers</a>
        </div>
      </li>
       <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-size: 20px;color: #323637;">
          Bath Accessories
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">5 piece Sets</a>
          <a class="dropdown-item" href="#">Glass Shelves</a>
          <a class="dropdown-item" href="#">SS Mirror cabinets</a>
          <a class="dropdown-item" href="#">Shower seats and foot rests</a>
          <a class="dropdown-item" href="#">Individual products</a>
        </div>
      </li>
      
      <li class="nav-item" >
        <a href="cart.php"><button id="main_cart_btn"><i class="fas fa-shopping-cart"></i> Cart</button></a>
         </li>
      
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" id="search_btn" type="submit">Search</button>
    </form>
  </div>
</nav>
      </div>
    </div>
  </div>
</header>




<!-----------------------------------CHECKOUT---------------------------------------->


<div class="checkout">
  <div class="container2">

    <div class="checkout_title">      
       <div class="row">
         <div class="col-md-12">
           <h1>Checkout</h1>
         </div>
       </div>
    </div>

    <div class="checkout_details">
       
       <div class="row">
         <div class="col-md-6" style="padding-top: 20px;">
           <input type="text" name="first_name" class="form-control" placeholder="First Name*">
         </div>
         <div class="col-md-6" style="padding-top: 20px;">
           <input type="text" name="last_name" class="form-control" placeholder="Last Name*">
         </div>
       </div>

       <div class="row">
         <div class="col-md-12" style="padding-top: 20px;">
           <input type="text" name="company" class="form-control" placeholder="Company Name">
         </div>
       </div>

       <div class="row">
         <div class="col-md-12" style="padding-top: 20px;">
           <label for="cars">Choose a country*:</label>

<select name="cars" id="cars" class="form-control">
  <option value="volvo">India</option>
  <option value="saab">USA</option>
  <option value="mercedes">England</option>
  <option value="audi">Australia</option>
</select>
         </div>
       </div>


         <div class="row">
         <div class="col-md-12" style="padding-top: 20px;">
           <input type="text" name="address" class="form-control" placeholder="Address*">
         </div>
       </div>

       <div class="row">
         <div class="col-md-12" style="padding-top: 20px;">
           <input type="text" name="town" class="form-control" placeholder="Town/City*">
         </div>
       </div>

        <div class="row">
         <div class="col-md-12" style="padding-top: 20px;">
           <input type="number" name="postcode" class="form-control" placeholder="Postcode/Zip*">
         </div>
       </div>

       <div class="row">
         <div class="col-md-6" style="padding-top: 20px;">
           <input type="number" name="phone" class="form-control" placeholder="Phone*">
         </div>
         <div class="col-md-6" style="padding-top: 20px;">
           <input type="text" name="email" class="form-control" placeholder="Email Address*">
         </div>
       </div>

       <div class="row">
         <div class="col-md-12" style="padding-top: 20px;">
            <label for="cars">Order Notes</label>
            <textarea class="form-control" placeholder="Note about your order,eg.special notes for delivery"></textarea>
         </div>
       </div>
     
       <div class="row">
         <div class="col-md-12 text-right" style="padding-top: 40px;">
           <h3>Billing Method</h3>
       
           <input type="radio" id="" name="payment" value="bank transfer">
           <label for="">Bank Transfer</label><br>
           <input type="radio" id="" name="payment" value=""> 
           <label for="">Check Payment</label><br>
           <input type="radio" id="" name="payment" value="">
           <label for="">Cash on delivery</label><br>

           <button class="btn" id="checkout_final_btn"><strong>Checkout</strong></button>
         </div>
       </div>

    </div>


  </div>
</div>




































<!---------------------------FOOTER AREA--------------------------------->

<footer id="footer_area" style="background-color: #f5f6f8;">

  <div class="container">
    
    
    <div class="row">
    
    <div class="col-md-4" id="left_part_footer" >

             <a href="index.php"><img src="logos/handle_logo.png" alt="The hangle store" id="store_logo"></a>
      <p>Aliquam sodales accumsan justo, at fringilla 
elit pulvinar cursus.Aliquam sodales accumsan justo, 
at fringilla elit pulvinar cursus.</p>

    </div>  
    <div class="col-md-2">
      <ul>
      <a href=""><li><p>MAIN DOOR</p></li></a>
      <a href=""><li><p>KITCHEN</p></li></a>    
      <a href=""><li><p>CABINET</p></li></a>  
      <a href=""><li><p>KNOBS</p></li></a>  
      <a href=""><li><p>PROFILES</p></li></a> 
      <a href=""><li><p>MORTICE</p></li></a>  

      </ul>
    </div>
    <div class="col-md-2">
      <ul>
      <a href=""><li>ABOUT US</li></a>
      <a href=""><li>CONTACT US</li></a>    
      <a href=""><li>PRIVACY POLICY</li></a>  
      <a href=""><li>FAQ'S</li></a> 
      <a href=""><li>WARRANTY</li></a>  
      <a href=""><li>T&C</li></a> 
      <a href=""><li>RETURN POLICY</li></a> 

      </ul>
    </div>
    <div class="col-md-4"  id="social_media">
      
      <h4>Social Media</h4>

      <div class="btn-group" id="button_group_social">
        <button><i class="fab fa-facebook-f"></i></button>
        <button><i class="fab fa-twitter"></i></button>
        <button><i class="fab fa-instagram"></i></button>
        <button><i class="fab fa-youtube"></i></button>
      </div>


    </div>

    </div>


  </div>
</footer>








<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>


</body>
</html>