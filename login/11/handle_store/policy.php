<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">

  <title>The Handle Store | Our Policy</title>

  <meta name="description" content="Best handle selling website">
  <meta name="author" content="Handle Store">

  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="style.css" rel="stylesheet">

  <script src="https://kit.fontawesome.com/f000cd9cea.js" crossorigin="anonymous"></script>

<!----created date:15 sep 2020-------->
</head>
<body>

<!---------------------------HEADER AREA--------------------------------->

<!-- <header id="header_area">
  <div class="container">
     <div class="row">


     <div class="col-md-5" >
            <a href="index.php" class="navbar-brand"><img src="logos/handle_logo.png" alt="The hangle store" id="store_logo"></a>

      <input type="" name="" placeholder="Search for handles,knobs and more;">
     </div> 
     <div id="main_menu" class="col-md-7">
      
      <nav id="menu" class="navbar navbar-expand-sm ">
        <div class="row">
          <div class="col-md-12 text-right">
            
          </div>
        </div>
         <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar" >
        <i class="fas fa-bars"></i>
  </button>

        <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul id="nav" class="navbar-nav">
          <li class="nav-item"><a href="" class="nav-link">MAIN DOOR</a></li>
          <li class="nav-item"><a href="" class="nav-link">KITCHEN</a></li>
          <li class="nav-item"><a href="" class="nav-link">CABINET</a></li>
          <li class="nav-item"><a href="" class="nav-link">KNOBS</a></li>
          <li class="nav-item"><a href="" class="nav-link">PROFILES</a></li>
          <li class="nav-item"><a href="" class="nav-link">MORTICE</a></li>
          <li class="nav-item"><button id="main_cart_btn"><i class="fas fa-shopping-cart"></i> Cart</button></li>

        </ul>
      </div>
      </nav>

     </div> 

     </div>   
  </div>  
</header> -->


<header>
  <div class="container">
    <div class="row">
      <div class="col-md-12" style="z-index: 5000;">
        <nav class="navbar navbar-expand-lg navbar-light bg-white" style="padding-top: 20px;padding-bottom: 20px;">
  <a href="index.php" class="navbar-brand"><img src="logos/handle_logo.png" alt="The hangle store" id="store_logo"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
    <!--   <li class="nav-item active">
        <a class="nav-link" href="index.php" style="font-size: 20px;color: #000000;">Home<span class="sr-only">(current)</span></a>
      </li> -->
       <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-size: 20px;color: #323637;">
          Handles
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Mortice Plates</a>
          <a class="dropdown-item" href="#">Mortice Rose</a>
          <a class="dropdown-item" href="#">Main Door Pull</a>
           <a class="dropdown-item" href="#">Cabinets</a>
          <a class="dropdown-item" href="#">Knobs</a>
          <a class="dropdown-item" href="#">Concealed</a>
           <a class="dropdown-item" href="#">Profile</a>
          <a class="dropdown-item" href="#">Glass door</a>
        </div>
      </li>
       <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-size: 20px;color: #323637;">
          Door Accessories
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Towerbolts</a>
          <a class="dropdown-item" href="#">Hinges</a>
          <a class="dropdown-item" href="#">Door Closers</a>
          <a class="dropdown-item" href="#">Aldrops & Latches</a>
          <a class="dropdown-item" href="#">Robe hooks and Keyholders</a>
          <a class="dropdown-item" href="#">Locks</a>
          <a class="dropdown-item" href="#">Magnets and stoppers</a>
          <a class="dropdown-item" href="#">Nails and Knockers</a>
          <a class="dropdown-item" href="#">Nails and Knockers</a>
        </div>
      </li>
       <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-size: 20px;color: #323637;">
          Bath Accessories
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">5 piece Sets</a>
          <a class="dropdown-item" href="#">Glass Shelves</a>
          <a class="dropdown-item" href="#">SS Mirror cabinets</a>
          <a class="dropdown-item" href="#">Shower seats and foot rests</a>
          <a class="dropdown-item" href="#">Individual products</a>
        </div>
      </li>
      
      <li class="nav-item" >
        <a href="cart.php"><button id="main_cart_btn"><i class="fas fa-shopping-cart"></i> Cart</button></a>
         </li>
      
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" id="search_btn" type="submit">Search</button>
    </form>
  </div>
</nav>
      </div>
    </div>
  </div>
</header>




<!---------------------------------POLICY--------------------------------------->


<div class="policy">
  <div class="container2">
    
   <div class="policy_title">
     <div class="row">
       <div class="col-md-12">
         <h1>Privacy & Cookie Policy</h1>
       </div>
     </div>
   </div>

   <div class="policy_content">
     <div class="row">
       <div class="col-md-1"></div>
       <div class="col-md-10">
         <div class="row">
           <div class="col-md-12">
             <p>Your privacy is very important to us. Accordingly, we have developed this Policy in order for you to understand how we collect, use, communicate and disclose and make use of personal information. The following outlines our privacy policy.</p>
             <ul>
               <li><p>Before or at the time of collecting personal information, we will identify the purposes for which information is being collected.</p></li>
               <li><p>We will collect and use of personal information solely with the objective of fulfilling those purposes specified by us and for other compatible purposes, unless we obtain the consent of the individual concerned or as required by law.</p></li>
               <li><p>We will only retain personal information as long as necessary for the fulfillment of those purposes.</p></li>
               <li><p>We will collect personal information by lawful and fair means and, where appropriate, with the knowledge or consent of the individual concerned.</p></li>
               <li><p>Personal data should be relevant to the purposes for which it is to be used, and, to the extent necessary for those purposes, should be accurate, complete, and up-to-date.</p></li>
               <li><p>We will protect personal information by reasonable security safeguards against loss or theft, as well as unauthorized access, disclosure, copying, use or modification.</p></li>
               <li><p>We will make readily available to customers information about our policies and practices relating to the management of personal information.</p></li>
             </ul>
             <p>We are committed to conducting our business in accordance with these principles in order to ensure</p>
           </div>
         </div>
       </div>
       <div class="col-md-1"></div>
     </div>
   </div>

  </div>
</div>




























<!---------------------------FOOTER AREA--------------------------------->

<footer id="footer_area" style="background-color: #f5f6f8;">

  <div class="container">
    
    
    <div class="row">
    
    <div class="col-md-4" id="left_part_footer" >

             <a href="index.php"><img src="logos/handle_logo.png" alt="The hangle store" id="store_logo"></a>
      <p>Aliquam sodales accumsan justo, at fringilla 
elit pulvinar cursus.Aliquam sodales accumsan justo, 
at fringilla elit pulvinar cursus.</p>

    </div>  
    <div class="col-md-2">
      <ul>
      <a href=""><li><p>MAIN DOOR</p></li></a>
      <a href=""><li><p>KITCHEN</p></li></a>    
      <a href=""><li><p>CABINET</p></li></a>  
      <a href=""><li><p>KNOBS</p></li></a>  
      <a href=""><li><p>PROFILES</p></li></a> 
      <a href=""><li><p>MORTICE</p></li></a>  

      </ul>
    </div>
    <div class="col-md-2">
      <ul>
      <a href=""><li>ABOUT US</li></a>
      <a href=""><li>CONTACT US</li></a>    
      <a href=""><li>PRIVACY POLICY</li></a>  
      <a href=""><li>FAQ'S</li></a> 
      <a href=""><li>WARRANTY</li></a>  
      <a href=""><li>T&C</li></a> 
      <a href=""><li>RETURN POLICY</li></a> 

      </ul>
    </div>
    <div class="col-md-4"  id="social_media">
      
      <h4>Social Media</h4>

      <div class="btn-group" id="button_group_social">
        <button><i class="fab fa-facebook-f"></i></button>
        <button><i class="fab fa-twitter"></i></button>
        <button><i class="fab fa-instagram"></i></button>
        <button><i class="fab fa-youtube"></i></button>
      </div>


    </div>

    </div>


  </div>
</footer>








<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>


</body>
</html>