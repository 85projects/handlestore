<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">

  <title>The Handle Store | Product Listingd</title>

  <meta name="description" content="Best handle selling website">
  <meta name="author" content="Handle Store">

  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="style.css" rel="stylesheet">

  <script src="https://kit.fontawesome.com/f000cd9cea.js" crossorigin="anonymous"></script>

<!----created date:15 sep 2020-------->
</head>
<body>

<!---------------------------HEADER AREA--------------------------------->

<!-- <header id="header_area">
  <div class="container">
     <div class="row">


     <div class="col-md-5" >
            <a href="index.php" class="navbar-brand"><img src="logos/handle_logo.png" alt="The hangle store" id="store_logo"></a>

      <input type="" name="" placeholder="Search for handles,knobs and more;">
     </div> 
     <div id="main_menu" class="col-md-7">
      
      <nav id="menu" class="navbar navbar-expand-sm ">
        <div class="row">
          <div class="col-md-12 text-right">
            
          </div>
        </div>
         <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar" >
        <i class="fas fa-bars"></i>
  </button>

        <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul id="nav" class="navbar-nav">
          <li class="nav-item"><a href="" class="nav-link">MAIN DOOR</a></li>
          <li class="nav-item"><a href="" class="nav-link">KITCHEN</a></li>
          <li class="nav-item"><a href="" class="nav-link">CABINET</a></li>
          <li class="nav-item"><a href="" class="nav-link">KNOBS</a></li>
          <li class="nav-item"><a href="" class="nav-link">PROFILES</a></li>
          <li class="nav-item"><a href="" class="nav-link">MORTICE</a></li>
          <li class="nav-item"><button id="main_cart_btn"><i class="fas fa-shopping-cart"></i> Cart</button></li>

        </ul>
      </div>
      </nav>

     </div> 

     </div>   
  </div>  
</header>

 -->



 <header>
  <div class="container">
    <div class="row">
      <div class="col-md-12" style="z-index: 5000;">
        <nav class="navbar navbar-expand-lg navbar-light bg-white" style="padding-top: 20px;padding-bottom: 20px;">
  <a href="index.php" class="navbar-brand"><img src="logos/handle_logo.png" alt="The hangle store" id="store_logo"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
    <!--   <li class="nav-item active">
        <a class="nav-link" href="index.php" style="font-size: 20px;color: #000000;">Home<span class="sr-only">(current)</span></a>
      </li> -->
       <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-size: 20px;color: #323637;">
          Handles
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Mortice Plates</a>
          <a class="dropdown-item" href="#">Mortice Rose</a>
          <a class="dropdown-item" href="#">Main Door Pull</a>
           <a class="dropdown-item" href="#">Cabinets</a>
          <a class="dropdown-item" href="#">Knobs</a>
          <a class="dropdown-item" href="#">Concealed</a>
           <a class="dropdown-item" href="#">Profile</a>
          <a class="dropdown-item" href="#">Glass door</a>
        </div>
      </li>
       <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-size: 20px;color: #323637;">
          Door Accessories
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Towerbolts</a>
          <a class="dropdown-item" href="#">Hinges</a>
          <a class="dropdown-item" href="#">Door Closers</a>
          <a class="dropdown-item" href="#">Aldrops & Latches</a>
          <a class="dropdown-item" href="#">Robe hooks and Keyholders</a>
          <a class="dropdown-item" href="#">Locks</a>
          <a class="dropdown-item" href="#">Magnets and stoppers</a>
          <a class="dropdown-item" href="#">Nails and Knockers</a>
          <a class="dropdown-item" href="#">Nails and Knockers</a>
        </div>
      </li>
       <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-size: 20px;color: #323637;">
          Bath Accessories
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">5 piece Sets</a>
          <a class="dropdown-item" href="#">Glass Shelves</a>
          <a class="dropdown-item" href="#">SS Mirror cabinets</a>
          <a class="dropdown-item" href="#">Shower seats and foot rests</a>
          <a class="dropdown-item" href="#">Individual products</a>
        </div>
      </li>
      
      <li class="nav-item" >
        <a href="cart.php"><button id="main_cart_btn"><i class="fas fa-shopping-cart"></i> Cart</button></a>
         </li>
      
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" id="search_btn" type="submit">Search</button>
    </form>
  </div>
</nav>
      </div>
    </div>
  </div>
</header>




<!---------------------------MAIN DOOR HANDLES--------------------------------->


<div id="main_door">
 <div class="container2">
   
   <div id="filters">
  <p> <a href="index.php">Home</a> / <a href="">Main Door</a></p>
   <div class="row">
     <div class="col-md-5">
       <h1>Main Door Handles</h1>
     </div>
     <div class="col-md-7">
       <hr/>
     </div>
   </div>


    <div id="filters_main">
      <div class="row">


        <div class="col-md-3">
            <div class="row">
              <div class="col-md-6">
                <h3>Filters</h3>
              </div>
              <div class="col-md-6 text-right">
              <a href=""><p class="reset">Reset</p></a>           
              </div>
            </div>
            <div id="view_the_product">

        <div id="select_material">
        <p><strong>SELECT MATERIAL</strong></p>
        <hr/>
        <a href=""> <p>Wood (563)</p></a>
        <a href=""> <p>Brass (434)</p></a>
        <a href=""> <p>SS-304(125)</p></a>       
        </div> 

        <div id="select_finish">
        <p><strong>SELECT FINISH</strong></p>
        <hr/>
        <a href=""><p>Copper (24)</p></a>
        <a href=""><p>Black touch (8)</p></a>
        <a href=""><p>White Touch (23)</p></a>       
        </div> 

         <div id="select_size">
        <p><strong>SELECT SIZE</strong></p>
        <hr/>
        <a href=""><p>4 inches(25)</p></a>
        <a href=""><p>6 inches (47)</p></a>
        <a href=""><p>8 inches (43)</p></a>
        <a href=""><p>12 inches (32)</p></a>
        <a href=""><p>16 inches (21)</p></a>
        <a href=""><p>24 inches (15)</p></a>
        <a href=""><p>30 inches and above (2)</p></a>  
        </div>

         <div id="price_range">
        <p><strong>PRICE RANGE</strong></p>
        <hr/>
        <input type="range" min="1" max="100" value="50" class="slider" id="">
        </div>  


        <div id="offer_div">
          <img src="background/offer_img.png" id="offer_img" class="img-fluid">
          <h1>Buy 1 get 1</h1>
          <h2>Free</h2>
          <p>Aliquam sodales
elit pulvinar cursus.</p>
        </div>

        </div>
      </div>



        <div class="col-md-9">
         <div class="row">
           <div class="col-md-8">
             <select name="sort" id="sort">
             <option value="popularity">Popularity</option>
             <option value="relevance">Relevance</option>
             <option value="price">Price</option>
             </select>
           </div>
           <div class="col-md-4">
             <div class="btn-group text-right" id="nav_buttons">
               <button class="product_navigation"><strong>PREV PAGE</strong></button>
               <button class="product_navigation"><strong>NEXT PAGE</strong></button>
             </div>
           </div>
         </div>

         <div class="product_display">
           <div class="row">
             
                     <div class="col-md-3 text-center">                     
                        <div class="container3">                     
                     <img src="background/arrival1.png" alt="product_img" class="product_img">
                     <h5 class="arrival_product_text1"><strong>Deep Rope</strong></h5>
                     <h5 class="arrival_product_text2">&#8377;1599/-</h5>
                     <hr/>
                    <div class="btn-group color_pallets">
                        <button class="btn_yellow"></button>
                        <button class="btn_black"></button>
                        <button class="btn_grey"></button>
                    </div>


                     <div class="overlayinn">
                     <h3>View The</h3>
                     <h3>Product</h3>
                     </div>

                       </div>                                                                  
                      </div>    

             <div class="col-md-3 text-center">                
                <div class="container3">                     
                   <img src="background/arrival2.png" alt="product_img" class="product_img">
                   <h5 class="arrival_product_text1"><strong>Cute Lady</strong></h5>
                   <h5 class="arrival_product_text2">&#8377;1299/-</h5>
                   <hr/>
                   <div class="btn-group color_pallets">
                        <button class="btn_yellow"></button>
                        <button class="btn_black"></button>
                        <button class="btn_grey"></button>
                    </div>
                   <div class="overlayinn">
                     <h3>View The</h3>
                     <h3>Product</h3>
                     </div>
               </div>                                          
             </div>  

             <div class="col-md-3 text-center">                 
                        <div class="container3">                     
                     <img src="background/arrival3.png" alt="product_img" class="product_img">
                     <h5 class="arrival_product_text1"><strong>Zig Zag</strong></h5>
                     <h5 class="arrival_product_text2">&#8377;2599/-</h5>
                     <hr/>
                     <div class="btn-group color_pallets">
                        <button class="btn_yellow"></button>
                        <button class="btn_black"></button>
                        <button class="btn_grey"></button>
                    </div>                        
                    <div class="overlayinn">
                     <h3>View The</h3>
                     <h3>Product</h3>
                     </div> 
                     </div>                           
             </div>  

             <div class="col-md-3 text-center">                    
                        <div class="container3">                     
                     <img src="background/arrival4.png" alt="product_img" class="product_img">
                     <h5 class="arrival_product_text1"><strong>Corola</strong></h5>
                     <h5 class="arrival_product_text2">&#8377;5499/-</h5>
                     <hr/>
                     <div class="btn-group color_pallets">
                        <button class="btn_yellow"></button>
                        <button class="btn_black"></button>
                        <button class="btn_grey"></button>
                    </div>
                    <div class="overlayinn">
                     <h3>View The</h3>
                     <h3>Product</h3>
                     </div>
                </div>                                  
             </div> 
           </div>


         <div class="row2">
           <div class="row">
             
                     <div class="col-md-3 text-center">                     
                        <div class="container3">                     
                     <img src="background/arrival1.png" alt="product_img" class="product_img">
                     <h5 class="arrival_product_text1"><strong>Deep Rope</strong></h5>
                     <h5 class="arrival_product_text2">&#8377;1599/-</h5>
                     <hr/>
                    <div class="btn-group color_pallets">
                        <button class="btn_yellow"></button>
                        <button class="btn_black"></button>
                        <button class="btn_grey"></button>
                    </div>


                     <div class="overlayinn">
                     <h3>View The</h3>
                     <h3>Product</h3>
                     </div>

                       </div>                                                                  
                      </div>    

             <div class="col-md-3 text-center">                
                <div class="container3">                     
                   <img src="background/arrival2.png" alt="product_img" class="product_img">
                   <h5 class="arrival_product_text1"><strong>Cute Lady</strong></h5>
                   <h5 class="arrival_product_text2">&#8377;1299/-</h5>
                   <hr/>
                   <div class="btn-group color_pallets">
                        <button class="btn_yellow"></button>
                        <button class="btn_black"></button>
                        <button class="btn_grey"></button>
                    </div>
                   <div class="overlayinn">
                     <h3>View The</h3>
                     <h3>Product</h3>
                     </div>
               </div>                                          
             </div>  

             <div class="col-md-3 text-center">                 
                        <div class="container3">                     
                     <img src="background/arrival3.png" alt="product_img" class="product_img">
                     <h5 class="arrival_product_text1"><strong>Zig Zag</strong></h5>
                     <h5 class="arrival_product_text2">&#8377;2599/-</h5>
                     <hr/>
                     <div class="btn-group color_pallets">
                        <button class="btn_yellow"></button>
                        <button class="btn_black"></button>
                        <button class="btn_grey"></button>
                    </div>
                    <div class="overlayinn">
                     <h3>View The</h3>
                     <h3>Product</h3>
                     </div>                     
                       </div>                             
             </div>  

             <div class="col-md-3 text-center">                    
                        <div class="container3">                     
                     <img src="background/arrival4.png" alt="product_img" class="product_img">
                     <h5 class="arrival_product_text1"><strong>Corola</strong></h5>
                     <h5 class="arrival_product_text2">&#8377;5499/-</h5>
                     <hr/>
                     <div class="btn-group color_pallets">
                        <button class="btn_yellow"></button>
                        <button class="btn_black"></button>
                        <button class="btn_grey"></button>
                    </div>
                    <div class="overlayinn">
                     <h3>View The</h3>
                     <h3>Product</h3>
                     </div>
                </div>                                  
             </div> 
           </div>

         </div>


          <div class="row3">
            <div class="row">
             
                     <div class="col-md-3 text-center">                     
                        <div class="container3">                     
                     <img src="background/arrival1.png" alt="product_img" class="product_img">
                     <h5 class="arrival_product_text1"><strong>Deep Rope</strong></h5>
                     <h5 class="arrival_product_text2">&#8377;1599/-</h5>
                     <hr/>
                    <div class="btn-group color_pallets">
                        <button class="btn_yellow"></button>
                        <button class="btn_black"></button>
                        <button class="btn_grey"></button>
                    </div>


                     <div class="overlayinn">
                     <h3>View The</h3>
                     <h3>Product</h3>
                     </div>

                       </div>                                                                  
                      </div>    

             <div class="col-md-3 text-center">                
                <div class="container3">                     
                   <img src="background/arrival2.png" alt="product_img" class="product_img">
                   <h5 class="arrival_product_text1"><strong>Cute Lady</strong></h5>
                   <h5 class="arrival_product_text2">&#8377;1299/-</h5>
                   <hr/>
                   <div class="btn-group color_pallets">
                        <button class="btn_yellow"></button>
                        <button class="btn_black"></button>
                        <button class="btn_grey"></button>
                    </div>
                   <div class="overlayinn">
                     <h3>View The</h3>
                     <h3>Product</h3>
                     </div>
               </div>                                          
             </div>  

             <div class="col-md-3 text-center">                 
                        <div class="container3">                     
                     <img src="background/arrival3.png" alt="product_img" class="product_img">
                     <h5 class="arrival_product_text1"><strong>Zig Zag</strong></h5>
                     <h5 class="arrival_product_text2">&#8377;2599/-</h5>
                     <hr/>
                     <div class="btn-group color_pallets">
                        <button class="btn_yellow"></button>
                        <button class="btn_black"></button>
                        <button class="btn_grey"></button>
                    </div> 
                    <div class="overlayinn">
                     <h3>View The</h3>
                     <h3>Product</h3>
                     </div>
                     </div>                            
             </div>  

             <div class="col-md-3 text-center">                    
                        <div class="container3">                     
                     <img src="background/arrival4.png" alt="product_img" class="product_img">
                     <h5 class="arrival_product_text1"><strong>Corola</strong></h5>
                     <h5 class="arrival_product_text2">&#8377;5499/-</h5>
                     <hr/>
                     <div class="btn-group color_pallets">
                        <button class="btn_yellow"></button>
                        <button class="btn_black"></button>
                        <button class="btn_grey"></button>
                    </div>
                    <div class="overlayinn">
                     <h3>View The</h3>
                     <h3>Product</h3>
                     </div>
                </div>                                  
             </div> 
           </div>

          </div>


           <div class="row4">
             <div class="row">
             
                     <div class="col-md-3 text-center">                     
                        <div class="container3">                     
                     <img src="background/arrival1.png" alt="product_img" class="product_img">
                     <h5 class="arrival_product_text1"><strong>Deep Rope</strong></h5>
                     <h5 class="arrival_product_text2">&#8377;1599/-</h5>
                     <hr/>
                    <div class="btn-group color_pallets">
                        <button class="btn_yellow"></button>
                        <button class="btn_black"></button>
                        <button class="btn_grey"></button>
                    </div>


                     <div class="overlayinn">
                     <h3>View The</h3>
                     <h3>Product</h3>
                     </div>

                       </div>                                                                  
                      </div>    

             <div class="col-md-3 text-center">                
                <div class="container3">                     
                   <img src="background/arrival2.png" alt="product_img" class="product_img">
                   <h5 class="arrival_product_text1"><strong>Cute Lady</strong></h5>
                   <h5 class="arrival_product_text2">&#8377;1299/-</h5>
                   <hr/>
                   <div class="btn-group color_pallets">
                        <button class="btn_yellow"></button>
                        <button class="btn_black"></button>
                        <button class="btn_grey"></button>
                    </div>
                   <div class="overlayinn">
                     <h3>View The</h3>
                     <h3>Product</h3>
                     </div>
               </div>                                          
             </div>  

             <div class="col-md-3 text-center">                 
                        <div class="container3">                     
                     <img src="background/arrival3.png" alt="product_img" class="product_img">
                     <h5 class="arrival_product_text1"><strong>Zig Zag</strong></h5>
                     <h5 class="arrival_product_text2">&#8377;2599/-</h5>
                     <hr/>
                     <div class="btn-group color_pallets">
                        <button class="btn_yellow"></button>
                        <button class="btn_black"></button>
                        <button class="btn_grey"></button>
                    </div>
                    <div class="overlayinn">
                     <h3>View The</h3>
                     <h3>Product</h3>
                     </div>     
                     </div>                       
             </div>  

             <div class="col-md-3 text-center">                    
                        <div class="container3">                     
                     <img src="background/arrival4.png" alt="product_img" class="product_img">
                     <h5 class="arrival_product_text1"><strong>Corola</strong></h5>
                     <h5 class="arrival_product_text2">&#8377;5499/-</h5>
                     <hr/>
                     <div class="btn-group color_pallets">
                        <button class="btn_yellow"></button>
                        <button class="btn_black"></button>
                        <button class="btn_grey"></button>
                    </div>
                    <div class="overlayinn">
                     <h3>View The</h3>
                     <h3>Product</h3>
                     </div>
                </div>                                  
             </div> 
           </div>

           </div>
         </div>

         
        


        </div>
      </div>
    </div>   
 </div>
 
         <div class="pagination_area">
           <div class="row">
             <div class="col-md-5">
               <hr/>
             </div>
             <div class="col-md-7">
               <div class="btn-group pagination_btn_group" >
                 <button class="pagination_prev" type="button">PREV PAGE</button>
                 <button class="pagination_btn" type="button">1</button>
                 <button class="pagination_btn" type="button">2</button>
                 <button class="pagination_btn" type="button">3</button>
                 <button class="pagination_btn" type="button">4</button>
                 <button class="pagination_btn" type="button">5</button>
                 <button class="pagination_btn" type="button">6</button>
                 <button class="pagination_next" type="button">NEXT PAGE</button>
               </div>
             </div>
           </div>
         </div>
 
 </div> 
</div>

<!---------------------------NEWSLETTER---------------------->


<div id="newsletter">
  <div class="container2">
    

     <div class="row">
      
      <div class="col-md-6" id="newsletter_main">
      
      <h1>Newsletter</h1> 
      <div class="row">
        <div class="col-md-8">
          <p>Subscribe to the Handle Store list to receive updates 
on new arrivals, special offers and 
other discount information.</p>

 <input type="" name="" placeholder="Enter your Email Address">
        </div>
        <div class="col-md-4"></div>
      </div>
      

        


      </div>
      <div class="col-md-6" id="newsletter_image">
        
        <img src="background/newsletter_img.png" alt="newsletter">
      </div>

     </div>

  </div>
</div>




<!---------------------------REASONS TO SHOP--------------------------------->


<div id="reasons_to_shop">
  <div class="container2" >

    <div class="row">
      <div class="col-md-12"> 
        <h1>Reasons to shop at</h1> 
        <h1 id="the_handle_store">The Handle Store</h1> 
        <hr/>
      </div>
    </div>


    <div id="three_reasons">
      <div class="row">

        <div class="col-md-4" id="great_offers">            
          <div class="row">
            <div class="col-md-2">
             <img src="icons/reason_1.png" alt="reason1">                 
            </div>
            <div class="col-md-10">
              <h4>Great Offers</h4>
              <p>Subscribe to the Handle Store
on new arrivals, special offers and 
other discount information.</p>
            </div>
          </div>
        </div>

        <div class="col-md-4" id="free_shipping">
          <div class="row">
            <div class="col-md-2">  
            <img src="icons/reason_2.png" alt="reason2">              
            </div>
            <div class="col-md-10">
              <h4>Free Shipping</h4>
              <p>Subscribe to the Handle Store
on new arrivals, special offers and 
other discount information.</p>
            </div>
          </div>
        </div>

        <div class="col-md-4" id="safest_online_store">
          <div class="row">
            <div class="col-md-2"> 
            <img src="icons/reason_3.png" alt="reason3">                
            </div>
            <div class="col-md-10">
              <h4>Safest Online Store</h4>
              <p>Subscribe to the Handle Store
on new arrivals, special offers and 
other discount information.</p>
            </div>
          </div>
        </div>

      </div>
    </div>

  </div>  
</div>





























<!---------------------------FOOTER AREA--------------------------------->

<footer id="footer_area" style="background-color: #f5f6f8;">

  <div class="container">
    
    
    <div class="row">
    
    <div class="col-md-4" id="left_part_footer" >

      <a href="index.php"><img src="logos/handle_logo.png" alt="The hangle store" id="store_logo"></a>
      <p>Aliquam sodales accumsan justo, at fringilla 
elit pulvinar cursus.Aliquam sodales accumsan justo, 
at fringilla elit pulvinar cursus.</p>

    </div>  
    <div class="col-md-2">
      <ul>
      <a href=""><li>MAIN DOOR</li></a>
      <a href=""><li>KITCHEN</li></a>   
      <a href=""><li>CABINET</li></a> 
      <a href=""><li>KNOBS</li></a> 
      <a href=""><li>PROFILES</li></a>  
      <a href=""><li>MORTICE</li></a> 

      </ul>
    </div>
    <div class="col-md-2">
      <ul>
      <a href=""><li>ABOUT US</li></a>
      <a href=""><li>CONTACT US</li></a>    
      <a href=""><li>PRIVACY POLICY</li></a>  
      <a href=""><li>FAQ'S</li></a> 
      <a href=""><li>WARRANTY</li></a>  
      <a href=""><li>T&C</li></a> 
      <a href=""><li>RETURN POLICY</li></a> 

      </ul>
    </div>
    <div class="col-md-4"  id="social_media">
      
      <h4>Social Media</h4>

      <div class="btn-group" id="button_group_social">
        <button><i class="fab fa-facebook-f"></i></button>
        <button><i class="fab fa-twitter"></i></button>
        <button><i class="fab fa-instagram"></i></button>
        <button><i class="fab fa-youtube"></i></button>
      </div>


    </div>

    </div>


  </div>
  

</footer>

































<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>

</body>
</html>